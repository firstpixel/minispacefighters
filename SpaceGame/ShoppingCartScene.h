//
//  ShoppingCartScene.h
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 12/19/09.
//  Copyright 2009 Firstpixel. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "cocos2d.h"

@interface ShoppingCartScene : CCScene {

    int scaleFactor;
    float px;
    float py;
}
+(id) scene;
@end
