//
//  ShoppingCartScene.m
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 12/19/09.
//  Copyright 2009 Firstpixel. All rights reserved.
//

#import "ShoppingCartScene.h"
#import "MenuLayer.h"
#import "GlobalSingleton.h"
#import "MKStoreManager.h" 
#import "SoundMenuItem.h"


@implementation ShoppingCartScene

+(id) scene
{
	CCScene *scene = [CCScene node];
	id child = [ShoppingCartScene node];
	
	[scene addChild:child];
	return scene;
}


- (id) init {
    self = [super init];
    if (self != nil) {
        CGSize s = [[CCDirector sharedDirector] winSize];
		
        CCSprite * bg = nil;
        bg = [CCSprite spriteWithFile:@"selectionscreen-bg.png"];
        [bg  setPosition:ccp(s.width/2, s.height/2)];
        [self addChild:bg z:0];
      
        MKStoreManager *myMKStoreManager = [MKStoreManager sharedManager];
        [myMKStoreManager buyFeature:kFeatureAId
                          onComplete:^(NSString* purchasedFeature)
         {
             NSLog(@"Purchased: %@", purchasedFeature);
             //[buyLabel setString:[NSString stringWithFormat:@"Purchased: %@", purchasedFeature]];
         }
                         onCancelled:^
         {
             NSLog(@"User Cancelled Transaction");
             //[buyLabel setString:@"User Cancelled Transaction"];
         }];
        

        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"hud.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"Sprites.plist"];
        scaleFactor = 1;
        px = 0.0;
        py = 0.0;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            scaleFactor = 2;
            px = 32.0f;
            py = 64.0f;
        }
        //Menu back
        SoundMenuItem *back = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt-back.png" selectedSpriteFrameName:@"bt-back-over.png" target:self selector:@selector(goBack)];
        CCMenu *menuBack = [CCMenu menuWithItems: back, nil];
        [menuBack alignItemsVerticallyWithPadding:12.0];
        [menuBack setPosition:ccp(( 22*scaleFactor )+px,( 270*scaleFactor)+py )];
        [self addChild:menuBack z:2];

        
        CCLabelBMFont* messageLoading = [CCLabelBMFont labelWithString:@"LOADING STORE... \n\n IF IT TAKES TOO LONG,\n\n CHECK YOUR INTERNET \n\n CONNECTION." fntFile:@"XitRam.fnt"];
        [messageLoading setPosition:ccp(s.width/2, s.height/2)];
        [self addChild:messageLoading z:2];
    }
    return self;
}

-(void)backCallback:(id)sender{
	[[CCDirector sharedDirector] replaceScene:[MenuLayer scene]];
}

-(void)goBack{
    
    [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[MenuLayer scene]]];
    
}


- (void)ccTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
	
	UITouch *touch = [touches anyObject];
	CGPoint location = [touch locationInView: [touch view]];
	//NSLog(@"-------------------------------------------------------------- Clicked began : %i , %i",location.x,location.y);
	location = CGPointMake(location.y, location.x);
	
	
	//return kEventHandled;	
}
@end
