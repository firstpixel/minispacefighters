//
//  AboutNode.h
//  LevelSVG
//
//  Created by Ricardo Quesada on 25/03/10.
//  Copyright 2010 Sapus Media. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION
//

#import "cocos2d.h"


@interface AboutScene : CCLayer {
    	CCSpriteBatchNode	*spritesBatchNode_;
    
    int scaleFactor;
    float px;
    float py;

}
+(id) scene;

@end
