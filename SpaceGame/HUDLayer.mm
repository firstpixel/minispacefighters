//
//  HUDLayer.mm .m
//  MiniSpaceFighters
//
//  Created by Gil Beyruth on 9/8/11.
//  Copyright 2011 Ray Wenderlich. All rights reserved.
//

#import "HUDLayer.h"
#import "ShopLayer.h"
#import "SoundMenuItem.h"

@implementation HUDLayer
- (id)init {
    
    if ((self = [super init])) {
        CGSize winSize = [CCDirector sharedDirector].winSize;
        
        int scaleFactor;
        float px = 0.0;
        float py = 0.0;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            scaleFactor = 2;
            px = 32.0f;
            py = 64.0f;
        } else {
            scaleFactor = 1;
        }
        
        NSString* backgroundFile;

        backgroundFile = [NSString stringWithString:@"selectionscreen-bg.png"];
        
        
        CCSprite* background = [CCSprite spriteWithFile:backgroundFile];
        background.position = ccp(winSize.width/2,winSize.height/2);
        [self addChild:background];
        

            _nukesLabel = [CCLabelBMFont labelWithString:@"" fntFile:@"XitRam.fnt"];
            _shieldLabel = [CCLabelBMFont labelWithString:@"" fntFile:@"XitRam.fnt"];
            _powerLabel = [CCLabelBMFont labelWithString:@"" fntFile:@"XitRam.fnt"];
            _pointsLabel = [CCLabelBMFont labelWithString:@"" fntFile:@"XitRam.fnt"];
            
        
        CCSprite* powerIcon = [CCSprite spriteWithSpriteFrameName:@"power1.png"];
        powerIcon.anchorPoint = ccp(0,0); 
        powerIcon.position = ccp(( 6 * scaleFactor ) + px , ( 2 * scaleFactor ) + py);
        [self addChild:powerIcon];
        _powerLabel.anchorPoint = ccp(0,0.5);
        _powerLabel.position = ccp(( 30 * scaleFactor ) + px , ( 12 * scaleFactor ) + py);
        [self addChild:_powerLabel];
        
        
        
        
        CCSprite* shieldIcon = [CCSprite spriteWithSpriteFrameName:@"shield1.png"];
        shieldIcon.anchorPoint = ccp(1,0); 
        shieldIcon.position = ccp(( 474 * scaleFactor ) + px , ( 2 * scaleFactor ) + py);
        [self addChild:shieldIcon];
        _shieldLabel.anchorPoint = ccp(1,0.5);
        _shieldLabel.position = ccp(( 450 * scaleFactor ) + px , ( 12 * scaleFactor ) + py);        
        [self addChild:_shieldLabel];     
        
        
        //MONEY
        CCSprite* nukeIcon = [CCSprite spriteWithSpriteFrameName:@"nukes.png"];
        nukeIcon.position = ccp(( 210 * scaleFactor ) + px , ( 12 * scaleFactor ) + py);
        [self addChild:nukeIcon];
        _nukesLabel.anchorPoint = ccp(0,0.5);
        _nukesLabel.position = ccp( ( 220 * scaleFactor ) + px , ( 12 * scaleFactor ) + py );
        [self addChild:_nukesLabel];
        
        //POINTS
        CCSprite* pointsIcon = [CCSprite spriteWithSpriteFrameName:@"points.png"];
        pointsIcon.position = ccp(( 210 * scaleFactor ) + px , ( 34 * scaleFactor ) + py);
        [self addChild:pointsIcon];
        _pointsLabel.anchorPoint = ccp(0,0.5);
        _pointsLabel.position = ccp(( 220 * scaleFactor ) + px , ( 34 * scaleFactor ) + py);
        [self addChild:_pointsLabel];
        
        GlobalSingleton* mySingleton = [GlobalSingleton sharedInstance];
        
        [self setPoints:[NSString stringWithFormat:@"%i",[mySingleton myPoints]]];
        [self setNukesString:[NSString stringWithFormat:@"%i",[mySingleton myNukes]]];
        [self setShield:[NSString stringWithFormat:@"%i",[[mySingleton getShipFromPref:[mySingleton shipSelected]] shield]]];
        [self setPower:[NSString stringWithFormat:@"%i",[[mySingleton getShipFromPref:[mySingleton shipSelected]] power]]];
        
        
        
        
        //Menu back
        SoundMenuItem *back = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt-back.png" selectedSpriteFrameName:@"bt-back-over.png" target:self selector:@selector(goBack)];
        CCMenu *menuBack = [CCMenu menuWithItems: back, nil];
        [menuBack alignItemsVerticallyWithPadding:12.0];
        [menuBack setPosition:ccp((22*scaleFactor)+px,(270*scaleFactor)+py)];
        [self addChild:menuBack z:2];
        
        
    }
    return self;
}

- (void)setNukesString:(NSString *)string {
    _nukesLabel.string = string;
}

- (void)setPower:(NSString *)string{
    _powerLabel.string = string;

}
- (void)setShield:(NSString *)string{
    _shieldLabel.string = string;
    
}
- (void)setPoints:(NSString *)string{
    _pointsLabel.string = string;
    
}


-(void)goBack{
    
    [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ShopLayer scene]]];
    
}


@end
