//
//  AppDelegate.h
//  SpaceGame
//
//  Created by Ray Wenderlich on 6/24/11.
//  Copyright Ray Wenderlich 2011. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// DO NOT DISTRIBUTE WITHOUT PRIOR AUTHORIZATION (SEE LICENSE.TXT)
//

#import <UIKit/UIKit.h>
#import "cocos2d.h"
#import <MediaPlayer/MediaPlayer.h>

@class RootViewController;

@interface AppDelegate :  NSObject <UIApplicationDelegate, CCDirectorDelegate> {
    UIWindow *window_;
    UINavigationController *navController_;
    CCDirectorIOS	*director_;
    MPMoviePlayerController * moviePlayer;
    NSString* moviePlaying;
}
-(void)startMovie:(NSString*)video ofType:(NSString*)ext;
-(void)movieStop;
-(BOOL)movieIsPlaying:(NSString*)movie;
-(BOOL)movieIsPlaying;
@property (nonatomic, retain) NSString* moviePlaying;

@property (nonatomic, retain) UIWindow *window;
@property (readonly) CCDirectorIOS *director;
@property (readonly) UINavigationController *navController;

@end
