//
//  AboutNode.m
//  LevelSVG
//
//  Created by Ricardo Quesada on 25/03/10.
//  Copyright 2010 Sapus Media. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION
//

#import "AboutScene.h"
#import "MenuLayer.h"
#import "SoundMenuItem.h"
#import "GlobalSingleton.h"


@implementation AboutScene

+(id) scene {
	CCScene *s = [CCScene node];
	id node = [AboutScene node];
	[s addChild:node];
	return s;
}

-(id) init {
	if( (self=[super init])) {
		 
		CGSize size = [[CCDirector sharedDirector] winSize];
		CCSprite *background = [CCSprite spriteWithFile:@"about.png"];
        background.position = ccp(size.width/2, size.height/2);
		[self addChild:background];
        
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"hud.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"Sprites.plist"];
        scaleFactor = 1;
        px = 0.0;
        py = 0.0;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            scaleFactor = 2;
            px = 32.0f;
            py = 64.0f;
        }
        
        CCMenuItem * item0;
        CCMenuItem * item1;
        CCMenuItem * item2;
              
        item0 = [CCMenuItemSprite itemFromNormalSprite:[CCSprite spriteWithFile:@"firstpixel.png"] selectedSprite:[CCSprite spriteWithFile:@"firstpixel.png"]  target:self selector:@selector(firstpixel)];
        
        item1 = [CCMenuItemSprite itemFromNormalSprite:[CCSprite spriteWithFile:@"facebook.png"] selectedSprite:[CCSprite spriteWithFile:@"facebook.png"] target:self selector:@selector(facebook)];
        
        item2 = [CCMenuItemSprite itemFromNormalSprite:[CCSprite spriteWithFile:@"twitter.png"] selectedSprite:[CCSprite spriteWithFile:@"twitter.png"] target:self selector:@selector(twitter)];
       
        
       CCMenu* menu = [CCMenu menuWithItems: item0, item1, item2, nil];
        
        [menu alignItemsHorizontally];        
		[self addChild:menu];
        [menu setPosition:ccp(size.width/2,size.height/2-50*scaleFactor)];
		//back button
        //Menu back
        SoundMenuItem *back = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt-back.png" selectedSpriteFrameName:@"bt-back-over.png" target:self selector:@selector(goBack:)];
        CCMenu *menuBack = [CCMenu menuWithItems: back, nil];
        [menuBack alignItemsVerticallyWithPadding:12.0];
        [menuBack setPosition:ccp(( 22*scaleFactor )+px,( 270*scaleFactor)+py )];
        [self addChild:menuBack z:2];
	}
	return self;
}

-(void) firstpixel{
	// Launches Safari and opens the requested web page
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://firstpixel.com/site/games/"]];
}
-(void) twitter {
	// Launches Safari and opens the requested web page
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://twitter.com/firstpixel"]];
}
-(void) facebook {
	// Launches Safari and opens the requested web page
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.facebook.com/pages/Firstpixel/134776246539448"]];
}

-(void) goBack:(id)sender
{
	[[CCDirector sharedDirector] replaceScene: [CCTransitionRotoZoom transitionWithDuration:1 scene:[MenuLayer scene] ]];
}
@end