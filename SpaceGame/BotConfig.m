//
//  BotConfig.m
//  MiniSpaceFighters
//
//  Created by Gil Beyruth on 9/12/11.
//  Copyright 2011 Ray Wenderlich. All rights reserved.
//

#import "BotConfig.h"


@implementation BotConfig

@synthesize botValue,botType;

+(id)initBot
{
    if ((self = [super init])) {
        
    }
    return self;
}
-(void)initNewBot:(NSString*)_botName withType:(int)_botType withValue:(int)_botValue 
{
    
    botName = _botName;
    botType = _botType;
    botValue = _botValue;
    
}
@end
