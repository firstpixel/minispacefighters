//
//  ShipConfig.h
//  MiniSpaceFighters
//
//  Created by Gil Beyruth on 9/11/11.
//  Copyright 2011 Ray Wenderlich. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface ShipConfig  : NSObject {
    NSString*shipName;
    int shipType, shipValue, power, shield, shipPowerLevel, shipShieldLevel, botSlots, bot1, bot2, bot3, bot4, repairValue1, repairValue2, repairValue3, repairValue4, repairValue5, repairValue6, upgradeValue1, upgradeValue2, upgradeValue3, upgradeValue4, upgradeValue5, upgradeValue6;
}

@property (nonatomic,readwrite) int shipType, shipValue, power, shield, shipPowerLevel, shipShieldLevel, botSlots, bot1, bot2, bot3, bot4, repairValue1, repairValue2, repairValue3, repairValue4, repairValue5, repairValue6, upgradeValue1, upgradeValue2, upgradeValue3, upgradeValue4, upgradeValue5, upgradeValue6;
@property (nonatomic, retain) NSString* shipName;
+(id)init;


-(id)init:(NSString*)_shipName      withType:(int)_shipType                 withValue:(int)_shipValue
    withPower:(int)_power           withPowerLevel:(int)_shipPowerLevel 
    withShield:(int)_shield         withShieldLevel:(int)_shipShieldLevel 
    withSlots:(int)_botSlots      
    withBot1:(int)_bot1        
    withBot2:(int)_bot2       
    withBot3:(int)_bot3      
    withBot4:(int)_bot4

    withRepairValue1:(int)_repairValue1 
    withRepairValue2:(int)_repairValue2 
    withRepairValue3:(int)_repairValue3 
    withRepairValue4:(int)_repairValue4 
    withRepairValue5:(int)_repairValue5 
    withRepairValue6:(int)_repairValue6 

    withUpgradeValue1:(int)_upgradeValue1 
    withUpgradeValue2:(int)_upgradeValue2 
    withUpgradeValue3:(int)_upgradeValue3 
    withUpgradeValue4:(int)_upgradeValue4
    withUpgradeValue5:(int)_upgradeValue5
    withUpgradeValue6:(int)_upgradeValue6;

@end
