//
//  IntroNode.m
//  MiniSpaceFighter
//
//  Created by Gil Beyruth on 8/29/11.
//  Copyright 2011 Ray Wenderlich. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION


#import "IntroScene.h"
#import "MenuLayer.h"
#import <sys/utsname.h>
#import "GlobalSingleton.h"
//
// This is an small Scene that makes the trasition smoother from the Defaul.png image to the menu scene
//

@implementation IntroScene

+(id)scene {
    CCScene *scene = [CCScene node];
    IntroScene *layer = [IntroScene node];
    [scene addChild:layer];
    return scene;
}
-(id) init {
	if( (self=[super init])) {
		// Load all the sprites/platforms now
		CGSize size = [[CCDirector sharedDirector] winSize];
        CCSprite *background;

        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"hud.plist"];
        background = [CCSprite spriteWithFile:@"default1.png"];
		background.position = ccp(size.width/2, size.height/2);
		[self addChild:background];
		[self schedule:@selector(wait1second:) interval:1];
	}
	return self;
}

-(void)dealloc{
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    [super dealloc];
}

- (NSString *)machine
{
	struct utsname systemInfo;
	uname(&systemInfo);
	return [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
}
- (NSString *) platformString
{
    NSString *platform = [self machine];
	if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone3,2"])    return @"iPhone 4 Verizon";
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    if ([platform isEqualToString:@"i386"])         return @"Simulator";
    return platform;
}


-(void) wait1second:(ccTime)dt
{
    NSLog(@"DEVICE MACHINE: %@",[self machine]);
    GlobalSingleton * mySingleton = [GlobalSingleton sharedInstance];
    [mySingleton setDeviceType:[NSString stringWithString:[self platformString]]];
    [self unschedule: @selector(wait1second:)];
    [[CCDirector sharedDirector] replaceScene: [CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[MenuLayer scene]]];
}
@end
