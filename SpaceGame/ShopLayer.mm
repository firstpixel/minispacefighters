//
//  MenuLayer.cpp
//  MiniSpaceFighters
//
//  Created by Gil Beyruth on 8/29/11.
//  Copyright 2011 Ray Wenderlich. All rights reserved.
//

#import "ShopLayer.h"
#import "LevelLayer.h"
#import "MenuLayer.h"
#import "SoundMenuItem.h"
#import "SimpleAudioEngine.h"
#import "AppDelegate.h"

@implementation ShopLayer

+(id)scene {
    CCScene *scene = [CCScene node];
    ShopLayer *layer = [ShopLayer node];
    [scene addChild:layer];
    return scene;
}

-(id)init
{
    if( (self=[super init]) )
	{
        
        CGSize winSize = [CCDirector sharedDirector].winSize;
        
        if([[GlobalSingleton sharedInstance] audioOn]){
            if ([SimpleAudioEngine sharedEngine] != nil) {
                if([[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying]==NO || ![[GlobalSingleton sharedInstance] isPlayingMenuMusic]){
                    [[GlobalSingleton sharedInstance] setIsPlayingMenuMusic:YES];
                    [[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"menu.m4a"];
                    [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"menu.m4a" loop:YES];
                }
            }
        }else{
            if ([[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying]==YES){
                [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
            }
            [[GlobalSingleton sharedInstance] setIsPlayingMenuMusic:NO];    
        }
        
        
        
        if(![(AppDelegate*)[[UIApplication sharedApplication] delegate] movieIsPlaying:@"select-ship"])[(AppDelegate*)[[UIApplication sharedApplication] delegate] startMovie:@"select-ship" ofType:@"mov"];
        
        GlobalSingleton*mySingleton = [GlobalSingleton sharedInstance];
        [mySingleton unlockItem:@"shipUnlocked1"];
        if([mySingleton shipSelected]==0)[mySingleton setShipSelected:1];
        
        // SoundMenuItem *prevShipItem;
        // SoundMenuItem *nextShipItem;
        SoundMenuItem *prevBotItem;
        SoundMenuItem *nextBotItem;
        
        SoundMenuItem *shipItem1;
        SoundMenuItem *shipItem2;
        SoundMenuItem *shipItem3;
        
        [self setupBatchNode];
        [self setupBatchNodeFighter];
        [self setupBatchNodeBot];
        
        CCLabelBMFont* shipName;
        CCLabelBMFont* shipValueLabel;
        
        CCLabelBMFont* shipShieldTotalLabel;
        
        CCLabelBMFont* shipShieldValueLabel;
        
        CCLabelBMFont* shipPowerTotalLabel;
        
        CCLabelBMFont* shipPowerValueLabel;
        CCLabelBMFont* shipRepairValueLabel;
        
        CCLabelBMFont* botNameLabel;
        
        CCLabelBMFont* botValueLabel;
        
        int powerTotal; 
        int shieldTotal;
        
        //bot = [[BotConfig alloc] init];
        // NSLog(@"SHIP NAME : %@",[[mySingleton ship1] shipName]);
        
        switch ([mySingleton shipSelected]) {
            default:
            case 1:
                ship = (ShipConfig*)[mySingleton ship1];
                //[ship setPower:300];
                dictionaryPowerShip = [[NSDictionary dictionaryWithDictionary:[mySingleton powerDictionaryShip1]] retain];
                powerTotal = [[dictionaryPowerShip objectForKey:[NSString stringWithFormat:@"%i", [ship shipPowerLevel]]] intValue];
                dictionaryShieldShip = [[NSDictionary dictionaryWithDictionary:[mySingleton shieldDictionaryShip1]] retain];
                shieldTotal = [[dictionaryShieldShip objectForKey:[NSString stringWithFormat:@"%i", [ship shipShieldLevel]]] intValue];
                switch ([mySingleton botSelected]) {
                    default:
                    case 1:
                        bot = [mySingleton bot11];
                        break;
                    case 2:
                        bot = [mySingleton bot12];
                        break;
                    case 3:
                        bot = [mySingleton bot13];
                        break;
                    case 4:
                        bot = [mySingleton bot14];
                        break;
                        
                }
                break;
            case 2:
                
                ship = (ShipConfig*)[mySingleton ship2];
                dictionaryPowerShip = [[NSDictionary dictionaryWithDictionary:[mySingleton powerDictionaryShip2]] retain];
                powerTotal = [[dictionaryPowerShip objectForKey:[NSString stringWithFormat:@"%i", [ship shipPowerLevel]]] intValue];
                dictionaryShieldShip = [[NSDictionary dictionaryWithDictionary:[mySingleton shieldDictionaryShip2]] retain];
                shieldTotal = [[dictionaryShieldShip objectForKey:[NSString stringWithFormat:@"%i", [ship shipShieldLevel]]] intValue];switch ([mySingleton botSelected]) {
                    default:
                    case 1:
                        bot = [mySingleton bot21];
                        break;
                    case 2:
                        bot = [mySingleton bot22];
                        break;
                    case 3:
                        bot = [mySingleton bot23];
                        break;
                    case 4:
                        bot = [mySingleton bot24];
                        break;
                }
                break;
            case 3:
                ship = (ShipConfig*)[mySingleton ship3];
                dictionaryPowerShip = [[NSDictionary dictionaryWithDictionary:[mySingleton powerDictionaryShip3]] retain];
                powerTotal = [[dictionaryPowerShip objectForKey:[NSString stringWithFormat:@"%i", [ship shipPowerLevel]]] intValue];
                dictionaryShieldShip = [[NSDictionary dictionaryWithDictionary:[mySingleton shieldDictionaryShip3]] retain];
                shieldTotal = [[dictionaryShieldShip objectForKey:[NSString stringWithFormat:@"%i", [ship shipShieldLevel]]] intValue];switch ([mySingleton botSelected]) {
                    default:
                    case 1:
                        bot = [mySingleton bot31];
                        break;
                    case 2:
                        bot = [mySingleton bot32];
                        break;
                    case 3:
                        bot = [mySingleton bot33];
                        break;
                    case 4:
                        bot = [mySingleton bot34];
                        break;
                } 
                break;
        }
        [mySingleton setShipConfigSelected:ship];
        
        int scaleFactor;
        float px = 0.0;
        float py = 0.0;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            scaleFactor = 2;
            px = 32.0f;
            py = 64.0f;
        } else {
            scaleFactor = 1;//[[CCDirector sharedDirector] contentScaleFactor];
        }
        
        //Menu back
        SoundMenuItem *back = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt-back.png" selectedSpriteFrameName:@"bt-back-over.png" target:self selector:@selector(goBack)];
        CCMenu *menuBack = [CCMenu menuWithItems: back, nil];
        [menuBack alignItemsVerticallyWithPadding:12.0];
        [menuBack setPosition:ccp((22*scaleFactor)+px,(270*scaleFactor)+py)];
        [self addChild:menuBack z:2];
        
        if([mySingleton shipSelected]==1){
            shipItem1 = [SoundMenuItem itemFromNormalSpriteFrameName:@"ship1-selected.png" selectedSpriteFrameName:@"ship1-selected.png" target:self selector:@selector(selectShip1)];
        }else{
            shipItem1 = [SoundMenuItem itemFromNormalSpriteFrameName:@"ship1.png" selectedSpriteFrameName:@"ship1-selected.png" target:self selector:@selector(selectShip1)];
        }
        
        if([mySingleton isItemUnlocked:@"shipUnlocked2"] ){
            if([mySingleton shipSelected]==2){
                [mySingleton setShipSelected:2];
                [mySingleton setShipConfigSelected:[mySingleton ship2]];
                shipItem2 = [SoundMenuItem itemFromNormalSpriteFrameName:@"ship2-selected.png" selectedSpriteFrameName:@"ship2-selected.png" target:self selector:@selector(selectShip2)];
            }else{
                
                shipItem2 = [SoundMenuItem itemFromNormalSpriteFrameName:@"ship2.png" selectedSpriteFrameName:@"ship2-selected.png" target:self selector:@selector(selectShip2)];  
            }
        }else{
            shipItem2 = [SoundMenuItem itemFromNormalSpriteFrameName:@"ship2-buy.png" selectedSpriteFrameName:@"ship2-buy.png" target:self selector:@selector(buyShip2)];
        }
            
        if([mySingleton isItemUnlocked:@"shipUnlocked3"] ){
            if([mySingleton shipSelected]==3){
                [mySingleton setShipSelected:3];
                [mySingleton setShipConfigSelected:[mySingleton ship3]];
                shipItem3 = [SoundMenuItem itemFromNormalSpriteFrameName:@"ship3-selected.png" selectedSpriteFrameName:@"ship3-selected.png" target:self selector:@selector(selectShip3)];
            }else{
                
                shipItem3 = [SoundMenuItem itemFromNormalSpriteFrameName:@"ship3.png" selectedSpriteFrameName:@"ship3-selected.png" target:self selector:@selector(selectShip3)]; 
            }
        }else{
            shipItem3 = [SoundMenuItem itemFromNormalSpriteFrameName:@"ship3-buy.png" selectedSpriteFrameName:@"ship3-buy.png" target:self selector:@selector(buyShip3)];
        }
        
    
        
        
        CCMenu *shipSelectMenu = [CCMenu menuWithItems: shipItem1, shipItem2,shipItem3, nil];
        [shipSelectMenu alignItemsHorizontallyWithPadding:7.0*scaleFactor];
        [shipSelectMenu setPosition:ccp((124*scaleFactor)+px,(216*scaleFactor)+py)];
        [self addChild:shipSelectMenu z:2];
        
        
        
        //Menu Ship
       /* prevShipItem = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt-rewind.png" selectedSpriteFrameName:@"bt-rewind-over.png" target:self selector:@selector(prevShip)];
        nextShipItem = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt-forward.png" selectedSpriteFrameName:@"bt-forward-over.png" target:self selector:@selector(nextShip)];
        
        CCMenu *shipMenu = [CCMenu menuWithItems: prevShipItem, nextShipItem, nil];
        [shipMenu alignItemsHorizontallyWithPadding:157.0*scaleFactor];
        [shipMenu setPosition:ccp((120*scaleFactor)+px,(230*scaleFactor)+py)];
        [self addChild:shipMenu z:2]; */
        
        //Menu Bots
        prevBotItem = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt-rewind.png" selectedSpriteFrameName:@"bt-rewind-over.png" target:self selector:@selector(prevBot)];
        nextBotItem = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt-forward.png" selectedSpriteFrameName:@"bt-forward-over.png" target:self selector:@selector(nextBot)];
        
        CCMenu *botMenu = [CCMenu menuWithItems: prevBotItem, nextBotItem, nil];
        [botMenu alignItemsHorizontallyWithPadding:148.0*scaleFactor];
        [botMenu setPosition:ccp((361*scaleFactor)+px,(228*scaleFactor)+py)];
        [self addChild:botMenu z:2];
        
        //Ship bg 1
        CCSprite *shipMenuBG = [CCSprite spriteWithSpriteFrameName:@"ship-menu.png"];
        [shipMenuBG setPosition:ccp(winSize.width/2,winSize.height/2)];
        [self addChild:shipMenuBG z:1];
        
        //Ship bg 2
        CCSprite *shipMenuOver = [CCSprite spriteWithSpriteFrameName:@"ship-menu-over.png"];
        [shipMenuOver setPosition:ccp(winSize.width/2,winSize.height/2)];
        [self addChild:shipMenuOver z:1];
        
        //Ship fighter
        CCSprite *shipSprite = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"fighter%i-0001.png",[mySingleton shipSelected]]];
        [shipSprite setPosition:ccp(( 128 * scaleFactor )+px,(winSize.height/3-(6*scaleFactor))+py)];
        [self addChild:shipSprite z:1];
        [shipSprite runAction:[CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:animationFighterRotation restoreOriginalFrame:NO]]];
        
        //bot
        CCSprite *botSprite = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"bot%i-0001.png",[mySingleton botSelected]]];
        [botSprite setPosition:ccp(( 358 * scaleFactor )+px,(winSize.height/2-(32*scaleFactor))+py)];
        [self addChild:botSprite z:1];
        [botSprite runAction:[CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:animationBotRotation restoreOriginalFrame:NO]]];
        
        int repairLevel=[ship shipPowerLevel];
        if([ship shipPowerLevel]==0 && [ship shipPowerLevel]<6){
            repairLevel =  [ship shipPowerLevel];
        }
        if(repairLevel < [ship shipShieldLevel] && [ship shipShieldLevel]<6){
            repairLevel = [ship shipShieldLevel];
        }
        CCLOG(@"SELECTOR POWER VALUE :%@  POWER LEVEL: %i",[NSString stringWithFormat:@"upgradeValue%i",([ship shipPowerLevel]+1)],[ship shipPowerLevel]);
       
        SEL selectorPowerValue = NSSelectorFromString([NSString stringWithFormat:@"upgradeValue%i",([ship shipPowerLevel]+1)]);
        SEL selectorShieldValue = NSSelectorFromString([NSString stringWithFormat:@"upgradeValue%i",([ship shipShieldLevel]+1)]);            
        SEL selectorRepairValue = NSSelectorFromString([NSString stringWithFormat:@"repairValue%i",repairLevel+1]);
        [ship performSelector:selectorPowerValue];
        [ship performSelector:selectorShieldValue];
        [ship performSelector:selectorRepairValue];
        
        shipName = [CCLabelBMFont labelWithString:[ship shipName] fntFile:@"XitRam.fnt"];
        shipValueLabel = [CCLabelBMFont labelWithString:[NSString stringWithFormat:@"%i",[ship shipValue]] fntFile:@"XitRam.fnt"];
        shipPowerTotalLabel = [CCLabelBMFont labelWithString:[NSString stringWithFormat:@"%i / %i",[ship power],powerTotal] fntFile:@"Microgramma.fnt"];
        shipShieldTotalLabel = [CCLabelBMFont labelWithString:[NSString stringWithFormat:@"%i / %i",[ship shield],shieldTotal] fntFile:@"Microgramma.fnt"];
        shipPowerValueLabel = [CCLabelBMFont labelWithString:[NSString stringWithFormat:@"%i",([ship shipPowerLevel])] fntFile:@"XitRam.fnt"];
        shipShieldValueLabel = [CCLabelBMFont labelWithString:[NSString stringWithFormat:@"%i",([ship shipShieldLevel])] fntFile:@"XitRam.fnt"];
        shipRepairValueLabel = [CCLabelBMFont labelWithString:[NSString stringWithFormat:@"%i",repairLevel+1] fntFile:@"XitRam.fnt"];
        botNameLabel = [CCLabelBMFont labelWithString:botName fntFile:@"XitRam.fnt"];
        botValueLabel = [CCLabelBMFont labelWithString:[NSString stringWithFormat:@"%i",[bot botValue]] fntFile:@"XitRam.fnt"];
        myNukesLabel = [CCLabelBMFont labelWithString:[NSString stringWithFormat:@"%i",[mySingleton myNukes]] fntFile:@"XitRam.fnt"];
        myPointsLabel = [CCLabelBMFont labelWithString:[NSString stringWithFormat:@"%i",[mySingleton myPoints]] fntFile:@"XitRam.fnt"];
        
        
       /* [shipName.texture setAliasTexParameters];
        [shipName setAnchorPoint:ccp(0.5f, 0.5f)];
        [self addChild:shipName z:2];
        [shipName setPosition:ccp((120*scaleFactor)+px,(230*scaleFactor)+py)];
        */
        [botNameLabel.texture setAliasTexParameters];
        [botNameLabel setAnchorPoint:ccp(0.5f, 0.5f)];
        [self addChild:botNameLabel z:2];
        [botNameLabel setPosition:ccp((361*scaleFactor)+px,(228*scaleFactor)+py)];
        
        [botValueLabel.texture setAliasTexParameters];
        [self addChild:botValueLabel z:2];
        [botValueLabel setPosition:ccp((304*scaleFactor)+px,(200*scaleFactor)+py)];
        
        CCSprite* nukesIcon = [CCSprite spriteWithSpriteFrameName:@"nukes.png"];
        [self addChild:nukesIcon z:3];
        [nukesIcon setPosition:ccp( ( 256 * scaleFactor ) + px , ( 36* scaleFactor ) + py )];
        
        CCSprite* pointsIcon = [CCSprite spriteWithSpriteFrameName:@"points.png"];
        [self addChild:pointsIcon z:3];
        [pointsIcon setPosition:ccp( ( 256 * scaleFactor ) + px , ( 16* scaleFactor ) + py )];
        
        [myNukesLabel.texture setAliasTexParameters];
        [myNukesLabel setAnchorPoint:ccp(0.0f, 0.5f)];
        [self addChild:myNukesLabel z:2];
        [myNukesLabel setPosition:ccp( ( 270 * scaleFactor ) + px , ( 36* scaleFactor ) + py )];
        
        [myPointsLabel.texture setAliasTexParameters];
        [myPointsLabel setAnchorPoint:ccp(0.0f, 0.5f)];
        [self addChild:myPointsLabel z:2];
        [myPointsLabel setPosition:ccp( ( 270 * scaleFactor ) + px , ( 16 * scaleFactor ) + py )];
        
        CCSprite* powerLevelSprite = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"power-l%i.png",[ship shipPowerLevel]]];
        [powerLevelSprite setPosition:ccp(( 70 * scaleFactor ) + px,( 61 * scaleFactor ) + py )];
        [self addChild:powerLevelSprite z:2];
        
        CCSprite* shieldLevelSprite = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"shield-l%i.png",[ship shipShieldLevel]]];
        [shieldLevelSprite setPosition:ccp(( 174 * scaleFactor ) + px,( 61 * scaleFactor ) + py )];
        [self addChild:shieldLevelSprite z:2];
        
        //POWER
        NSNumber* powerPercent;
        powerPercent = [NSNumber numberWithFloat:( float( [ship power] ) / float( powerTotal ) ) * 100 ];
        int powerPercentInt = [[NSNumber numberWithInt:( [powerPercent intValue] - ( [powerPercent intValue] % 10 ) ) / 10 ] intValue];
        
        if( [powerPercent intValue] > 0 ){
            CCSprite* powerSprite = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"power%i.png",powerPercentInt]];
            [powerSprite setPosition:ccp(( 21 * scaleFactor )+px,(115*scaleFactor)+py)];
            [self addChild:powerSprite z:2];
        }
        
        [shipPowerTotalLabel.texture setAliasTexParameters];
        [shipPowerTotalLabel setAnchorPoint:ccp(0.0f, 0.5f)];
        [self addChild:shipPowerTotalLabel z:2];
        [shipPowerTotalLabel setPosition:ccp(( 34 * scaleFactor)+px,( 174 * scaleFactor)+py)];
        
        //SHIELD
        NSNumber* shieldPercent;
        shieldPercent = [NSNumber numberWithFloat:( float( [ship shield] ) / float( shieldTotal ) ) * 100 ];
        shieldPercent = [NSNumber numberWithInt:([shieldPercent intValue]-([shieldPercent intValue]%10))/10];
        
        if([shieldPercent intValue]>0){
            CCSprite* shieldSprite = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"shield%i.png",[shieldPercent intValue]]];
            [shieldSprite setPosition:ccp(( 222 * scaleFactor )+px,(115*scaleFactor)+py)];
            [self addChild:shieldSprite z:2];
        }
        
        [shipShieldTotalLabel.texture setAliasTexParameters];
        [shipShieldTotalLabel setAnchorPoint:ccp(1.0f, 0.5f)];
        [self addChild:shipShieldTotalLabel z:2];
        [shipShieldTotalLabel setPosition:ccp(( 208 * scaleFactor )+px,( 174 * scaleFactor)+py)];
        
        //Buy menu and repair/upgrade menu
        if( ([mySingleton shipSelected]==1 && [mySingleton isItemUnlocked:@"shipUnlocked1"]) || 
           ([mySingleton shipSelected]==2 && [mySingleton isItemUnlocked:@"shipUnlocked2"]) || 
           ([mySingleton shipSelected]==3 && [mySingleton isItemUnlocked:@"shipUnlocked3"]) ){
            
            //repair ship
            SoundMenuItem *updatePowerBt = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt-upgrade-power.png" selectedSpriteFrameName:@"bt-upgrade-power-over.png" target:self selector:@selector(upgradePower)]; 
            [shipPowerValueLabel setAnchorPoint:ccp(0.0f, 0.5f)];
            [updatePowerBt addChild:shipPowerValueLabel];
            [shipPowerValueLabel setPosition:ccp((22*scaleFactor),(12*scaleFactor))];
            
            SoundMenuItem *repairBt = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt-repair.png" selectedSpriteFrameName:@"bt-repair-over.png" target:self selector:@selector(repairShip)]; 
            [shipRepairValueLabel setAnchorPoint:ccp(0.0f, 0.5f)];
            [repairBt addChild:shipRepairValueLabel];
            [shipRepairValueLabel setPosition:ccp((24*scaleFactor),(12*scaleFactor))];
            
            SoundMenuItem *updateShieldBt = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt-upgrade-shield.png" selectedSpriteFrameName:@"bt-upgrade-shield-over.png" target:self selector:@selector(upgradeShield)];
            
            [shipShieldValueLabel setAnchorPoint:ccp(0.0f, 0.5f)];
            [updateShieldBt addChild:shipShieldValueLabel];
            [shipShieldValueLabel setPosition:ccp((22*scaleFactor),(12*scaleFactor))];
            
            if([ship shipShieldLevel]<5 && [ship shipPowerLevel]<5){
                
            }else if([ship shipShieldLevel]==5 && [ship shipPowerLevel]==5 ){
                
                [updatePowerBt setIsEnabled:NO];
                [updateShieldBt setIsEnabled:NO];
            }else if([ship shipShieldLevel]==5){
                [updateShieldBt setIsEnabled:NO];
                
            }else if([ship shipPowerLevel]==5 ){
                [updatePowerBt setIsEnabled:NO];
            }
            CCMenu *menuUpgrade = [CCMenu menuWithItems: updatePowerBt,repairBt,updateShieldBt, nil];
            [menuUpgrade alignItemsHorizontallyWithPadding:1.0];
            [menuUpgrade setPosition:ccp((122*scaleFactor)+px,(26*scaleFactor)+py)];
            [self addChild:menuUpgrade z:2];
            
            
            //Bot add/remove menu
            CCMenu *menuBot;
            if([ship shipPowerLevel]==0 && [ship shipShieldLevel]==0){
                SoundMenuItem *bot11bt;
                if([ship bot1]==0){
                    bot11bt = [SoundMenuItem itemFromNormalSpriteFrameName:@"slot1-add.png" selectedSpriteFrameName:@"slot1-add.png" target:self selector:@selector(addBot1)];
                }else{
                    bot11bt = [SoundMenuItem itemFromNormalSpriteFrameName:@"slot1-remove.png" selectedSpriteFrameName:@"slot1-remove.png" target:self selector:@selector(removeBot1)];
                    CCSprite* botSprite = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"bot%i-0019.png",[ship bot1]]];
                    [bot11bt addChild:botSprite z:1];
                    [botSprite setPosition:ccp( 100 * scaleFactor , 30 * scaleFactor )];
                }
                menuBot = [CCMenu menuWithItems: bot11bt, nil];
                [menuBot alignItemsHorizontallyWithPadding:1.0];
                [menuBot setPosition:ccp((360*scaleFactor)+px,(87*scaleFactor)+py)];
                [self addChild:menuBot z:2];
            }
            
            if(([ship shipPowerLevel]==1 || [ship shipShieldLevel]==1 || [ship shipPowerLevel]==2 || [ship shipShieldLevel]==2) && ([ship shipPowerLevel]<3 && [ship shipShieldLevel]<3)){
                SoundMenuItem *bot12bt;
                SoundMenuItem *bot22bt;
                
                if([ship bot1]==0){
                    bot12bt = [SoundMenuItem itemFromNormalSpriteFrameName:@"slot1-2-add.png" selectedSpriteFrameName:@"slot1-2-add.png" target:self selector:@selector(addBot1)];
                }else{
                    bot12bt = [SoundMenuItem itemFromNormalSpriteFrameName:@"slot1-2-remove.png" selectedSpriteFrameName:@"slot1-2-remove.png" target:self selector:@selector(removeBot1)    ];
                    CCSprite* botSprite = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"bot%i-0019.png",[ship bot1]]];
                    [bot12bt addChild:botSprite z:1];
                    [botSprite setPosition:ccp(55*scaleFactor,30*scaleFactor)];
                }
                
                if([ship bot2]==0){
                    bot22bt = [SoundMenuItem itemFromNormalSpriteFrameName:@"slot2-2-add.png" selectedSpriteFrameName:@"slot2-2-add.png" target:self selector:@selector(addBot2)];
                }else{
                    bot22bt = [SoundMenuItem itemFromNormalSpriteFrameName:@"slot2-2-remove.png" selectedSpriteFrameName:@"slot2-2-remove.png" target:self selector:@selector(removeBot2)];
                    CCSprite* botSprite = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"bot%i-0019.png",[ship bot2]]];
                    [bot22bt addChild:botSprite z:1];
                    [botSprite setPosition:ccp(55*scaleFactor,30*scaleFactor)];
                }
                
                menuBot = [CCMenu menuWithItems: bot12bt, bot22bt, nil];
                [menuBot alignItemsHorizontallyWithPadding:1.0];
                [menuBot setPosition:ccp((360*scaleFactor)+px,(87*scaleFactor)+py)];
                [self addChild:menuBot z:2];
            }
            
            
            if(([ship shipPowerLevel]==3 || [ship shipShieldLevel]==3) && ([ship shipPowerLevel]<4 && [ship shipShieldLevel]<4) ){
                SoundMenuItem *bot13bt;
                SoundMenuItem *bot23bt;
                SoundMenuItem *bot33bt;
                
                if([ship bot1]==0){
                    bot13bt = [SoundMenuItem itemFromNormalSpriteFrameName:@"slot1-3-add.png" selectedSpriteFrameName:@"slot1-3-add.png" target:self selector:@selector(addBot1)];
                }else{
                    bot13bt = [SoundMenuItem itemFromNormalSpriteFrameName:@"slot1-3-remove.png" selectedSpriteFrameName:@"slot1-3-remove.png" target:self selector:@selector(removeBot1)    ];
                    CCSprite* botSprite = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"bot%i-0019.png",[ship bot1]]];
                    [bot13bt addChild:botSprite z:1];
                    [botSprite setPosition:ccp(31*scaleFactor,30*scaleFactor)];
                }
                
                if([ship bot2]==0){
                    bot23bt = [SoundMenuItem itemFromNormalSpriteFrameName:@"slot2-3-add.png" selectedSpriteFrameName:@"slot2-3-add.png" target:self selector:@selector(addBot2)];
                }else{
                    bot23bt = [SoundMenuItem itemFromNormalSpriteFrameName:@"slot2-3-remove.png" selectedSpriteFrameName:@"slot2-3-remove.png" target:self selector:@selector(removeBot2)];
                    CCSprite* botSprite = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"bot%i-0019.png",[ship bot2]]];
                    [bot23bt addChild:botSprite z:1];
                    [botSprite setPosition:ccp(31*scaleFactor,30*scaleFactor)];
                }
                if([ship bot3]==0){
                    bot33bt = [SoundMenuItem itemFromNormalSpriteFrameName:@"slot3-3-add.png" selectedSpriteFrameName:@"slot3-3-add.png" target:self selector:@selector(addBot3)];
                }else{
                    bot33bt = [SoundMenuItem itemFromNormalSpriteFrameName:@"slot3-3-remove.png" selectedSpriteFrameName:@"slot3-3-remove.png" target:self selector:@selector(removeBot3)];
                    CCSprite* botSprite = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"bot%i-0019.png",[ship bot3]]];
                    [bot33bt addChild:botSprite z:1];
                    [botSprite setPosition:ccp(31*scaleFactor,30*scaleFactor)];
                }
                
                menuBot = [CCMenu menuWithItems: bot13bt, bot23bt, bot33bt, nil];
                [menuBot alignItemsHorizontallyWithPadding:1.0];
                [menuBot setPosition:ccp((360*scaleFactor)+px,(87*scaleFactor)+py)];
                [self addChild:menuBot z:2];
            }
            
            
            if([ship shipPowerLevel]>=4 || [ship shipShieldLevel]>=4){
                SoundMenuItem *bot14bt;
                SoundMenuItem *bot24bt;
                SoundMenuItem *bot34bt;
                SoundMenuItem *bot44bt;
                
                if([ship bot1]==0){
                    bot14bt = [SoundMenuItem itemFromNormalSpriteFrameName:@"slot1-4-add.png" selectedSpriteFrameName:@"slot1-4-add.png" target:self selector:@selector(addBot1)];
                }else{
                    bot14bt = [SoundMenuItem itemFromNormalSpriteFrameName:@"slot1-4-remove.png" selectedSpriteFrameName:@"slot1-4-remove.png" target:self selector:@selector(removeBot1)    ];
                    CCSprite* botSprite = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"bot%i-0019.png",[ship bot1]]];
                    [bot14bt addChild:botSprite z:1];
                    [botSprite setPosition:ccp(25*scaleFactor,30*scaleFactor)];
                }
                
                if([ship bot2]==0){
                    bot24bt = [SoundMenuItem itemFromNormalSpriteFrameName:@"slot2-4-add.png" selectedSpriteFrameName:@"slot2-4-add.png" target:self selector:@selector(addBot2)];
                }else{
                    bot24bt = [SoundMenuItem itemFromNormalSpriteFrameName:@"slot2-4-remove.png" selectedSpriteFrameName:@"slot2-4-remove.png" target:self selector:@selector(removeBot2)];
                    CCSprite* botSprite = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"bot%i-0019.png",[ship bot2]]];
                    [bot24bt addChild:botSprite z:1];
                    [botSprite setPosition:ccp(25*scaleFactor,30*scaleFactor)];
                }
                if([ship bot3]==0){
                    bot34bt = [SoundMenuItem itemFromNormalSpriteFrameName:@"slot2-4-add.png" selectedSpriteFrameName:@"slot2-4-add.png" target:self selector:@selector(addBot3)];
                }else{
                    bot34bt = [SoundMenuItem itemFromNormalSpriteFrameName:@"slot2-4-remove.png" selectedSpriteFrameName:@"slot2-4-remove.png" target:self selector:@selector(removeBot3)];
                    CCSprite* botSprite = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"bot%i-0019.png",[ship bot3]]];
                    [bot34bt addChild:botSprite z:1];
                    [botSprite setPosition:ccp(25*scaleFactor,30*scaleFactor)];
                }
                if([ship bot4]==0){
                    bot44bt = [SoundMenuItem itemFromNormalSpriteFrameName:@"slot4-4-add.png" selectedSpriteFrameName:@"slot4-4-add.png" target:self selector:@selector(addBot4)];
                }else{
                    bot44bt = [SoundMenuItem itemFromNormalSpriteFrameName:@"slot4-4-remove.png" selectedSpriteFrameName:@"slot4-4-remove.png" target:self selector:@selector(removeBot4)];
                    CCSprite* botSprite = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"bot%i-0019.png",[ship bot4]]];
                    [bot44bt addChild:botSprite z:1];
                    [botSprite setPosition:ccp(25*scaleFactor,30*scaleFactor)];
                }
                menuBot = [CCMenu menuWithItems: bot14bt, bot24bt, bot34bt, bot44bt, nil];
                [menuBot alignItemsHorizontallyWithPadding:1.0];
                [menuBot setPosition:ccp((360*scaleFactor)+px,(87*scaleFactor)+py)];
                [self addChild:menuBot z:2];                
            }
            
            SoundMenuItem *selectLevel = [SoundMenuItem itemFromNormalSpriteFrameName:@"play.png" selectedSpriteFrameName:@"play-selected.png" target:self selector:@selector(goMission)]; 
            
            CCMenu *menuGo = [CCMenu menuWithItems: selectLevel, nil];
            [menuGo alignItemsVerticallyWithPadding:12.0];
            [menuGo setPosition:ccp((420*scaleFactor)+px,(23*scaleFactor)+py)];
            [self addChild:menuGo z:2];
            
        }else if(//([mySingleton shipSelected]==1 && ![mySingleton isItemUnlocked:@"shipUnlocked1"]) || 
                 //buy ship
                 ([mySingleton shipSelected]==2 && ![mySingleton isItemUnlocked:@"shipUnlocked2"]) || 
                 ([mySingleton shipSelected]==3 && ![mySingleton isItemUnlocked:@"shipUnlocked3"]) ){
            
            SoundMenuItem *buyNowBt = [SoundMenuItem itemFromNormalSpriteFrameName:@"buy-ship.png" selectedSpriteFrameName:@"buy-ship-over.png" target:self selector:@selector(buyShip)];
            
            CCMenu *menuBuyNow = [CCMenu menuWithItems: buyNowBt, nil];
            [menuBuyNow alignItemsVerticallyWithPadding:12.0];
            [menuBuyNow setPosition:ccp((122*scaleFactor)+px,(22*scaleFactor)+py)];
            [self addChild:menuBuyNow z:2];
            [buyNowBt addChild:shipValueLabel];
            [shipValueLabel setPosition:ccp(60,20)];
            
        }
        [mySingleton saveShip:ship forKey:[mySingleton shipSelected]];

        //Display Message, you must buy a ship to start the game
        if(([mySingleton shipSelected]==1 && ![mySingleton isItemUnlocked:@"shipUnlocked1"])){
            
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Welcome!" 
                                                            message:@"You must buy a ship to help the Intergalatic Confederation defend Solar System from Reptilians." 
                                                           delegate:self 
                                                  cancelButtonTitle:@"OK" 
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
    }
    return self;
}

-(void)buyShip{
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    //if(){ has certificate
    
    if([mySingleton myNukes]>=[ship shipValue] ){
        if([mySingleton shipSelected]==2)[mySingleton unlockItem:@"shipUnlocked2"];
        if([mySingleton shipSelected]==3)[mySingleton unlockItem:@"shipUnlocked3"];
        [self subtractMyNukes:[ship shipValue]];
        [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ShopLayer scene]]];
    }else{
        int difference = [ship shipValue] - [mySingleton myNukes];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ops!" 
                                                        message:[NSString stringWithFormat:@"You need extra %i nukes to buy this ship. Go back and fight again, click on PLAY!",difference] 
                                                       delegate:self 
                                              cancelButtonTitle:@"OK" 
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
    
    
}

-(void)buyShip2{
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    if([mySingleton myNukes]>=[[mySingleton ship2] shipValue] ){
        [mySingleton unlockItem:@"shipUnlocked2"];
        [mySingleton setShipSelected:2];
        [self subtractMyNukes:[ship shipValue]];
        [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ShopLayer scene]]];
    }else{
        int oldSelected = [mySingleton shipSelected];
        int difference = [[mySingleton ship2] shipValue] - [mySingleton myNukes];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ops!" 
                                                        message:[NSString stringWithFormat:@"You need extra %i nukes to buy this ship. Go back and fight again, click on PLAY!",difference] 
                                                       delegate:self 
                                              cancelButtonTitle:@"OK" 
                                              otherButtonTitles:nil];
        [mySingleton setShipSelected:oldSelected];
        
        [alert show];
        [alert release];
    }
    
    
}
-(void)buyShip3{
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    if([mySingleton myNukes]>=[[mySingleton ship3] shipValue] ){
        [mySingleton unlockItem:@"shipUnlocked3"];
        [mySingleton setShipSelected:3];
        [self subtractMyNukes:[ship shipValue]];
        [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ShopLayer scene]]];
    }else{
        int oldSelected = [mySingleton shipSelected];
        int difference = [[mySingleton ship3] shipValue] - [mySingleton myNukes];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ops!" 
                                                        message:[NSString stringWithFormat:@"You need extra %i nukes to buy this ship. Go back and fight again, click on PLAY!",difference] 
                                                       delegate:self 
                                              cancelButtonTitle:@"OK" 
                                              otherButtonTitles:nil];
        [mySingleton setShipSelected:oldSelected];
        
        [alert show];
        [alert release];
    }
    
    
}

-(void)addBot1{
    GlobalSingleton* mySingleton = [GlobalSingleton sharedInstance];
    if([mySingleton myNukes]>=[bot botValue]){
        [self subtractMyNukes:[bot botValue]];
        [ship setBot1:[mySingleton botSelected]];
        [mySingleton saveShip:ship forKey:[mySingleton shipSelected]];
        [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ShopLayer scene]]];
        [[GlobalSingleton sharedInstance] intPref:[[GlobalSingleton sharedInstance] myNukes] forKey:@"myNukes"];
    }else{
        int difference = [bot botValue] - [mySingleton myNukes];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ops!" 
                                                        message:[NSString stringWithFormat:@"You need extra %i nukes to buy this bot. Go back and fight again, click on PLAY!",difference] 
                                                       delegate:self 
                                              cancelButtonTitle:@"OK" 
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }    
}

-(void)removeBot1{
    GlobalSingleton* mySingleton = [GlobalSingleton sharedInstance];
    if([mySingleton shipSelected]==1){
        if([ship bot1]==1){
            [self addMyNukes:[[mySingleton bot11] botValue]];
        }else  if([ship bot1]==2){
            [self addMyNukes:[[mySingleton bot12] botValue]];
        }else  if([ship bot1]==3){
            [self addMyNukes:[[mySingleton bot13] botValue]];
        }else  if([ship bot1]==4){
            [self addMyNukes:[[mySingleton bot14] botValue]];
        }
    }else if([mySingleton shipSelected]==2){
        if([ship bot1]==1){
            [self addMyNukes:[[mySingleton bot21] botValue]];
        }else  if([ship bot1]==2){
            [self addMyNukes:[[mySingleton bot22] botValue]];
        }else  if([ship bot1]==3){
            [self addMyNukes:[[mySingleton bot23] botValue]];
        }else  if([ship bot1]==4){
            [self addMyNukes:[[mySingleton bot24] botValue]];
        }
    }else if([mySingleton shipSelected]==3){
        if([ship bot1]==1){
            [self addMyNukes:[[mySingleton bot31] botValue]];
        }else  if([ship bot1]==2){
            [self addMyNukes:[[mySingleton bot32] botValue]];
        }else  if([ship bot1]==3){
            [self addMyNukes:[[mySingleton bot33] botValue]];
        }else  if([ship bot1]==4){
            [self addMyNukes:[[mySingleton bot34] botValue]];
        }
    }
    [ship setBot1:0];
    [[GlobalSingleton sharedInstance] intPref:[[GlobalSingleton sharedInstance] myNukes] forKey:@"myNukes"];

    [mySingleton saveShip:ship forKey:[mySingleton shipSelected]];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ShopLayer scene]]];
}
-(void)addBot2{
    GlobalSingleton* mySingleton = [GlobalSingleton sharedInstance];
    if([mySingleton myNukes]>=[bot botValue]){
        [self subtractMyNukes:[bot botValue]];
        [ship setBot2:[mySingleton botSelected]];
        [mySingleton saveShip:ship forKey:[mySingleton shipSelected]];
        [[GlobalSingleton sharedInstance] intPref:[[GlobalSingleton sharedInstance] myNukes] forKey:@"myNukes"];

        [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ShopLayer scene]]];
    }else{
        int difference = [bot botValue] - [mySingleton myNukes];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ops!" 
                                                        message:[NSString stringWithFormat:@"You need extra %i nukes to buy this bot. Go back and fight again, click on PLAY!",difference] 
                                                       delegate:self 
                                              cancelButtonTitle:@"OK" 
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}
-(void)removeBot2{
    GlobalSingleton* mySingleton = [GlobalSingleton sharedInstance];
    if([mySingleton shipSelected]==1){
        if([ship bot2]==1){
            [self addMyNukes:[[mySingleton bot11] botValue]];
        }else  if([ship bot2]==2){
            [self addMyNukes:[[mySingleton bot12] botValue]];
        }else  if([ship bot2]==3){
            [self addMyNukes:[[mySingleton bot13] botValue]];
        }else  if([ship bot2]==4){
            [self addMyNukes:[[mySingleton bot14] botValue]];
        }
    }else if([mySingleton shipSelected]==2){
        if([ship bot2]==1){
            [self addMyNukes:[[mySingleton bot21] botValue]];
        }else  if([ship bot2]==2){
            [self addMyNukes:[[mySingleton bot22] botValue]];
        }else  if([ship bot2]==3){
            [self addMyNukes:[[mySingleton bot23] botValue]];
        }else  if([ship bot2]==4){
            [self addMyNukes:[[mySingleton bot24] botValue]];
        }
    }else if([mySingleton shipSelected]==3){
        if([ship bot2]==1){
            [self addMyNukes:[[mySingleton bot31] botValue]];
        }else  if([ship bot2]==2){
            [self addMyNukes:[[mySingleton bot32] botValue]];
        }else  if([ship bot2]==3){
            [self addMyNukes:[[mySingleton bot33] botValue]];
        }else  if([ship bot2]==4){
            [self addMyNukes:[[mySingleton bot34] botValue]];
        }
    }
    [ship setBot2:0];
    [[GlobalSingleton sharedInstance] intPref:[[GlobalSingleton sharedInstance] myNukes] forKey:@"myNukes"];

    [mySingleton saveShip:ship forKey:[mySingleton shipSelected]];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ShopLayer scene]]];
}
-(void)addBot3{
    GlobalSingleton* mySingleton = [GlobalSingleton sharedInstance];
    if([mySingleton myNukes]>=[bot botValue]){
        [self subtractMyNukes:[bot botValue]];
        [[GlobalSingleton sharedInstance] intPref:[[GlobalSingleton sharedInstance] myNukes] forKey:@"myNukes"];

        [ship setBot3:[mySingleton botSelected]];
        [mySingleton saveShip:ship forKey:[mySingleton shipSelected]];
        [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ShopLayer scene]]];
    }else{
        int difference = [bot botValue] - [mySingleton myNukes];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ops!" 
                                                        message:[NSString stringWithFormat:@"You need extra %i nukes to buy this bot. Go back and fight again, click on PLAY!",difference] 
                                                       delegate:self 
                                              cancelButtonTitle:@"OK" 
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}
-(void)removeBot3{
    GlobalSingleton* mySingleton = [GlobalSingleton sharedInstance];
    if([mySingleton shipSelected]==1){
        if([ship bot3]==1){
            [self addMyNukes:[[mySingleton bot11] botValue]];
        }else  if([ship bot3]==2){
            [self addMyNukes:[[mySingleton bot12] botValue]];
        }else  if([ship bot3]==3){
            [self addMyNukes:[[mySingleton bot13] botValue]];
        }else  if([ship bot3]==4){
            [self addMyNukes:[[mySingleton bot14] botValue]];
        }
    }else if([mySingleton shipSelected]==2){
        if([ship bot3]==1){
            [self addMyNukes:[[mySingleton bot21] botValue]];
        }else  if([ship bot3]==2){
            [self addMyNukes:[[mySingleton bot22] botValue]];
        }else  if([ship bot3]==3){
            [self addMyNukes:[[mySingleton bot23] botValue]];
        }else  if([ship bot3]==4){
            [self addMyNukes:[[mySingleton bot24] botValue]];
        }
    }else if([mySingleton shipSelected]==3){
        if([ship bot3]==1){
            [self addMyNukes:[[mySingleton bot31] botValue]];
        }else  if([ship bot3]==2){
            [self addMyNukes:[[mySingleton bot32] botValue]];
        }else  if([ship bot3]==3){
            [self addMyNukes:[[mySingleton bot33] botValue]];
        }else  if([ship bot3]==4){
            [self addMyNukes:[[mySingleton bot34] botValue]];
        }
    }
    [[GlobalSingleton sharedInstance] intPref:[[GlobalSingleton sharedInstance] myNukes] forKey:@"myNukes"];

    [ship setBot3:0];
    [mySingleton saveShip:ship forKey:[mySingleton shipSelected]];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ShopLayer scene]]];
}
-(void)addBot4{
    GlobalSingleton* mySingleton = [GlobalSingleton sharedInstance];
    if([mySingleton myNukes]>=[bot botValue]){
        [self subtractMyNukes:[bot botValue]];
        [ship setBot4:[mySingleton botSelected]];
        [[GlobalSingleton sharedInstance] intPref:[[GlobalSingleton sharedInstance] myNukes] forKey:@"myNukes"];

        [mySingleton saveShip:ship forKey:[mySingleton shipSelected]];
        [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ShopLayer scene]]];
    }else{
        int difference = [bot botValue] - [mySingleton myNukes];
        UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ops!" 
                                                        message:[NSString stringWithFormat:@"You need extra %i nukes to buy this bot. Go back and fight again, click on PLAY!",difference] 
                                                       delegate:self 
                                              cancelButtonTitle:@"OK" 
                                              otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}
-(void)removeBot4{
    GlobalSingleton* mySingleton = [GlobalSingleton sharedInstance];
    if([mySingleton shipSelected]==1){
        if([ship bot4]==1){
            [self addMyNukes:[[mySingleton bot11] botValue]];
        }else  if([ship bot4]==2){
            [self addMyNukes:[[mySingleton bot12] botValue]];
        }else  if([ship bot4]==3){
            [self addMyNukes:[[mySingleton bot13] botValue]];
        }else  if([ship bot4]==4){
            [self addMyNukes:[[mySingleton bot14] botValue]];
        }
    }else if([mySingleton shipSelected]==2){
        if([ship bot4]==1){
            [self addMyNukes:[[mySingleton bot21] botValue]];
        }else  if([ship bot4]==2){
            [self addMyNukes:[[mySingleton bot22] botValue]];
        }else  if([ship bot4]==3){
            [self addMyNukes:[[mySingleton bot23] botValue]];
        }else  if([ship bot4]==4){
            [self addMyNukes:[[mySingleton bot24] botValue]];
        }
    }else if([mySingleton shipSelected]==3){
        if([ship bot4]==1){
            [self addMyNukes:[[mySingleton bot31] botValue]];
        }else  if([ship bot4]==2){
            [self addMyNukes:[[mySingleton bot32] botValue]];
        }else  if([ship bot4]==3){
            [self addMyNukes:[[mySingleton bot33] botValue]];
        }else  if([ship bot4]==4){
            [self addMyNukes:[[mySingleton bot34] botValue]];
        }
    }
    [[GlobalSingleton sharedInstance] intPref:[[GlobalSingleton sharedInstance] myNukes] forKey:@"myNukes"];

    [ship setBot4:0];
    [mySingleton saveShip:ship forKey:[mySingleton shipSelected]];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ShopLayer scene]]];
}

-(void)upgradePower{
    GlobalSingleton* mySingleton = [GlobalSingleton sharedInstance];
    SEL selector = NSSelectorFromString([NSString stringWithFormat:@"upgradeValue%i",([ship shipPowerLevel]+1)]);
    if([ship respondsToSelector:selector]){
        if((int)(NSInteger)[ship performSelector:selector]<=[mySingleton myNukes]){
            
            [ship setShipPowerLevel:[ship shipPowerLevel]+1];
            int powerTotal = [[dictionaryPowerShip objectForKey:[NSString stringWithFormat:@"%i", [ship shipPowerLevel]]] intValue];
            [ship setPower:powerTotal];
            [self subtractMyNukes:(int)(NSInteger)[ship performSelector:selector]];
            //SEL selectorDictionary = NSSelectorFromString([NSString stringWithFormat:@"powerDictionaryShip%i",[mySingleton shipSelected]]);
            //if([mySingleton respondsToSelector:selectorDictionary])[ship setPower:(int)[mySingleton performSelector:selectorDictionary]];
            [mySingleton saveShip:ship forKey:[mySingleton shipSelected]];
            [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ShopLayer scene]]];
        }else{
            //alert no money, get to work vagabunds
            int difference = (int)(NSInteger)[ship performSelector:selector] - [mySingleton myNukes];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ops!" 
                                                            message:[NSString stringWithFormat:@"You need extra %i nukes to buy this power upgrade. Go back and fight again, click on PLAY!",difference] 
                                                           delegate:self 
                                                  cancelButtonTitle:@"OK" 
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
    }else{
        //no selector
        CCLOG(@"Ship not responding to selector %@", selector);
    }
}

-(void)upgradeShield{
    GlobalSingleton* mySingleton = [GlobalSingleton sharedInstance];
    SEL selector = NSSelectorFromString([NSString stringWithFormat:@"upgradeValue%i",([ship shipShieldLevel]+1)]);
    if([ship respondsToSelector:selector]){
        if((int)(NSInteger)[ship performSelector:selector]<=[mySingleton myNukes]){
            
            [ship setShipShieldLevel:[ship shipShieldLevel]+1];
            int shieldTotal = [[dictionaryShieldShip objectForKey:[NSString stringWithFormat:@"%i", [ship shipShieldLevel]]] intValue];
            [ship setShield:shieldTotal];
            
            [self subtractMyNukes:(int)(NSInteger)[ship performSelector:selector]];
            //SEL selectorDictionary = NSSelectorFromString([NSString stringWithFormat:@"shieldDictionaryShip%i",[mySingleton shipSelected]]);
            //if([mySingleton respondsToSelector:selectorDictionary])[ship setShield:(int)[mySingleton performSelector:selectorDictionary]];
            [mySingleton saveShip:ship forKey:[mySingleton shipSelected]];
            [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ShopLayer scene]]];
        }else{
            //alert no money, get to work vagabunds
            int difference = (int)(NSInteger)[ship performSelector:selector] - [mySingleton myNukes];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ops!" 
                                                            message:[NSString stringWithFormat:@"You need extra %i nukes to buy this shield upgrade. Go back and fight again, click on PLAY!",difference] 
                                                           delegate:self 
                                                  cancelButtonTitle:@"OK" 
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
    }else{
        //no selector
        CCLOG(@"Ship not responding to selector %@", selector);
    }
    
}
-(void)repairShipToMin{
     [ship setPower:4];
}

-(void)repairShip{
    GlobalSingleton* mySingleton = [GlobalSingleton sharedInstance];
    int repairLevel=[ship shipPowerLevel];
    if([ship shipPowerLevel]==0 && [ship shipPowerLevel]<6){
        repairLevel =  [ship shipPowerLevel];
    }
    if(repairLevel < [ship shipShieldLevel] && [ship shipShieldLevel]<6){
        repairLevel = [ship shipShieldLevel];
    }
    SEL selector = NSSelectorFromString([NSString stringWithFormat:@"repairValue%i",repairLevel+1]);
    if([ship respondsToSelector:selector]){
        if((int)(NSInteger)[ship performSelector:selector]<=[mySingleton myNukes]){
            
            int shieldTotal = [[dictionaryShieldShip objectForKey:[NSString stringWithFormat:@"%i", [ship shipShieldLevel]]] intValue];
            
            int powerTotal = [[dictionaryPowerShip objectForKey:[NSString stringWithFormat:@"%i", [ship shipPowerLevel]]] intValue];
            if([ship power]==powerTotal && [ship shield]==shieldTotal){
                int difference = (int)(NSInteger)[ship performSelector:selector] - [mySingleton myNukes];
                UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ops!" 
                                                                message:@"You don't need to repair your ship. Go back and fight again, click on PLAY!"
                                                               delegate:self 
                                                      cancelButtonTitle:@"OK" 
                                                      otherButtonTitles:nil];
                [alert show];
                [alert release];
            }else{
                [ship setPower:powerTotal];
                [ship setShield:shieldTotal];
                
                [self subtractMyNukes:(int)(NSInteger)[ship performSelector:selector]];
                [mySingleton saveShip:ship forKey:[mySingleton shipSelected]];
                [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ShopLayer scene]]];
                
            }
        }else{
            //alert no money, get to work vagabunds
            int difference = (int)(NSInteger)[ship performSelector:selector] - [mySingleton myNukes];
            UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ops!" 
                                                            message:[NSString stringWithFormat:@"You need extra %i nukes to buy this ship repair. Go back and fight again, click on PLAY!",difference] 
                                                           delegate:self 
                                                  cancelButtonTitle:@"OK" 
                                                  otherButtonTitles:nil];
            [alert show];
            [alert release];
        }
    }else{
        //no selector
        CCLOG(@"Ship not responding to selector %@", selector);
    }
    
}

- (void)subtractMyNukes:(int)value {
    
    GlobalSingleton* mySingleton = [GlobalSingleton sharedInstance];
    [mySingleton setMyNukes:[mySingleton myNukes]-value];
    [myNukesLabel setString:[NSString stringWithFormat:@"%i",[mySingleton myNukes]]];
    
}
- (void)addMyNukes:(int)value {
    
    GlobalSingleton* mySingleton = [GlobalSingleton sharedInstance];
    [mySingleton setMyNukes:[mySingleton myNukes]+value];
    [myNukesLabel setString:[NSString stringWithFormat:@"%i",[mySingleton myNukes]]];
    
}


- (void)setupBatchNode {
    
    NSString *hudPvrCcz = @"hud.pvr.ccz";
    NSString *hudPlist = @"hud.plist";
    
    _batchNode = [CCSpriteBatchNode batchNodeWithFile:hudPvrCcz];
    [self addChild:_batchNode z:-1];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:hudPlist];
      
}

- (void)setupBatchNodeFighter {
    
    GlobalSingleton*mySingleton = [GlobalSingleton sharedInstance];
    NSString *fighterPvrCcz = [NSString stringWithFormat:@"SpaceFighter%i-select.pvr.ccz",[mySingleton shipSelected]];
    NSString *fighterPlist = [NSString stringWithFormat:@"SpaceFighter%i-select.plist",[mySingleton shipSelected]];
    
        
    _batchNodeFighter = [CCSpriteBatchNode batchNodeWithFile:fighterPvrCcz];
    [self addChild:_batchNodeFighter z:-1];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:fighterPlist];
    
    CCSpriteFrameCache * cache = 
    [CCSpriteFrameCache sharedSpriteFrameCache];
    
    animationFighterRotation = [CCAnimation animation];
    [animationFighterRotation addSpriteFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-0001.png",[mySingleton shipSelected]]]];
    [animationFighterRotation addSpriteFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-0003.png",[mySingleton shipSelected]]]];
    [animationFighterRotation addSpriteFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-0005.png",[mySingleton shipSelected]]]];
    [animationFighterRotation addSpriteFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-0007.png",[mySingleton shipSelected]]]];
    [animationFighterRotation addSpriteFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-0009.png",[mySingleton shipSelected]]]];
    [animationFighterRotation addSpriteFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-0011.png",[mySingleton shipSelected]]]];
    [animationFighterRotation addSpriteFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-0013.png",[mySingleton shipSelected]]]];
    [animationFighterRotation addSpriteFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-0015.png",[mySingleton shipSelected]]]];
    [animationFighterRotation addSpriteFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-0017.png",[mySingleton shipSelected]]]];
    [animationFighterRotation addSpriteFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-0019.png",[mySingleton shipSelected]]]];
    [animationFighterRotation addSpriteFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-0021.png",[mySingleton shipSelected]]]];
    [animationFighterRotation addSpriteFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-0023.png",[mySingleton shipSelected]]]];
    [animationFighterRotation addSpriteFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-0025.png",[mySingleton shipSelected]]]];
    [animationFighterRotation addSpriteFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-0027.png",[mySingleton shipSelected]]]];
    [animationFighterRotation addSpriteFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-0029.png",[mySingleton shipSelected]]]];
    [animationFighterRotation addSpriteFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-0031.png",[mySingleton shipSelected]]]];
    animationFighterRotation.delayPerUnit = 0.18;
    [animationFighterRotation retain];
    
}

- (void)setupBatchNodeBot {
    
    GlobalSingleton*mySingleton = [GlobalSingleton sharedInstance];
    NSString *botPvrCcz = @"bots-select.pvr.ccz";
    NSString *botPlist = @"bots-select.plist";
    
    
    
    switch ([mySingleton botSelected]) {
        case 1:
            botName = @"REPAIR POWER";
            botType = BotRepairPower;
            break;
        case 2:
            botName = @"REPAIR SHIELD";
            botType = BotRepairShield;
            break;
        case 3:
            botName = @"LASER BOT";
            botType = BotLaser;
            break;
        case 4:
            botName = @"ROCKET BOT";
            botType = BotRocket;
            break;
            
        default:
            break;
    }
    
    _batchNodeBot = [CCSpriteBatchNode batchNodeWithFile:botPvrCcz];
    [self addChild:_batchNodeBot z:-1];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:botPlist];
    
    CCSpriteFrameCache * cache = 
    [CCSpriteFrameCache sharedSpriteFrameCache];
    
    animationBotRotation = [CCAnimation animation];
    [animationBotRotation addSpriteFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"bot%i-0001.png",[mySingleton botSelected]]]];
    [animationBotRotation addSpriteFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"bot%i-0003.png",[mySingleton botSelected]]]];
    [animationBotRotation addSpriteFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"bot%i-0005.png",[mySingleton botSelected]]]];
    [animationBotRotation addSpriteFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"bot%i-0007.png",[mySingleton botSelected]]]];
    [animationBotRotation addSpriteFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"bot%i-0009.png",[mySingleton botSelected]]]];
    [animationBotRotation addSpriteFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"bot%i-0011.png",[mySingleton botSelected]]]];
    [animationBotRotation addSpriteFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"bot%i-0013.png",[mySingleton botSelected]]]];
    [animationBotRotation addSpriteFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"bot%i-0015.png",[mySingleton botSelected]]]];
    [animationBotRotation addSpriteFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"bot%i-0017.png",[mySingleton botSelected]]]];
    [animationBotRotation addSpriteFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"bot%i-0019.png",[mySingleton botSelected]]]];
    [animationBotRotation addSpriteFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"bot%i-0021.png",[mySingleton botSelected]]]];
    [animationBotRotation addSpriteFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"bot%i-0023.png",[mySingleton botSelected]]]];
    [animationBotRotation addSpriteFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"bot%i-0025.png",[mySingleton botSelected]]]];
    [animationBotRotation addSpriteFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"bot%i-0027.png",[mySingleton botSelected]]]];
    [animationBotRotation addSpriteFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"bot%i-0029.png",[mySingleton botSelected]]]];
    [animationBotRotation addSpriteFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"bot%i-0031.png",[mySingleton botSelected]]]];
    animationBotRotation.delayPerUnit = 0.2;
    [animationBotRotation retain];
    
}

-(void)prevBot{
    
    GlobalSingleton*mySingleton = [GlobalSingleton sharedInstance];
    if([mySingleton botSelected]-1==0){
        [mySingleton setBotSelected:4];
    }else{
        [mySingleton setBotSelected:[mySingleton botSelected]-1];
    }
    [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ShopLayer scene]]];
}

-(void)nextBot{
    
    GlobalSingleton*mySingleton = [GlobalSingleton sharedInstance];
    if([mySingleton botSelected]+1==5){
        [mySingleton setBotSelected:1];
    }else{
        [mySingleton setBotSelected:[mySingleton botSelected]+1];
    }
    [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ShopLayer scene]]];
    
}


-(void)prevShip{
    
    GlobalSingleton*mySingleton = [GlobalSingleton sharedInstance];
    if([mySingleton shipSelected]-1==0){
        [mySingleton setShipSelected:3];
    }else{
        [mySingleton setShipSelected:[mySingleton shipSelected]-1];
    }
    [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ShopLayer scene]]];
    
}

-(void)nextShip{
    
    GlobalSingleton*mySingleton = [GlobalSingleton sharedInstance];
    if([mySingleton shipSelected]+1==4){
        [mySingleton setShipSelected:1];
    }else{
        [mySingleton setShipSelected:[mySingleton shipSelected]+1];
    }
    [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ShopLayer scene]]];
    
}

-(void)selectShip{
    [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ShopLayer scene]]];
}
-(void)selectShip1{
    GlobalSingleton* mySingleton = [GlobalSingleton sharedInstance];
    [mySingleton setShipSelected:1];
    [mySingleton setShipConfigSelected:[mySingleton ship1]]; 
    [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ShopLayer scene]]];
}

-(void)selectShip2{
    GlobalSingleton* mySingleton = [GlobalSingleton sharedInstance];
    [mySingleton setShipSelected:2];
    [mySingleton setShipConfigSelected:[mySingleton ship2]]; 
    [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ShopLayer scene]]];
}

-(void)selectShip3{
    GlobalSingleton* mySingleton = [GlobalSingleton sharedInstance];
    [mySingleton setShipSelected:3];
    [mySingleton setShipConfigSelected:[mySingleton ship3]]; 
    [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ShopLayer scene]]];
}

-(void)goMission{
    
    if([ship power]<=0){
        [self repairShipToMin];
    }
    [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[LevelLayer scene]]];
    
}

-(void)goBack{
    
    [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[MenuLayer scene]]];
    
}
-(void)dealloc{
    
    [dictionaryShieldShip release];
    [dictionaryPowerShip release];
    [animationFighterRotation release];
    [animationBotRotation release];
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    //[ship release];
    //[bot release];
    ship = nil;
    bot = nil;
    [super dealloc];
}

@end;