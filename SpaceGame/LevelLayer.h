//
//  MenuLayer.h
//  MiniSpaceFighters
//
//  Created by Gil Beyruth on 8/29/11.
//  Copyright 2011 Ray Wenderlich. All rights reserved.
//

#import "cocos2d.h"

@interface LevelLayer : CCLayer {
    int scaleFactor;
    float px;
    float py;

}
+(id)scene;
@end;