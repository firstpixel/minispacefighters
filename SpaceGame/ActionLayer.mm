//
//  ActionLayer.m
//  SpaceGame
//
//  Created by Ray Wenderlich on 6/24/11.
//  Copyright 2011 Ray Wenderlich. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// DO NOT DISTRIBUTE WITHOUT PRIOR AUTHORIZATION (SEE LICENSE.TXT)
//

#import "ActionLayer.h"
#import "SimpleAudioEngine.h"
#import "Common.h"
#import "CCParallaxNode-Extras.h"
#import "ShapeCache.h"
#import "SimpleContactListener.h"
#import "AppDelegate.h"
#import "GlobalSingleton.h"
#import "ShopLayer.h"
#import "EndingLayer.h"
#import "Box2dDebugDrawNode.h"
#import "Box2D.h"

#define kCategoryShip       0x1
#define kCategoryShipLaser  0x2
#define kCategoryEnemy      0x4
#define kCategoryPowerup    0x8

@implementation ActionLayer


+ (id)scene {
    CCScene *scene = [CCScene node];
    
    HUDLayer *hud = [HUDLayer node];
    [scene addChild:hud z:1];
    
    ActionLayer *layer = [[[ActionLayer alloc] initWithHUD:hud] autorelease];
    [scene addChild:layer];
    
    return scene;
}

- (void)removeNode:(CCNode *)sender {
    [sender removeFromParentAndCleanup:YES];
}

- (void)invisNode:(GameObject *)sender {
    [sender destroy];
}

- (void)restartTapped:(id)sender {
    
    // Reload the current scene
    CCScene *scene = [ActionLayer scene];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionZoomFlipX transitionWithDuration:0.5 scene:scene]];
    
}
- (void)backToBaseTapped:(id)sender {
    
    // Reload the current scene
    CCScene *scene = [ShopLayer scene];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionZoomFlipX transitionWithDuration:0.5 scene:scene]];
    
}

- (void)endScene:(BOOL)win {
  //  GlobalSingleton * mySingleton = [GlobalSingleton sharedInstance];
    if (_gameOver) return;
    _gameOver = TRUE;
    //_gameStage = GameStageDone;
    
    CGSize winSize = [CCDirector sharedDirector].winSize;
    
    
    NSString *message;
    CCLabelBMFont * backToBaseLabel;
    CCMenuItemLabel *backTobaseItem =nil;
    botCanShoot = NO;
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];

        backToBaseLabel = [CCLabelBMFont labelWithString:@"Back to base!" fntFile:@"SpaceGameFont.fnt"];
    
    
        backTobaseItem = [CCMenuItemLabel itemWithLabel:backToBaseLabel target:self selector:@selector(backToBaseTapped:)];
    
    CCSprite* endBackground;
    
    if (win) {
        
        message = @"Congratulations!";
         if([[GlobalSingleton sharedInstance] audioOn])[[SimpleAudioEngine sharedEngine] playEffect:@"gameEnd.mp3" pitch:1.0f pan:0.0f gain:0.7f];
        endBackground = [CCSprite spriteWithSpriteFrameName:@"conf_talk.png"];
 
        
        //UNLOCK NEXT LEVEL    
        [mySingleton unlockItem:[NSString stringWithFormat:@"level%i",[mySingleton gameLevel]+1]];
        
        if([mySingleton gameLevel]==10){
            CCScene *scene = [EndingLayer scene];
            [[CCDirector sharedDirector] replaceScene:[CCTransitionZoomFlipX transitionWithDuration:0.5 scene:scene]];
            
            
        }
        
    } else {
        message = @"You lose!";
        endBackground = [CCSprite spriteWithSpriteFrameName:@"reptil_talk.png"];

    }
    [self addChild:endBackground z:0];
    endBackground.position = ccp(winSize.width/2,winSize.height/2*0.91);
    
    CCLabelBMFont *label;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        label = [CCLabelBMFont labelWithString:message fntFile:@"SpaceGameFont-hd.fnt"];
    } else {
        label = [CCLabelBMFont labelWithString:message fntFile:@"SpaceGameFont.fnt"];
    }
    label.scale = 0.1;
    label.position = ccp(winSize.width/2,winSize.height * 0.45);
    [self addChild:label];
    [label runAction:[CCScaleTo actionWithDuration:0.5 scale:0.5]];
    
    backTobaseItem.scale = 0.01;
    backTobaseItem.position = ccp(winSize.width/2,winSize.height * 0.3);
    CCMenu *menu = [CCMenu menuWithItems:backTobaseItem, nil];
    menu.position = CGPointZero;
    [self addChild:menu];
    
    [backTobaseItem runAction:[CCSequence actions:[CCDelayTime actionWithDuration:3],[CCScaleTo actionWithDuration:0.5 scale:0.5], nil]];
    
}

- (void)shakeScreen:(int)times {    
    id shakeLow = [CCMoveBy actionWithDuration:0.025 position:ccp(0, -3)];
    id shakeLowBack = [shakeLow reverse];
    id shakeHigh = [CCMoveBy actionWithDuration:0.025 position:ccp(0, 3)];
    id shakeHighBack = [shakeHigh reverse];
    id shakeEnd = [CCMoveTo actionWithDuration:0.025 position:ccp(0, 0)];
    id shake = [CCSequence actions:shakeLow, shakeLowBack, shakeHigh, shakeHighBack, shakeEnd, nil];
    CCRepeat* shakeAction = [CCRepeat actionWithAction:shake times:times];
    [self runAction:shakeAction];
}

- (void)spawnShip {
    CGSize winSize = [CCDirector sharedDirector].winSize;
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    
    if([[mySingleton shipConfigSelected] power]==0){
        [[mySingleton shipConfigSelected] setPower:4];
        
    }
    
    float shipHP = [[mySingleton shipConfigSelected] power]+[[mySingleton shipConfigSelected] shield];
    
    _ship = [[[GameObject alloc] initWithSpriteFrameName:[NSString stringWithFormat:@"fighter%i-side-0005.png",[mySingleton shipSelected]] world:world_ shapeName:[NSString stringWithFormat:@"fighter%i-side-0005",[mySingleton shipSelected]]  maxHp:shipHP healthBarType:HealthBarTypeNone] autorelease];
    _ship.position = ccp(-_ship.contentSize.width/2, 
                         winSize.height * 0.5);
    [self updateShipPower:[[mySingleton shipConfigSelected] power]];
    [self updateShipShield:[[mySingleton shipConfigSelected] shield]];
    
    
    
    if([[mySingleton shipConfigSelected] bot1]!=0){
        int botNumber = [[mySingleton shipConfigSelected] bot1];
        _bot1 = [[[GameObject alloc] initWithSpriteFrameName:[NSString stringWithFormat:@"bot%i-side-0005.png",botNumber] world:world_ shapeName:[NSString stringWithFormat:@"bot%i-side-0005",botNumber] maxHp:1000 healthBarType:HealthBarTypeNone] autorelease];
        _bot1.position = ccp(-_ship.contentSize.width/2 + _bot1.contentSize.width, 
                             winSize.height * 0.5);
        [_bot1 revive];
        [_batchNodeBot addChild:_bot1 z:1];
    }
    if([[mySingleton shipConfigSelected] bot2]!=0){
        int botNumber = [[mySingleton shipConfigSelected] bot2];
        _bot2 = [[[GameObject alloc] initWithSpriteFrameName:[NSString stringWithFormat:@"bot%i-side-0005.png",botNumber] world:world_ shapeName:[NSString stringWithFormat:@"bot%i-side-0005",botNumber] maxHp:1000 healthBarType:HealthBarTypeNone] autorelease];
        _bot2.position = ccp(-_ship.contentSize.width/2 + _bot2.contentSize.width, 
                             winSize.height * 0.5);
        [_bot2 revive];
        [_batchNodeBot addChild:_bot2 z:1];
    }
    if([[mySingleton shipConfigSelected] bot3]!=0){
        int botNumber = [[mySingleton shipConfigSelected] bot3];
        _bot3 = [[[GameObject alloc] initWithSpriteFrameName:[NSString stringWithFormat:@"bot%i-side-0005.png",botNumber] world:world_ shapeName:[NSString stringWithFormat:@"bot%i-side-0005",botNumber] maxHp:1000 healthBarType:HealthBarTypeNone] autorelease];
        _bot3.position = ccp(-_ship.contentSize.width/2 + _bot3.contentSize.width, 
                             winSize.height * 0.5);
        [_bot3 revive];
        [_batchNodeBot addChild:_bot3 z:1];
    }
    if([[mySingleton shipConfigSelected] bot4]!=0){
        int botNumber = [[mySingleton shipConfigSelected] bot4];
        _bot4 = [[[GameObject alloc] initWithSpriteFrameName:[NSString stringWithFormat:@"bot%i-side-0005.png",botNumber] world:world_ shapeName:[NSString stringWithFormat:@"bot%i-side-0005",botNumber] maxHp:1000 healthBarType:HealthBarTypeNone] autorelease];
        _bot4.position = ccp(-_ship.contentSize.width/2 + _bot4.contentSize.width, 
                             winSize.height * 0.5);
        [_bot4 revive];
        [_batchNodeBot addChild:_bot4 z:1];
    }
    [_ship revive];
    [_batchNodeFighter addChild:_ship z:1];
    [_ship runAction:
     [CCSequence actions:
      [CCEaseOut actionWithAction: [CCMoveBy actionWithDuration:0.5  position:ccp(_ship.contentSize.width/2 + winSize.width*0.3, 0)] rate:4.0],
      [CCEaseInOut actionWithAction: [CCMoveBy actionWithDuration:0.5  position:ccp(-winSize.width*0.2, 0)] rate:4.0],
      nil]];
    
    CCSpriteFrameCache * cache = [CCSpriteFrameCache sharedSpriteFrameCache];

    animationUp = [CCAnimation animation];
    [animationUp addFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-side-0005.png",[mySingleton shipSelected]]]];
    [animationUp addFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-side-0004.png",[mySingleton shipSelected]]]];
    [animationUp addFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-side-0003.png",[mySingleton shipSelected]]]];
    [animationUp addFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-side-0002.png",[mySingleton shipSelected]]]];
    [animationUp addFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-side-0001.png",[mySingleton shipSelected]]]];
    [animationUp addFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-side-0001.png",[mySingleton shipSelected]]]];
    [animationUp addFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-side-0001.png",[mySingleton shipSelected]]]];
    animationUp.delayPerUnit = 0.05;
    [animationUp retain];
    
    
    animationUpStop = [CCAnimation animation];
    [animationUpStop addFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-side-0001.png",[mySingleton shipSelected]]]];
    [animationUpStop addFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-side-0002.png",[mySingleton shipSelected]]]];
    [animationUpStop addFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-side-0003.png",[mySingleton shipSelected]]]];
    [animationUpStop addFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-side-0004.png",[mySingleton shipSelected]]]];
    [animationUpStop addFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-side-0005.png",[mySingleton shipSelected]]]];
    [animationUpStop addFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-side-0006.png",[mySingleton shipSelected]]]];
    [animationUpStop addFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-side-0005.png",[mySingleton shipSelected]]]];
    animationUpStop.delayPerUnit = 0.05;
    [animationUpStop retain];
    
    
    animationDown = [CCAnimation animation];
    [animationDown addFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-side-0005.png",[mySingleton shipSelected]]]];
    [animationDown addFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-side-0006.png",[mySingleton shipSelected]]]];
    [animationDown addFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-side-0007.png",[mySingleton shipSelected]]]];
    [animationDown addFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-side-0008.png",[mySingleton shipSelected]]]];
    [animationDown addFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-side-0009.png",[mySingleton shipSelected]]]];
    [animationDown addFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-side-0009.png",[mySingleton shipSelected]]]];
    [animationDown addFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-side-0009.png",[mySingleton shipSelected]]]];
    animationDown.delayPerUnit = 0.05;
    [animationDown retain];
    
    animationDownStop = [CCAnimation animation];
    [animationDownStop addFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-side-0009.png",[mySingleton shipSelected]]]];
    [animationDownStop addFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-side-0008.png",[mySingleton shipSelected]]]];
    [animationDownStop addFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-side-0007.png",[mySingleton shipSelected]]]];
    [animationDownStop addFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-side-0006.png",[mySingleton shipSelected]]]];
    [animationDownStop addFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-side-0005.png",[mySingleton shipSelected]]]];
    [animationDownStop addFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-side-0004.png",[mySingleton shipSelected]]]];
    [animationDownStop addFrame:[cache spriteFrameByName:[NSString stringWithFormat:@"fighter%i-side-0005.png",[mySingleton shipSelected]]]];
    animationDownStop.delayPerUnit = 0.05;
    [animationDownStop retain];
    
    CCParticleSystemQuad *trailEffect = [_trailEffects nextParticleSystem];
    [trailEffect resetSystem];
    /* [_ship runAction:[CCRepeatForever actionWithAction:[CCAnimate actionWithAnimation:animation]]]; */
    
}

- (void)playTapped:(id)sender {
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    [(AppDelegate*)[[UIApplication sharedApplication] delegate] startMovie:[mySingleton levelSelected] ofType:@"mov"];
    if([[GlobalSingleton sharedInstance] audioOn])[[SimpleAudioEngine sharedEngine] playEffect:@"powerup.caf"];
    /*
    NSArray * nodes = [NSArray arrayWithObjects:_titleLabel1, _titleLabel2, _playItem, nil];
    for (CCNode *node in nodes) {
        [node runAction:
         [CCSequence actions:
          [CCEaseOut actionWithAction:
           [CCScaleTo actionWithDuration:0.5 scale:0] rate:4.0],
          [CCCallFuncN actionWithTarget:self selector:@selector(removeNode:)],
          nil]];
    }
    */
    [self spawnShip];
    //_gameStage = GameStageAsteroids;
    [_levelManager nextStage];
    [self newStageStarted];
}

- (void)playTitleEffect {
    if([[GlobalSingleton sharedInstance] audioOn])[[SimpleAudioEngine sharedEngine] playEffect:@"title.caf"];
}

- (void)setupSound {
    GlobalSingleton* mySingleton = [GlobalSingleton sharedInstance];
    
    if([[GlobalSingleton sharedInstance] audioOn]){
        
        if ([SimpleAudioEngine sharedEngine] != nil) {
            [[SimpleAudioEngine sharedEngine] playBackgroundMusic:[NSString stringWithFormat:@"%@.m4a",[mySingleton levelSelected]] loop:YES];
        }
    }else{
        if ([[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying]==YES){
            [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
        }
    }

    
    [mySingleton setIsPlayingMenuMusic:NO];
    [[SimpleAudioEngine sharedEngine] preloadEffect:@"explosion_large.caf"];
    [[SimpleAudioEngine sharedEngine] preloadEffect:@"explosion_small.caf"];
    [[SimpleAudioEngine sharedEngine] preloadEffect:@"laser_enemy.caf"];
    [[SimpleAudioEngine sharedEngine] preloadEffect:@"laser_ship.caf"];
    [[SimpleAudioEngine sharedEngine] preloadEffect:@"shake.caf"];
    [[SimpleAudioEngine sharedEngine] preloadEffect:@"powerup.caf"];
    [[SimpleAudioEngine sharedEngine] preloadEffect:@"boss.caf"];
    [[SimpleAudioEngine sharedEngine] preloadEffect:@"cannon.caf"];
    [[SimpleAudioEngine sharedEngine] preloadEffect:@"title.caf"];
    [[SimpleAudioEngine sharedEngine] preloadEffect:@"gameEnd.mp3"];
}

- (void)setupStars {
    CGSize winSize = [CCDirector sharedDirector].winSize;
    NSArray *starsArray = [NSArray arrayWithObjects:@"Stars1.plist", @"Stars2.plist", @"Stars3.plist", nil];
    for(NSString *stars in starsArray) {        
        CCParticleSystemQuad *starsEffect = [CCParticleSystemQuad particleWithFile:stars];        
        starsEffect.position = ccp(winSize.width*1.5, winSize.height/2);
        starsEffect.posVar = ccp(starsEffect.posVar.x, (winSize.height/2) * 1.5);
        if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPad) {
            starsEffect.scale = 0.5;
            starsEffect.posVar = ccpMult(starsEffect.posVar, 2.0);
        }
        [self addChild:starsEffect];
    }   
}

- (void)setupBatchNode {
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    NSString *spritesPvrCcz = @"Sprites.pvr.ccz";
    NSString *spritesPlist = @"Sprites.plist"; 
    NSString *fighterPvrCcz = [NSString stringWithFormat:@"SpaceFighter%i.pvr.ccz",[mySingleton shipSelected]];
    NSString *fighterPlist = [NSString stringWithFormat:@"SpaceFighter%i.plist",[mySingleton shipSelected]]; 
    NSString *botsPvrCcz = @"bots.pvr.ccz";
    NSString *botsPlist = @"bots.plist"; 
    
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:botsPlist];
    _batchNodeBot = [CCSpriteBatchNode batchNodeWithFile:botsPvrCcz capacity:100];
    [self addChild:_batchNodeBot z:-1];
    
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:spritesPlist];
    _batchNode = [CCSpriteBatchNode batchNodeWithFile:spritesPvrCcz capacity:100];
    [self addChild:_batchNode z:-1];
    
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:fighterPlist];
    _batchNodeFighter = [CCSpriteBatchNode batchNodeWithFile:fighterPvrCcz capacity:100];
    [self addChild:_batchNodeFighter z:-1];
}

- (void)setupEffects {
    _fire1Effect = [[CCParticleSystemQuad alloc] initWithFile:@"fireTail1.plist"];
    [self addChild:_fire1Effect z:10];
    [_fire1Effect stopSystem];
    
    _fire12Effect = [[CCParticleSystemQuad alloc] initWithFile:@"fireTail12.plist"];
    [self addChild:_fire12Effect z:10];
    [_fire12Effect stopSystem];
    
    _smoke1Effect = [[CCParticleSystemQuad alloc] initWithFile:@"smokeTail1.plist"];
    [self addChild:_smoke1Effect z:10];
    [_smoke1Effect stopSystem];
    
    _smoke11Effect = [[CCParticleSystemQuad alloc] initWithFile:@"smokeTail11.plist"];
    [self addChild:_smoke11Effect z:10];
    [_smoke11Effect stopSystem];
    
    _smoke2Effect = [[CCParticleSystemQuad alloc] initWithFile:@"smokeTail2.plist"];
    [self addChild:_smoke2Effect z:10];
    [_smoke2Effect stopSystem];
    
    _smoke21Effect = [[CCParticleSystemQuad alloc] initWithFile:@"smokeTail21.plist"];
    [self addChild:_smoke21Effect z:10];
    [_smoke21Effect stopSystem];
    
    _shield1Effect = [[CCParticleSystemQuad alloc] initWithFile:@"shield1.plist"];
    [self addChild:_shield1Effect z:10];
    [_shield1Effect stopSystem];
    
    _shield2Effect = [[CCParticleSystemQuad alloc] initWithFile:@"shield2.plist"];
    [self addChild:_shield2Effect z:10];
    [_shield2Effect stopSystem];
    
    _shield3Effect = [[CCParticleSystemQuad alloc] initWithFile:@"shield3.plist"];
    [self addChild:_shield3Effect z:10];
    [_shield3Effect stopSystem];
}

- (void)setupArrays {
    GlobalSingleton *mySingleton = [GlobalSingleton sharedInstance];
    _asteroidsArray = [[SpriteArray alloc] initWithCapacity:20 spriteFrameName:[NSString stringWithFormat:@"%@-rock.png",[mySingleton levelSelected]] batchNode:_batchNode world:world_ shapeName:@"earth-rock" maxHp:1*([mySingleton gameLevel]) healthBarType:HealthBarTypeNone];
    int laserHitPower;
    if([mySingleton shipSelected]==1){
        laserHitPower= 1;
    }else if([mySingleton shipSelected]==2){
        laserHitPower = 3;
    }else if([mySingleton shipSelected]==3){
        laserHitPower = 8;
    }
    _laserArray = [[SpriteArray alloc] initWithCapacity:20 spriteFrameName:@"laserbeam_blue.png" batchNode:_batchNode world:world_ shapeName:@"laserbeam_blue" maxHp:laserHitPower healthBarType:HealthBarTypeNone];
    
    _rocketArray = [[SpriteArray alloc] initWithCapacity:20 spriteFrameName:@"rocket.png" batchNode:_batchNode world:world_ shapeName:@"laserbeam_blue" maxHp:2*laserHitPower healthBarType:HealthBarTypeNone];
    
    _explosions = [[ParticleSystemArray alloc] initWithFile:@"Explosion.plist" capacity:10 parent:self];
    
    _alienArray = [[SpriteArray alloc] initWithCapacity:20 spriteFrameName:[NSString stringWithFormat:@"reptil-%@.png",[mySingleton levelSelected]] batchNode:_batchNodeFighter world:world_ shapeName:[NSString stringWithFormat:@"reptil-%@",[mySingleton levelSelected]] maxHp:3*([mySingleton gameLevel]) healthBarType:HealthBarTypeNone];
    
    _enemyLasers = [[SpriteArray alloc] initWithCapacity:40 spriteFrameName:@"laserbeam_red.png" batchNode:_batchNode world:world_ shapeName:@"laserbeam_red" maxHp:laserHitPower healthBarType:HealthBarTypeNone];
   
    _powerupEffects = [[ParticleSystemArray alloc] initWithFile:@"nuke20.plist" capacity:1 parent:self];
    
    _powerups = [[SpriteArray alloc] initWithCapacity:1 spriteFrameName:@"powerup0.png" batchNode:_batchNode world:world_ shapeName:@"powerup" maxHp:1 healthBarType:HealthBarTypeNone];
    
    _boostEffects = [[ParticleSystemArray alloc] initWithFile:@"Boost.plist" capacity:1 parent:self];
    
    if([mySingleton shipSelected]==1){
        _trailEffects = [[ParticleSystemArray alloc] initWithFile:@"shipTail1.plist" capacity:1 parent:self];
    }else if([mySingleton shipSelected]==2){
        _trailEffects = [[ParticleSystemArray alloc] initWithFile:@"shipTail2.plist" capacity:1 parent:self];
    }else if([mySingleton shipSelected]==3){
        _trailEffects = [[ParticleSystemArray alloc] initWithFile:@"shipTail3.plist" capacity:1 parent:self];
    }
    
    _cannonBalls = [[SpriteArray alloc] initWithCapacity:20 spriteFrameName:@"Boss_cannon_ball.png" batchNode:_batchNode world:world_ shapeName:@"Boss_cannon_ball" maxHp:3*([mySingleton gameLevel]+1) healthBarType:HealthBarTypeNone];
}
/*
- (void)setupBackground {
    CGSize winSize = [CCDirector sharedDirector].winSize;
    // 1) Create the CCParallaxNode
    _backgroundNode = [CCParallaxNode node];
    [self addChild:_backgroundNode z:-2];
    // 2) Create the sprites we'll add to the CCParallaxNode
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        _spacedust1 = [CCSprite spriteWithFile:@"bg_front_spacedust-hd.png"];
        _spacedust2 = [CCSprite spriteWithFile:@"bg_front_spacedust-hd.png"];
        //_planetsunrise = [CCSprite spriteWithFile:@"bg_planetsunrise-hd.png"];
        //_galaxy = [CCSprite spriteWithFile:@"bg_galaxy-hd.png"];
        _spacialanomaly = [CCSprite spriteWithFile:@"bg_spacialanomaly-hd.png"];
        _spacialanomaly2 = [CCSprite spriteWithFile:@"bg_spacialanomaly2-hd.png"];
        _spacedust1.scale = 1.5;
        _spacedust2.scale = 1.5;
    } else {
        _spacedust1 = [CCSprite spriteWithFile:@"bg_front_spacedust.png"];
        _spacedust2 = [CCSprite spriteWithFile:@"bg_front_spacedust.png"];
       // _planetsunrise = [CCSprite spriteWithFile:@"bg_planetsunrise.png"];
       // _galaxy = [CCSprite spriteWithFile:@"bg_galaxy.png"];
        _spacialanomaly = [CCSprite spriteWithFile:@"bg_spacialanomaly.png"];
        _spacialanomaly2 = [CCSprite spriteWithFile:@"bg_spacialanomaly2.png"];
    }
    
    // 3) Determine relative movement speeds for space dust and background
    CGPoint dustSpeed = ccp(0.1, 0.1);
    CGPoint bgSpeed = ccp(0.05, 0.05);
    // 4) Add children to CCParallaxNode
   // [_backgroundNode addChild:_spacedust1 z:0 parallaxRatio:dustSpeed positionOffset:ccp(0,winSize.height/2)];
   // [_backgroundNode addChild:_spacedust2 z:0 parallaxRatio:dustSpeed positionOffset:ccp(_spacedust1.contentSize.width*_spacedust1.scale,winSize.height/2)];     
   // [_backgroundNode addChild:_galaxy z:-1 parallaxRatio:bgSpeed positionOffset:ccp(0,winSize.height * 0.7)];
    //[_backgroundNode addChild:_planetsunrise z:-1 parallaxRatio:bgSpeed positionOffset:ccp(600,winSize.height * 0)];        
   // [_backgroundNode addChild:_spacialanomaly z:-1 parallaxRatio:bgSpeed positionOffset:ccp(900,winSize.height * 0.3)];        
  //  [_backgroundNode addChild:_spacialanomaly2 z:-1 parallaxRatio:bgSpeed positionOffset:ccp(1500,winSize.height * 0.9)];
}
*/
- (void)setupWorld {    
    b2Vec2 gravity = b2Vec2(0.0f, 0.0f);
    //bool doSleep = false;
    world_ = new b2World(gravity); //, doSleep);
    _contactListener = new SimpleContactListener(self);
    world_->SetContactListener(_contactListener);
}

- (void)setupDebugDraw {    
    /*_debugDraw = new GLESDebugDraw(PTM_RATIO*[[CCDirector sharedDirector] contentScaleFactor]);
    world_->SetDebugDraw(_debugDraw);
    _debugDraw->SetFlags(b2DebugDraw::e_shapeBit | b2DebugDraw::e_jointBit);*/
    
    Box2dDebugDrawNode *b2node = [Box2dDebugDrawNode nodeWithWorld:world_];
    [self addChild:b2node z:30];
}

- (void)testBox2D {
    CGSize winSize = [CCDirector sharedDirector].winSize;
    b2BodyDef bodyDef;
    bodyDef.type = b2_dynamicBody;
    bodyDef.position = b2Vec2(winSize.width/2/PTM_RATIO, 
                              winSize.height/2/PTM_RATIO);
    b2Body *body = world_->CreateBody(&bodyDef);
    b2CircleShape circleShape;
    circleShape.m_radius = 25.0/PTM_RATIO;
    b2FixtureDef fixtureDef;
    fixtureDef.shape = &circleShape;
    fixtureDef.density = 1.0;
    body->CreateFixture(&fixtureDef);
    body->ApplyAngularImpulse(0.01);
}

- (void)setupShapeCache {
    [[ShapeCache sharedShapeCache] addShapesWithFile:@"Shapes.plist"];
}

- (void)setupLevelManager {
    _levelManager = [[LevelManager alloc] init];    
}

- (void)setupBoss {
    _boss = [[[BossShip alloc] initWithWorld:world_ layer:self] autorelease];
    _boss.visible = NO;
    [_batchNode addChild:_boss];
}

// Replace beginning of init with the following
- (id)initWithHUD:(HUDLayer *)hud
{
    if ((self = [super init])) {
        GlobalSingleton * mySingleton = [GlobalSingleton sharedInstance];
        _hud = hud;
        px = 0.0;
        py = 0.0;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            scaleFactor = 2;
            px = 32.0f;
            py = 64.0f;
        } else {
            scaleFactor = 1;//[[CCDirector sharedDirector] contentScaleFactor];
        }
        if([mySingleton shipSelected]==1){
            powerDictionaryShip = [mySingleton powerDictionaryShip1];
            shieldDictionaryShip = [mySingleton shieldDictionaryShip1];
            
        } 
        if([mySingleton shipSelected]==2){
            powerDictionaryShip = [mySingleton powerDictionaryShip2];
            shieldDictionaryShip = [mySingleton shieldDictionaryShip2];
            
        } 
        if([mySingleton shipSelected]==3){
            powerDictionaryShip = [mySingleton powerDictionaryShip3];
            shieldDictionaryShip = [mySingleton shieldDictionaryShip3];
            
        } 
        [self setupBatchNode];
        [self setupWorld];
        [self setupDebugDraw];
        //[self testBox2D];
        [self setupShapeCache];
        [self setupSound];
        //    [self setupTitle];
        [self setupStars];
        self.isAccelerometerEnabled = YES;
        [self scheduleUpdate];
        [self setupArrays];
        [self setupEffects];
        self.isTouchEnabled = YES;
       // [self setupBackground];
        //double curTime = CACurrentMediaTime();
        //_gameWonTime = curTime + 30.0;
        [self setupLevelManager];
        [self setupBoss];
        [self updateNukes:[mySingleton myNukes]];
        [self updatePoints:[mySingleton myPoints]];
        [self playTapped:self];
    }
    return self;    
}

- (void)animationStopUpDone{
    shipIsStoppingUp=NO;
}

- (void)animationStopDownDone{
    shipIsStoppingDown=NO;
}

- (void)playShipAnimationUp{
    if(shipIsGoingUp==NO && shipIsGoingDown==NO && shipIsStoppingUp==NO && shipIsStoppingDown==NO){
        shipIsGoingUp = YES;
        [_ship runAction:[CCSequence actions:
                          [CCAnimate actionWithAnimation:animationUp restoreOriginalFrame:NO],
                         // [CCDelayTime actionWithDuration:0.2],
                          nil]];  
    }
}

- (void)playShipAnimationDown{
    if(shipIsGoingUp==NO && shipIsGoingDown==NO && shipIsStoppingUp==NO && shipIsStoppingDown==NO){
        shipIsGoingDown= YES;
        [_ship runAction:[CCSequence actions:
                          [CCAnimate actionWithAnimation:animationDown restoreOriginalFrame:NO],
                         // [CCDelayTime actionWithDuration:0.2],
                          nil]];
    }
}

- (void)playShipAnimationStop{
    if(shipIsGoingUp==YES  && shipIsGoingDown==NO && shipIsStoppingUp==NO && shipIsStoppingDown==NO){
        shipIsGoingUp= NO;
        shipIsStoppingUp = YES;
        [_ship runAction:[CCSequence actions:
                          [CCAnimate actionWithAnimation:animationUpStop restoreOriginalFrame:YES],
                          //[CCDelayTime actionWithDuration:0.2],
                          [CCCallFunc actionWithTarget:self selector:@selector(animationStopUpDone)],
                          nil]];
    }
    if(shipIsGoingUp==NO && shipIsGoingDown==YES && shipIsStoppingUp==NO && shipIsStoppingDown==NO){
        shipIsGoingDown = NO;
        shipIsStoppingDown = YES;
        [_ship runAction:[CCSequence actions:
                          [CCAnimate actionWithAnimation:animationDownStop restoreOriginalFrame:YES],
                          //[CCDelayTime actionWithDuration:0.2],
                          [CCCallFunc actionWithTarget:self selector:@selector(animationStopDownDone)],
                          nil]];
    }
}

-(void) shotRocketBot1{
    if(botCanShoot){
        CGSize winSize = [CCDirector sharedDirector].winSize;
        GameObject *bot1Rocket;
        bot1Rocket = [_rocketArray nextSprite];
        [bot1Rocket stopAllActions];
        bot1Rocket.position = ccpAdd(_bot1.position, ccp(bot1Rocket.contentSize.width/2, 0));
        [bot1Rocket revive];
        [bot1Rocket runAction:[CCSequence actions:
                           [CCMoveBy actionWithDuration:0.1 position:ccp(_bot1.position.x+30, 0)],
                           [CCMoveBy actionWithDuration:0.2 position:ccp(0, -20)],
                           [CCMoveBy actionWithDuration:0.8 position:ccp(winSize.width, 0)],
                           [CCCallFuncN actionWithTarget:self selector:@selector(invisNode:)],
                           nil]];
        if([[GlobalSingleton sharedInstance] audioOn])[[SimpleAudioEngine sharedEngine] playEffect:@"laser_ship.caf" pitch:1.0f pan:0.0f gain:0.25f];
    }
}

-(void) shotRocketBot2{
    if(botCanShoot){
        CGSize winSize = [CCDirector sharedDirector].winSize;
        GameObject *bot2Rocket;
        bot2Rocket = [_rocketArray nextSprite];
        [bot2Rocket stopAllActions];
        bot2Rocket.position = ccpAdd(_bot2.position, ccp(bot2Rocket.contentSize.width/2, 0));
        [bot2Rocket revive];
        [bot2Rocket runAction:[CCSequence actions:
                           [CCMoveBy actionWithDuration:0.1 position:ccp(_bot2.position.x+30, 0)],
                           [CCMoveBy actionWithDuration:0.2 position:ccp(0, -20)],
                           [CCMoveBy actionWithDuration:0.8 position:ccp(winSize.width, 0)],
                           [CCCallFuncN actionWithTarget:self selector:@selector(invisNode:)],
                           nil]];
        if([[GlobalSingleton sharedInstance] audioOn])[[SimpleAudioEngine sharedEngine] playEffect:@"laser_ship.caf" pitch:1.0f pan:0.0f gain:0.25f];
    }
}

-(void) shotRocketBot3{
    if(botCanShoot){
        CGSize winSize = [CCDirector sharedDirector].winSize;
        GameObject *bot3Rocket;
        bot3Rocket = [_rocketArray nextSprite];
        [bot3Rocket stopAllActions];
        bot3Rocket.position = ccpAdd(_bot2.position, ccp(bot3Rocket.contentSize.width/2, 0));
        [bot3Rocket revive];
        [bot3Rocket runAction:[CCSequence actions:
                           [CCMoveBy actionWithDuration:0.1 position:ccp(_bot2.position.x+30, 0)],
                           [CCMoveBy actionWithDuration:0.2 position:ccp(0, -20)],
                           [CCMoveBy actionWithDuration:0.8 position:ccp(winSize.width, 0)],
                           [CCCallFuncN actionWithTarget:self selector:@selector(invisNode:)],
                           nil]];
        if([[GlobalSingleton sharedInstance] audioOn])[[SimpleAudioEngine sharedEngine] playEffect:@"laser_ship.caf" pitch:1.0f pan:0.0f gain:0.25f];
    }
}
-(void) shotRocketBot4{
    if(botCanShoot){
        CGSize winSize = [CCDirector sharedDirector].winSize;
        GameObject *bot4Rocket;
        bot4Rocket = [_rocketArray nextSprite];
        [bot4Rocket stopAllActions];
        bot4Rocket.position = ccpAdd(_bot4.position, ccp(bot4Rocket.contentSize.width/2, 0));
        [bot4Rocket revive];
        [bot4Rocket runAction:[CCSequence actions:
                           [CCMoveBy actionWithDuration:0.1 position:ccp(_bot4.position.x+30, 0)],
                           [CCMoveBy actionWithDuration:0.2 position:ccp(0, -20)],
                           [CCMoveBy actionWithDuration:0.8 position:ccp(winSize.width, 0)],
                           [CCCallFuncN actionWithTarget:self selector:@selector(invisNode:)],
                           nil]];
        if([[GlobalSingleton sharedInstance] audioOn])[[SimpleAudioEngine sharedEngine] playEffect:@"laser_ship.caf" pitch:1.0f pan:0.0f gain:0.25f];
    }
}

-(void) shotLaserBot1{
    if(botCanShoot){
        CGSize winSize = [CCDirector sharedDirector].winSize;
        GameObject *bot1Laser;
        bot1Laser = [_laserArray nextSprite];
        [bot1Laser stopAllActions];
        bot1Laser.position = ccpAdd(_bot1.position, ccp(bot1Laser.contentSize.width/2, 0));
        [bot1Laser revive];
        [bot1Laser runAction:[CCSequence actions:
                          [CCMoveBy actionWithDuration:0.5 position:ccp(winSize.width, 0)],
                          [CCCallFuncN actionWithTarget:self selector:@selector(invisNode:)],
                          nil]];
        if([[GlobalSingleton sharedInstance] audioOn])[[SimpleAudioEngine sharedEngine] playEffect:@"laser_ship.caf" pitch:1.0f pan:0.0f gain:0.25f];
    }
}

-(void) shotLaserBot2{
    if(botCanShoot){
        CGSize winSize = [CCDirector sharedDirector].winSize;
        GameObject *bot2Laser;
        bot2Laser = [_laserArray nextSprite];
        [bot2Laser stopAllActions];
        bot2Laser.position = ccpAdd(_bot2.position, ccp(bot2Laser.contentSize.width/2, 0));
        [bot2Laser revive];
        [bot2Laser runAction:[CCSequence actions:
                          [CCMoveBy actionWithDuration:0.5 position:ccp(winSize.width, 0)],
                          [CCCallFuncN actionWithTarget:self selector:@selector(invisNode:)],
                          nil]];
        if([[GlobalSingleton sharedInstance] audioOn])[[SimpleAudioEngine sharedEngine] playEffect:@"laser_ship.caf" pitch:1.0f pan:0.0f gain:0.25f];
    }
}

-(void) shotLaserBot3{
    if(botCanShoot){
        CGSize winSize = [CCDirector sharedDirector].winSize;
        GameObject *bot3Laser;
        bot3Laser = [_laserArray nextSprite];
        [bot3Laser stopAllActions];
        bot3Laser.position = ccpAdd(_bot3.position, ccp(bot3Laser.contentSize.width/2, 0));
        [bot3Laser revive];
        [bot3Laser runAction:[CCSequence actions:
                          [CCMoveBy actionWithDuration:0.5 position:ccp(winSize.width, 0)],
                          [CCCallFuncN actionWithTarget:self selector:@selector(invisNode:)],
                          nil]];
        if([[GlobalSingleton sharedInstance] audioOn])[[SimpleAudioEngine sharedEngine] playEffect:@"laser_ship.caf" pitch:1.0f pan:0.0f gain:0.25f];
    }
}

-(void) shotLaserBot4{
    if(botCanShoot){
        CGSize winSize = [CCDirector sharedDirector].winSize;
        GameObject *bot4Laser;
        bot4Laser = [_laserArray nextSprite];
        [bot4Laser stopAllActions];
        bot4Laser.position = ccpAdd(_bot4.position, ccp(bot4Laser.contentSize.width/2, 0));
        [bot4Laser revive];
        [bot4Laser runAction:[CCSequence actions:
                          [CCMoveBy actionWithDuration:0.5 position:ccp(winSize.width, 0)],
                          [CCCallFuncN actionWithTarget:self selector:@selector(invisNode:)],
                          nil]];
        if([[GlobalSingleton sharedInstance] audioOn])[[SimpleAudioEngine sharedEngine] playEffect:@"laser_ship.caf" pitch:1.0f pan:0.0f gain:0.25f];
    }
}

-(void) fixPowerBot:(int)botId{
    GlobalSingleton * mySingleton = [GlobalSingleton sharedInstance];
    //visualEffect.position = _bot1.position;
    int power = [[mySingleton shipConfigSelected] power];
    int powerTotal;
    powerTotal = [[powerDictionaryShip objectForKey:[NSString stringWithFormat:@"%i",[[mySingleton shipConfigSelected] shipPowerLevel]]] intValue];
    power = power+(10*powerTotal/100);
    if(power<=powerTotal){
    }else{
        power = powerTotal;
    }
    [[mySingleton shipConfigSelected] setPower:power];
    [mySingleton saveShip:[mySingleton shipConfigSelected] forKey:[mySingleton shipSelected]];
    [self updateShipPower:power];
    [_ship setHP:power + [[mySingleton shipConfigSelected] shield]];
}

-(void) fixShieldBot:(int)botId{
    GlobalSingleton * mySingleton = [GlobalSingleton sharedInstance];
    //visualEffect.position = _bot1.position;
    int shield = [[mySingleton shipConfigSelected] shield];
    int shieldTotal;
    shieldTotal = [[shieldDictionaryShip objectForKey:[NSString stringWithFormat:@"%i",[[mySingleton shipConfigSelected] shipShieldLevel]]] intValue];
    shield = shield + (10*shieldTotal/100);
    if(shield<=shieldTotal){
        }else{
        shield = shieldTotal;
    }
    [[mySingleton shipConfigSelected] setShield:shield];
    [mySingleton saveShip:[mySingleton shipConfigSelected] forKey:[mySingleton shipSelected]];
    [self updateShipShield:shield];
    [_ship setHP:shield + [[mySingleton shipConfigSelected] power]];
}

- (void)updateShipPos:(ccTime)dt {
    GlobalSingleton * mySingleton = [GlobalSingleton sharedInstance];
    CGSize winSize = [CCDirector sharedDirector].winSize;
    float maxY = winSize.height - _ship.contentSize.height/2;
    float minY = _ship.contentSize.height/2;
    float actualPosiY = _ship.position.y + (_shipPointsPerSecY * dt);
    if( actualPosiY > newY && actualPosiY > newY + 2 ){
      //  CCLOG(@"ship going up: -ship: %f, newY %f _shipPointsPerSecY %f",actualPosiY,newY, _shipPointsPerSecY);
        [self playShipAnimationUp];
    }else if( actualPosiY < newY && actualPosiY < newY - 2 ){
      //   CCLOG(@"ship going down: -ship: %f, newY %f _shipPointsPerSecY %f",actualPosiY,newY, _shipPointsPerSecY);
        [self playShipAnimationDown];
    }else{
      //  CCLOG(@"ship flying: -ship: %f, newY %f _shipPointsPerSecY %f",actualPosiY,newY, _shipPointsPerSecY);
        [self playShipAnimationStop];
    }
    
    //controll on screen , comment for release
   // _shipPointsPerSecY = -(lastTouchPosition.y-310);
    float maxX = winSize.width - _ship.contentSize.width/2-(30*scaleFactor);
    float minX = _ship.contentSize.width/2+(5*scaleFactor);
    newX = _ship.position.x + (_shipPointsPerSecX * dt);
    newX = MIN(MAX(newX, minX), maxX);
    newY = _ship.position.y + (_shipPointsPerSecY * dt);
    newY = MIN(MAX(newY, minY), maxY);
    // CCLOG(@"position Y :%f",newY);
    _ship.position = ccp(_ship.position.x, newY);
    _shield1Effect.position = ccp(_ship.position.x+(60 *scaleFactor),_ship.position.y);
    _shield2Effect.position = ccp(_ship.position.x+(60 *scaleFactor),_ship.position.y);
    _shield3Effect.position = ccp(_ship.position.x+(60 *scaleFactor),_ship.position.y);
    
    //_ship.position = ccp(newX, newY);
    
    #define kDistanceBot1 25
    #define kDistanceBot2 50
    #define kDistanceBot3 75
    #define kDistanceBot4 100
    
    if ( ccpDistance(_bot1.position, _ship.position)>kDistanceBot1  && !_bot1.dead) {
        _bot1.position = ccp(_bot1.position.x+(_ship.position.x-_bot1.position.x)/10,_bot1.position.y+(_ship.position.y-_bot1.position.y)/10);
     } 
    if ( ccpDistance(_bot2.position, _ship.position)>kDistanceBot2 && !_bot2.dead) {
        _bot2.position = ccp(_bot2.position.x+(_ship.position.x-_bot2.position.x)/15,_bot2.position.y+(_ship.position.y-_bot2.position.y)/15);
    }
    if ( ccpDistance(_bot3.position, _ship.position)>kDistanceBot3 && !_bot3.dead) {
        _bot3.position = ccp(_bot3.position.x+(_ship.position.x-_bot3.position.x)/20,_bot3.position.y+(_ship.position.y-_bot3.position.y)/20);
    }
    if ( ccpDistance(_bot4.position, _ship.position)>kDistanceBot4 && !_bot4.dead) {
        _bot4.position = ccp(_bot4.position.x+(_ship.position.x-_bot4.position.x)/25,_bot4.position.y+(_ship.position.y-_bot4.position.y)/25);
    }
    shotTime = shotTime+1;
    //BOT POWER
    if([[mySingleton shipConfigSelected] bot1]==1 && shotTime%(250-([[mySingleton shipConfigSelected] shipPowerLevel]*5))==0  && !_bot1.dead){
        [self fixPowerBot:1];
    }
    if([[mySingleton shipConfigSelected] bot2]==1 && shotTime%(250-([[mySingleton shipConfigSelected] shipPowerLevel]*5))==0  && !_bot2.dead){
        [self fixPowerBot:2];
    }
    if([[mySingleton shipConfigSelected] bot3]==1 && shotTime%(250-([[mySingleton shipConfigSelected] shipPowerLevel]*5))==0  && !_bot3.dead){
        [self fixPowerBot:3];
    }
    if([[mySingleton shipConfigSelected] bot4]==1 && shotTime%(250-([[mySingleton shipConfigSelected] shipPowerLevel]*5))==0  && !_bot4.dead){
        [self fixPowerBot:4];
    }
    //BOT SHIELD
    if([[mySingleton shipConfigSelected] bot1]==2 && shotTime%(300-([[mySingleton shipConfigSelected] shipShieldLevel]*5))==0  && !_bot1.dead){
        [self fixShieldBot:1];
    }
    if([[mySingleton shipConfigSelected] bot2]==2 && shotTime%(300-([[mySingleton shipConfigSelected] shipShieldLevel]*5))==0  && !_bot2.dead){
        [self fixShieldBot:2];
    }
    if([[mySingleton shipConfigSelected] bot3]==2 && shotTime%(300-([[mySingleton shipConfigSelected] shipShieldLevel]*5))==0  && !_bot3.dead){
        [self fixShieldBot:3];
    }
    if([[mySingleton shipConfigSelected] bot4]==2 && shotTime%(300-([[mySingleton shipConfigSelected] shipShieldLevel]*5))==0  && !_bot4.dead){
        [self fixShieldBot:4];
    }
    int shipLaserShootDelay = 70-[mySingleton shipSelected]*10;
    
    //BOT LASER
    if([[mySingleton shipConfigSelected] bot1]==3 && shotTime%(shipLaserShootDelay-([[mySingleton shipConfigSelected] shipPowerLevel]*5))==0  && !_bot1.dead){
        [self shotLaserBot1];
    }
    if([[mySingleton shipConfigSelected] bot2]==3 && shotTime%(shipLaserShootDelay-([[mySingleton shipConfigSelected] shipPowerLevel]*5))==0  && !_bot2.dead){
        [self shotLaserBot2];
    }
    if([[mySingleton shipConfigSelected] bot3]==3 && shotTime%(shipLaserShootDelay-([[mySingleton shipConfigSelected] shipPowerLevel]*5))==0  && !_bot3.dead){
        [self shotLaserBot3];
    }
    if([[mySingleton shipConfigSelected] bot4]==3 && shotTime%(shipLaserShootDelay-([[mySingleton shipConfigSelected] shipPowerLevel]*5))==0  && !_bot4.dead){
        [self shotLaserBot4];
    }
    
    //BOT ROCKET
    int shipRocketShootDelay = 90-[mySingleton shipSelected]*10;
    
    
    if([[mySingleton shipConfigSelected] bot1]==4 && shotTime%(shipRocketShootDelay-([[mySingleton shipConfigSelected] shipPowerLevel]*5))==0  && !_bot1.dead){
        [self shotRocketBot1];
    }
    if([[mySingleton shipConfigSelected] bot2]==4 && shotTime%(shipRocketShootDelay-([[mySingleton shipConfigSelected] shipPowerLevel]*5))==0  && !_bot2.dead){
       [self shotRocketBot2];
    }
    if([[mySingleton shipConfigSelected] bot3]==4 && shotTime%(shipRocketShootDelay-([[mySingleton shipConfigSelected] shipPowerLevel]*5))==0  && !_bot3.dead){
        [self shotRocketBot3];
    }
    if([[mySingleton shipConfigSelected] bot4]==4 && shotTime%(shipRocketShootDelay-([[mySingleton shipConfigSelected] shipPowerLevel]*5))==0  && !_bot4.dead){
        [self shotRocketBot4];
    }
}



- (void)updateAsteroids:(ccTime)dt {
    //if (_gameStage != GameStageAsteroids) return;
    if (_levelManager.gameState != GameStateNormal) return;
    if (![_levelManager boolForProp:@"SpawnAsteroids"]) return;
    CGSize winSize = [CCDirector sharedDirector].winSize;
    // Is it time to spawn an asteroid?
    double curTime = CACurrentMediaTime();
    if (curTime > _nextAsteroidSpawn) {
        float spawnSecsLow = [_levelManager floatForProp:@"ASpawnSecsLow"];
        float spawnSecsHigh = [_levelManager floatForProp:@"ASpawnSecsHigh"];
        float randSecs = randomValueBetween(spawnSecsLow, spawnSecsHigh);
        _nextAsteroidSpawn = randSecs + curTime;
        float randY = randomValueBetween(0.0, winSize.height);
        float moveDurationLow = [_levelManager floatForProp:@"AMoveDurationLow"];
        float moveDurationHigh = [_levelManager floatForProp:@"AMoveDurationHigh"];
        float randDuration = randomValueBetween(moveDurationLow, moveDurationHigh);
        // Create a new asteroid sprite
        GameObject *asteroid = [_asteroidsArray nextSprite];
        [asteroid stopAllActions];
        asteroid.visible = YES;
        // Set its position to be offscreen to the right
        asteroid.position = ccp(winSize.width+asteroid.contentSize.width/2, randY);
        GlobalSingleton * mySingleton = [GlobalSingleton sharedInstance];
        // Set it's size to be one of 3 random sizes
        int randNum = arc4random() % 3;
        if (randNum == 0) {
            asteroid.scale = 0.25;
            asteroid.maxHp = 1*([mySingleton gameLevel]+1);
        } else if (randNum == 1) {
            asteroid.scale = 0.5;
            asteroid.maxHp = 3*([mySingleton gameLevel]+1);
        } else {
            asteroid.scale = 1.0;
            asteroid.maxHp = 5*([mySingleton gameLevel]+1);
        }
        [asteroid revive];
        // Move it offscreen to the left, and when it's done call removeNode
        [asteroid runAction:
         [CCSequence actions:
          [CCMoveBy actionWithDuration:randDuration position:ccp(-winSize.width-asteroid.contentSize.width, 0)],
          [CCCallFuncN actionWithTarget:self selector:@selector(invisNode:)],
          nil]];
    }
}

- (void)updateCollisions:(ccTime)dt {
    for (CCSprite *laser in _laserArray.array) {
        if (!laser.visible) continue;
        for (CCSprite *asteroid in _asteroidsArray.array) {
            if (!asteroid.visible) continue;
            if (CGRectIntersectsRect(asteroid.boundingBox, laser.boundingBox)) {
                if([[GlobalSingleton sharedInstance] audioOn])[[SimpleAudioEngine sharedEngine] playEffect:@"explosion_large.caf" pitch:1.0f pan:0.0f gain:0.25f];
                asteroid.visible = NO;
                laser.visible = NO;
                break;
            }   
        }        
    }
    for (CCSprite *rocket in _rocketArray.array) {
        if (!rocket.visible) continue;
        for (CCSprite *asteroid in _asteroidsArray.array) {
            if (!asteroid.visible) continue;
            if (CGRectIntersectsRect(asteroid.boundingBox, rocket.boundingBox)) {
                if([[GlobalSingleton sharedInstance] audioOn])[[SimpleAudioEngine sharedEngine] playEffect:@"explosion_large.caf" pitch:1.0f pan:0.0f gain:0.25f];
                asteroid.visible = NO;
                rocket.visible = NO;
                break;
            }   
        }        
    }
}
/*
- (void)updateBackground:(ccTime)dt {
    CGPoint backgroundScrollVel = ccp(-1000, 0);
    _backgroundNode.position = ccpAdd(_backgroundNode.position, ccpMult(backgroundScrollVel, dt));
    NSArray *spaceDusts = [NSArray arrayWithObjects:_spacedust1, _spacedust2, nil];
    for (CCSprite *spaceDust in spaceDusts) {
        if ([_backgroundNode convertToWorldSpace:spaceDust.position].x < -spaceDust.contentSize.width*self.scale) {
            [_backgroundNode incrementOffset:ccp(2*spaceDust.contentSize.width*spaceDust.scale,0) forChild:spaceDust];
        }
    }
    NSArray *backgrounds = [NSArray arrayWithObjects:_planetsunrise, _galaxy, _spacialanomaly, _spacialanomaly2, nil];
    for (CCSprite *background in backgrounds) {
        if ([_backgroundNode convertToWorldSpace:background.position].x < -background.contentSize.width*self.scale) {
            [_backgroundNode incrementOffset:ccp(2000,0) forChild:background];
        }
    }
}
*/
- (void)updateBox2D:(ccTime)dt {
    world_->Step(dt, 1, 1);
    for(b2Body *b = world_->GetBodyList(); b; b=b->GetNext()) {
        if (b->GetUserData() != NULL) {
            GameObject *sprite = (GameObject *)b->GetUserData();
            b2Vec2 b2Position = b2Vec2(sprite.position.x/PTM_RATIO,
                                       sprite.position.y/PTM_RATIO);
            float32 b2Angle = -1 * CC_DEGREES_TO_RADIANS(sprite.rotation);
            b->SetTransform(b2Position, b2Angle);
        }
    }
}

- (void)updateShipShield:(int)shield{
   GlobalSingleton* mySingleton = [GlobalSingleton sharedInstance];
   int shieldTotal = [[powerDictionaryShip objectForKey:[NSString stringWithFormat:@"%i",[[mySingleton shipConfigSelected] shipShieldLevel]]] intValue];
    CCLOG(@"SHIELD VALUES %i  %i",[[mySingleton shipConfigSelected] shield],(shieldTotal/3 )*2);
    
    if([[mySingleton shipConfigSelected] shield]> (shieldTotal/3 )*2){
         CCLOG(@"SHIELD LEVEL 1");
        [_shield1Effect stopSystem];
        [_shield2Effect stopSystem];
        [_shield3Effect stopSystem];
        [_shield1Effect resetSystem];
        
    }else if([[mySingleton shipConfigSelected] shield] <= shieldTotal/3 *2 && [[mySingleton shipConfigSelected] shield] >= shieldTotal/3 ){
        CCLOG(@"SHIELD LEVEL 2");
        [_shield1Effect stopSystem];
        [_shield2Effect stopSystem];
        [_shield3Effect stopSystem];
        [_shield2Effect resetSystem];
    }else if([[mySingleton shipConfigSelected] shield] < shieldTotal/3 && [[mySingleton shipConfigSelected] shield]>0){
        CCLOG(@"SHIELD LEVEL 3");

        [_shield1Effect stopSystem];
        [_shield2Effect stopSystem];
        [_shield3Effect stopSystem];
        [_shield3Effect resetSystem];
    }else{
        CCLOG(@"NO SHIELD");

        [_shield1Effect stopSystem];
        [_shield2Effect stopSystem];
        [_shield3Effect stopSystem];
    }
    [_hud setShield:[NSString stringWithFormat:@"%i", shield]];
}

- (void)updatePoints:(int)points{
    [_hud setPoints:[NSString stringWithFormat:@" %i", points]];
}

- (void)updateShipPower:(int)power{
   [_hud setPower:[NSString stringWithFormat:@"%i", power]];
}

- (void)updateNukes:(int)nukes {
    CCLOG(@"NUKES UPDATE : %i",nukes);
    [_hud setNukesString:[NSString stringWithFormat:@"%i", nukes]];
}

-(void) removePower:(int)value{
    //NSLog(@"remove power");
    GlobalSingleton * mySingleton = [GlobalSingleton sharedInstance];
    //visualEffect.position = _bot1.position;
    int power = [[mySingleton shipConfigSelected] power] - value;
    int powerTotal;
    powerTotal = [[powerDictionaryShip objectForKey:[NSString stringWithFormat:@"%i",[[mySingleton shipConfigSelected] shipPowerLevel]]] intValue];
    
    if( power > 0 ){
    }else{
        power = 0;
    }
    [[mySingleton shipConfigSelected] setPower:power];
    [self updateShipPower:power];
    [mySingleton saveShip:[mySingleton shipConfigSelected] forKey:[mySingleton shipSelected]];
}

-(void) removeShield:(int)value{
    GlobalSingleton * mySingleton = [GlobalSingleton sharedInstance];
    //visualEffect.position = _bot1.position;
    int shield = [[mySingleton shipConfigSelected] shield];
    shield = shield - value;
    int shieldTotal;
    shieldTotal = [[shieldDictionaryShip objectForKey:[NSString stringWithFormat:@"%i",[[mySingleton shipConfigSelected] shipShieldLevel]]] intValue];
    
   if(shield>0){
       
    }else{
        shield=0;
        [self removePower:value];
    }
    [[mySingleton shipConfigSelected] setShield:shield];
    [self updateShipShield:shield];
    [mySingleton saveShip:[mySingleton shipConfigSelected] forKey:[mySingleton shipSelected]];
}

- (void)updateLevel:(ccTime)dt {
    BOOL newStage = [_levelManager update];
    if (newStage) {
        [self newStageStarted];
    }
    if (_wantNextStage) {
        _wantNextStage = NO;
        [_levelManager nextStage];
        [self newStageStarted];
    }
}

-(void)enableShoot{
    botCanShoot = YES;
}

-(void)removeChat{
    [self removeChildByTag:1000 cleanup:YES]; 
    [self removeChildByTag:1000 cleanup:YES]; 
    [self enableShoot];
}

- (void)doLevelIntro {
    botCanShoot = NO;
    CGSize winSize = [CCDirector sharedDirector].winSize;
    NSString *fontName = @"SpaceGameFont.fnt";
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        fontName = @"SpaceGameFont-hd.fnt";
    }
    NSString *message1 = [NSString stringWithFormat:@"Level %d", _levelManager.curLevelIdx+1];
    NSString *message2 = [_levelManager stringForProp:@"LText"];
    _levelIntroLabel1 = [CCLabelBMFont labelWithString:message1 fntFile:fontName];
    _levelIntroLabel1.scale = 0;
    _levelIntroLabel1.position = ccp(winSize.width/2, winSize.height * 0.6);
    [self addChild:_levelIntroLabel1 z:100];
    [_levelIntroLabel1 runAction:
     [CCSequence actions:
      [CCEaseOut actionWithAction:
       [CCScaleTo actionWithDuration:0.5 scale:0.5] rate:4.0],
      [CCDelayTime actionWithDuration:3.0],
      [CCEaseOut actionWithAction:
       [CCScaleTo actionWithDuration:0.5 scale:0] rate:4.0],
      [CCCallFuncN actionWithTarget:self selector:@selector(removeNode:)],
      nil]];
    _levelIntroLabel2 = [CCLabelBMFont labelWithString:message2 fntFile:fontName];
    _levelIntroLabel2.position = ccp(winSize.width/2, winSize.height * 0.4);
    _levelIntroLabel2.scale = 0;
    [self addChild:_levelIntroLabel2 z:100];
    [_levelIntroLabel2 runAction:
     [CCSequence actions:
      [CCEaseOut actionWithAction:
       [CCScaleTo actionWithDuration:0.5 scale:0.5] rate:4.0],
      //[CCCallFuncN actionWithTarget:self selector:@selector(enableShoot)],
      [CCDelayTime actionWithDuration:3.0],
      [CCEaseOut actionWithAction:
       [CCScaleTo actionWithDuration:0.5 scale:0] rate:4.0],
      [CCCallFuncN actionWithTarget:self selector:@selector(removeNode:)], nil]];
    if([_levelManager stringForProp:@"ReptilText1"]==nil)[self enableShoot];
}

- (void)doLevelChat {
    botCanShoot = NO;
    //CGSize winSize = [CCDirector sharedDirector].winSize;
    id actionFadeIn = [CCFadeIn actionWithDuration:1.0f];
	id actionFadeOut = [actionFadeIn reverse];
    NSString *fontNameConf = @"Arial-gray.fnt";
    NSString *fontNameRept = @"Arial-yellow.fnt";
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        fontNameConf = @"Arial-gray-hd.fnt";
        fontNameRept = @"Arial-yellow-hd.fnt";
    }
    CCSprite* chatReptilBG = [CCSprite spriteWithSpriteFrameName:@"reptilians-chat.png"];
    chatReptilBG.opacity = 0;
    [self addChild:chatReptilBG z:1 tag:1000];
    CCSprite* chatConfBG = [CCSprite spriteWithSpriteFrameName:@"intergalactic-confederation-chat.png"];
    chatConfBG.opacity = 0;
    [self addChild:chatConfBG z:1 tag:1000];
    
    chatReptilBG.position = ccp((330*scaleFactor)+px,(220*scaleFactor)+py);
    chatConfBG.position = ccp((150*scaleFactor)+px,(90*scaleFactor)+py);
    
    float duration = [_levelManager floatForProp:@"Duration"];
    
    [chatReptilBG runAction:
     [CCSequence actions:
      [CCDelayTime actionWithDuration:duration/8],
       actionFadeIn,
      [CCDelayTime actionWithDuration:duration/8*7],
      actionFadeOut,
      [CCCallFuncN actionWithTarget:self selector:@selector(removeNode:)],
      nil]];
    [chatConfBG runAction:
     [CCSequence actions:
      [CCDelayTime actionWithDuration:duration/8*2],      
      actionFadeIn,
      [CCDelayTime actionWithDuration:duration/8*6],
      actionFadeOut,
      [CCCallFuncN actionWithTarget:self selector:@selector(removeNode:)],
      nil]];
    
    NSString *messageReptil1 = [_levelManager stringForProp:@"ReptilText1"];
    NSString *messageReptil2 = [_levelManager stringForProp:@"ReptilText2"];
    NSString *messageReptil3 = [_levelManager stringForProp:@"ReptilText3"];
   
	NSString *messageConf1 = [_levelManager stringForProp:@"ConfText1"];
    NSString *messageConf2 = [_levelManager stringForProp:@"ConfText2"];
    NSString *messageConf3 = [_levelManager stringForProp:@"ConfText3"];
    
    _levelChatLabel1 = [CCLabelBMFont labelWithString:messageReptil1 fntFile:fontNameRept];
    _levelChatLabel1.anchorPoint = ccp(0,0.5);
    _levelChatLabel1.position = ccp((190*scaleFactor)+px,(230*scaleFactor)+py);
    _levelChatLabel1.opacity = 0;
    [self addChild:_levelChatLabel1 z:100];
    [_levelChatLabel1 runAction:
     [CCSequence actions:
      [CCDelayTime actionWithDuration:duration/7*1],      
      actionFadeIn,
      [CCDelayTime actionWithDuration:duration/7*6],
      actionFadeOut,
      [CCCallFuncN actionWithTarget:self selector:@selector(removeNode:)],
      nil]];
    
    _levelChatLabel2 = [CCLabelBMFont labelWithString:messageReptil2 fntFile:fontNameRept];
    _levelChatLabel2.anchorPoint = ccp(0,0.5);
    _levelChatLabel2.position =  ccp((190*scaleFactor)+px,(210*scaleFactor)+py);
    _levelChatLabel2.opacity = 0;
    [self addChild:_levelChatLabel2 z:100];
    [_levelChatLabel2 runAction:
     [CCSequence actions:
      [CCDelayTime actionWithDuration:duration/7*2],      
      actionFadeIn,
      [CCDelayTime actionWithDuration:duration/7*5],
      actionFadeOut,
      [CCCallFuncN actionWithTarget:self selector:@selector(removeNode:)],
      nil]];
    
    _levelChatLabel3 = [CCLabelBMFont labelWithString:messageReptil3 fntFile:fontNameRept];
    _levelChatLabel3.anchorPoint = ccp(0,0.5);
    _levelChatLabel3.position =  ccp((190*scaleFactor)+px,(190*scaleFactor)+py);
    _levelChatLabel3.opacity = 0;
    [self addChild:_levelChatLabel3 z:100];
    [_levelChatLabel3 runAction:
     [CCSequence actions:
      [CCDelayTime actionWithDuration:duration/7*3],
      actionFadeIn,
      [CCDelayTime actionWithDuration:duration/7*4],
      actionFadeOut,
      [CCCallFuncN actionWithTarget:self selector:@selector(removeNode:)],
      nil]];
    
    _levelChatLabel4 = [CCLabelBMFont labelWithString:messageConf1 fntFile:fontNameConf];
    _levelChatLabel4.anchorPoint = ccp(1,0.5);
    _levelChatLabel4.position =  ccp((290*scaleFactor)+px,(110*scaleFactor)+py);
    _levelChatLabel4.opacity = 0;
    [self addChild:_levelChatLabel4 z:100];
    [_levelChatLabel4 runAction:
     [CCSequence actions:
      [CCDelayTime actionWithDuration:duration/7*4],
      actionFadeIn,
      [CCDelayTime actionWithDuration:duration/7*3],
      actionFadeOut,
      [CCCallFuncN actionWithTarget:self selector:@selector(removeNode:)],
      nil]];
    
    _levelChatLabel5 = [CCLabelBMFont labelWithString:messageConf2 fntFile:fontNameConf];
    _levelChatLabel5.anchorPoint = ccp(1,0.5);
    _levelChatLabel5.position =  ccp((290*scaleFactor)+px,(90*scaleFactor)+py);
    _levelChatLabel5.opacity = 0;
    [self addChild:_levelChatLabel5 z:100];
    [_levelChatLabel5 runAction:
     [CCSequence actions:
      [CCDelayTime actionWithDuration:duration/7*5],      
      actionFadeIn,
      [CCDelayTime actionWithDuration:duration/7*2],
      actionFadeOut,
      [CCCallFuncN actionWithTarget:self selector:@selector(removeNode:)],
      nil]];
    
    _levelChatLabel6 = [CCLabelBMFont labelWithString:messageConf3 fntFile:fontNameConf];
    _levelChatLabel6.anchorPoint = ccp(1,0.5);
    _levelChatLabel6.position =  ccp((290*scaleFactor)+px,(70*scaleFactor)+py);
    _levelChatLabel6.opacity = 0;
    [self addChild:_levelChatLabel6 z:100];
    [_levelChatLabel6 runAction:
     [CCSequence actions:
      [CCDelayTime actionWithDuration:duration/7*6],
      actionFadeIn,
      [CCDelayTime actionWithDuration:duration/7],
      actionFadeOut,
      [CCCallFuncN actionWithTarget:self selector:@selector(removeNode:)],
      [CCCallFuncN actionWithTarget:self selector:@selector(removeChat)],
      nil]];
}

- (void)spawnBoss {
    CGSize winSize = [CCDirector sharedDirector].winSize;    
    _boss.position = ccp(winSize.width*1.2, winSize.height*1.2);
    [_boss revive];
    [self shakeScreen:30];
    if([[GlobalSingleton sharedInstance] audioOn])[[SimpleAudioEngine sharedEngine] playEffect:@"boss.caf"];
}

- (void)newStageStarted {
    if (_levelManager.gameState == GameStateDone) {
        [self endScene:YES];
    } else if ([_levelManager boolForProp:@"SpawnLevelIntro"] && ![_ship dead]) {
        [self doLevelIntro];
    } else if ([_levelManager boolForProp:@"SpawnLevelChat"] && ![_ship dead]){
       [self doLevelChat];     
    } 
    if (_levelManager.gameState == GameStateNormal && [_levelManager hasProp:@"SpawnBoss"]  && ![_ship dead]) {
        [self spawnBoss];
    }
}

- (void)shootCannonBallAtShipFromPosition:(CGPoint)position {
    CGSize winSize = [CCDirector sharedDirector].winSize;
    GameObject *cannonBall = [_cannonBalls nextSprite];
    if([[GlobalSingleton sharedInstance] audioOn])[[SimpleAudioEngine sharedEngine] playEffect:@"cannon.caf" pitch:1.0f pan:0.0f gain:0.25f];
    
    CGPoint shootVector = ccpNormalize(ccpSub(_ship.position, position));    
    CGPoint shootTarget = ccpMult(shootVector, winSize.width*2);
    cannonBall.position = position;
    [cannonBall revive];
    [cannonBall runAction:
     [CCSequence actions:
      [CCMoveBy actionWithDuration:5.0 position:shootTarget],
      [CCCallFuncN actionWithTarget:self selector:@selector(invisNode:)],
      nil]];
}

- (void)shootEnemyLaserFromPosition:(CGPoint)position {
    
    CGSize winSize = [CCDirector sharedDirector].winSize;
    GameObject *shipLaser = [_enemyLasers nextSprite];
    if([[GlobalSingleton sharedInstance] audioOn])[[SimpleAudioEngine sharedEngine] playEffect:@"laser_enemy.caf" pitch:1.0f pan:0.0f gain:0.25f];
    shipLaser.position = position;
    [shipLaser revive];
    [shipLaser stopAllActions];
    [shipLaser runAction:
     [CCSequence actions:
      [CCMoveBy actionWithDuration:3.0 position:ccp(-winSize.width*1.6, 0)],
      [CCCallFuncN actionWithTarget:self selector:@selector(invisNode:)],
      nil]];
}

- (void)updateAlienSwarm:(ccTime)dt {
    
    if (_levelManager.gameState != GameStateNormal) return;
    if (![_levelManager hasProp:@"SpawnAlienSwarm"]) return;
    
    CGSize winSize = [CCDirector sharedDirector].winSize;
    
    double curTime = CACurrentMediaTime();
    if (curTime > _nextAlienSpawn) {
        
        if (_numAlienSpawns == 0) {
            CGPoint pos1 = ccp(winSize.width*1.3, 
                               randomValueBetween(0, winSize.height*0.1));
            CGPoint cp1 = ccp(randomValueBetween(winSize.width*0.1, winSize.width*0.6), 
                              randomValueBetween(0, winSize.height*0.3));
            CGPoint pos2 = ccp(winSize.width*1.3, 
                               randomValueBetween(winSize.height*0.9, winSize.height*1.0));
            CGPoint cp2 = ccp(randomValueBetween(winSize.width*0.1, winSize.width*0.6), 
                              randomValueBetween(winSize.height*0.7, winSize.height*1.0));
            _numAlienSpawns = arc4random() % 20 + 1;
            if (arc4random() % 2 == 0) {
                _alienSpawnStart = pos1;
                _bezierConfig.controlPoint_1 = cp1;
                _bezierConfig.controlPoint_2 = cp2;
                _bezierConfig.endPosition = pos2;
            } else {
                _alienSpawnStart = pos2;
                _bezierConfig.controlPoint_1 = cp2;
                _bezierConfig.controlPoint_2 = cp1;
                _bezierConfig.endPosition = pos1;
            }
            
            _nextAlienSpawn = curTime + 1.0;
            
        } else {
            
            _nextAlienSpawn = curTime + 0.3;
            
            _numAlienSpawns -= 1;
            
            GameObject *alien = [_alienArray nextSprite];
            alien.position = _alienSpawnStart;
            [alien revive];
            
            [alien runAction:[CCBezierTo actionWithDuration:3.0 bezier:_bezierConfig]];
            
        }
    } 
    
    if (curTime > _nextShootChance) {
        
        _nextShootChance = curTime + 0.1;
        
        for (GameObject *alien in _alienArray.array) {
            if (alien.visible) {
                if (arc4random() % 40 == 0) {
                    [self shootEnemyLaserFromPosition:alien.position];
                }
            }
        }        
    }
}


- (void)updateAlienSwarm2:(ccTime)dt {
    
    if (_levelManager.gameState != GameStateNormal) return;
    if (![_levelManager hasProp:@"SpawnAlienSwarm2"]) return;
    
    CGSize winSize = [CCDirector sharedDirector].winSize;
    
    double curTime = CACurrentMediaTime();
    if (curTime > _nextAlienSpawn) {
        
        if (_numAlienSpawns == 0) {
            CGPoint pos1 = ccp(winSize.width*1.3, 
                               randomValueBetween(0, winSize.height*0.1));
            CGPoint cp1 = ccp(randomValueBetween(winSize.width*0.1, winSize.width*0.6), 
                              randomValueBetween(0.8, winSize.height*1.5));
            CGPoint pos2 = ccp(winSize.width*1.3, 
                               randomValueBetween(winSize.height*0.9, winSize.height*1.0));
            CGPoint cp2 = ccp(randomValueBetween(winSize.width*0.1, winSize.width*0.6), 
                              randomValueBetween(winSize.height*0.2, winSize.height*-0.5));
            _numAlienSpawns = arc4random() % 20 + 1;
            if (arc4random() % 2 == 0) {
                _alienSpawnStart = pos1;
                _bezierConfig.controlPoint_1 = cp1;
                _bezierConfig.controlPoint_2 = cp2;
                _bezierConfig.endPosition = pos2;
            } else {
                _alienSpawnStart = pos2;
                _bezierConfig.controlPoint_1 = cp2;
                _bezierConfig.controlPoint_2 = cp1;
                _bezierConfig.endPosition = pos1;
            }
            
            _nextAlienSpawn = curTime + 1.0;
            
        } else {
            
            _nextAlienSpawn = curTime + 0.3;
            
            _numAlienSpawns -= 1;
            
            GameObject *alien = [_alienArray nextSprite];
            alien.position = _alienSpawnStart;
            [alien revive];
            
            [alien runAction:[CCBezierTo actionWithDuration:4.0 bezier:_bezierConfig]];
            
        }
    } 
    
    if (curTime > _nextShootChance) {
        
        _nextShootChance = curTime + 0.1;
        
        for (GameObject *alien in _alienArray.array) {
            if (alien.visible) {
                if (arc4random() % 30 == 0) {
                    [self shootEnemyLaserFromPosition:alien.position];
                }
            }
        }        
    }
}


- (void)updatePowerups:(ccTime)dt {
    
    if (_levelManager.gameState != GameStateNormal) return;
    if (![_levelManager boolForProp:@"SpawnPowerups"]) return;
    CGSize winSize = [CCDirector sharedDirector].winSize;
    double curTime = CACurrentMediaTime();
    if (curTime > _nextPowerupSpawn) {            
        _nextPowerupSpawn = curTime + [_levelManager floatForProp:@"PSpawnSecs"];
        GameObject * powerup = [_powerups nextSprite];
        powerup.position = ccp(winSize.width, randomValueBetween(0, winSize.height));
        [powerup revive];
        [powerup runAction:
         [CCSequence actions:
          [CCMoveBy actionWithDuration:5.0 position:ccp(-winSize.width*1.5, 0)],
          [CCCallFuncN actionWithTarget:self selector:@selector(invisNode:)],
          nil]]; 
    }   
}


- (void)updateBoostEffects:(ccTime)dt  {
    for (CCParticleSystemQuad * particleSystem in _boostEffects.array) {
        particleSystem.position = _ship.position;
    }
}

- (void)updateTrailEffects:(ccTime)dt  {
    for (CCParticleSystemQuad * particleSystem in _trailEffects.array) {
        particleSystem.position = ccp(_ship.position.x-30,_ship.position.y);
        particleSystem.angle = -180;
    }
}


- (void)updateBoss:(ccTime)dt {
    if (_levelManager.gameState != GameStateNormal) return;
    if (![_levelManager boolForProp:@"SpawnBoss"]) return;
    if (_boss.visible) {
        [_boss updateWithShipPosition:_ship.position];
    } 
}

- (void)update:(ccTime)dt {
    
    [self updateTrailEffects:dt];
    [self updateShipPos:dt];
    [self updateAsteroids:dt];
    //[self updateCollisions:dt];
    //[self updateBackground:dt];    
    [self updateBox2D:dt];
    [self updateLevel:dt];
    
    [self updateAlienSwarm:dt];
    
    [self updateAlienSwarm2:dt];
    
    [self updatePowerups:dt];
    
    [self updateBoostEffects:dt];
    

    
    [self updateBoss:dt];
    
    if(_ship.dead){
        [_bot1 destroy];
        [_bot2 destroy];
        [_bot3 destroy];
        [_bot4 destroy];
    }
}

- (void)accelerometer:(UIAccelerometer *)accelerometer 
        didAccelerate:(UIAcceleration *)acceleration {
    
    #define kFilteringFactorX 0.75
    #define kFilteringFactorY 1.0
    
    static UIAccelerationValue rollingX = 0, rollingY = 0, rollingZ = 0;
    
    rollingX = (acceleration.x * kFilteringFactorX) + 
        (rollingX * (1.0 - kFilteringFactorX));    
    rollingY = (acceleration.y * kFilteringFactorY) + 
        (rollingY * (1.0 - kFilteringFactorY));    
    rollingZ = (acceleration.z * kFilteringFactorX) + 
        (rollingZ * (1.0 - kFilteringFactorX));
    
    float accelX = rollingX;
    float accelY = rollingY;
  //  float accelZ = rollingZ;
   
    CGSize winSize = [CCDirector sharedDirector].winSize;

    #define kRestAccelX 0.6
    #define kShipMaxPointsPerSec (winSize.height*0.3)
    #define kMaxDiffX 0.2
    
    #define kRestAccelY 0.01
    #define kShipMaxPointsPerSecY (winSize.width*0.5)
    #define kMaxDiffY 0.2

    float accelDiffX = kRestAccelX - ABS(accelX);
    float accelFractionX = accelDiffX / kMaxDiffX;
    float pointsPerSecX = kShipMaxPointsPerSec * accelFractionX;
    
    float accelDiffY= kRestAccelY - accelY;
    float accelFractionY = accelDiffY / kMaxDiffY;
    float pointsPerSecY = kShipMaxPointsPerSecY * accelFractionY;

    _shipPointsPerSecY = pointsPerSecX;
	
    
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
	if (orientation == UIDeviceOrientationLandscapeRight) {
        _shipPointsPerSecX = -pointsPerSecY; 
    } else if( orientation == UIDeviceOrientationLandscapeLeft) {
        _shipPointsPerSecX = pointsPerSecY;
    }
}

- (void)ccTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    GlobalSingleton * mySingleton = [GlobalSingleton sharedInstance];
    if (_ship == nil || _ship.dead) return;
    
    UITouch* touch =  [touches anyObject];
    lastTouchPosition = [touch locationInView:touch.view];
    
    //CCLOG(@"TOUCH Y %f",lastTouchPosition.y-310);
    //CCLOG(@"TOUCH X %f",lastTouchPosition.x);
    
    CGSize winSize = [CCDirector sharedDirector].winSize;
    
    if([[GlobalSingleton sharedInstance] audioOn])[[SimpleAudioEngine sharedEngine] playEffect:@"laser_ship.caf" pitch:1.0f pan:0.0f gain:0.25f];
    
    GameObject *shipLaser = [_laserArray nextSprite];
    [shipLaser stopAllActions];
    //shipLaser.scaleX = 10;
    shipLaser.position = ccpAdd(_ship.position, ccp(shipLaser.contentSize.width/2, 0));
    [shipLaser revive];
    
    
    shipLaser.position = ccpAdd(_ship.position, ccp(shipLaser.contentSize.width/2, 0));
    [shipLaser runAction:
     [CCSequence actions:
      [CCMoveBy actionWithDuration:0.5 position:ccp(winSize.width, 0)],
      [CCCallFuncN actionWithTarget:self selector:@selector(invisNode:)],
      nil]];
    
    if([(ShipConfig*)[mySingleton shipConfigSelected] shipPowerLevel]>0){
        GameObject *shipLaser2 = [_laserArray nextSprite];
        [shipLaser2 stopAllActions];
        shipLaser2.position = ccpAdd(_ship.position, ccp(shipLaser2.contentSize.width/2, 8));
        [shipLaser2 revive];
    
        [shipLaser2 runAction:
         [CCSequence actions:
          [CCMoveBy actionWithDuration:0.5 position:ccp(winSize.width, 16)],
          [CCCallFuncN actionWithTarget:self selector:@selector(invisNode:)],
          nil]];
    }
    if([(ShipConfig*)[mySingleton shipConfigSelected] shipPowerLevel]>2){
        GameObject *shipLaser3 = [_laserArray nextSprite];
        [shipLaser3 stopAllActions];
        shipLaser3.position = ccpAdd(_ship.position, ccp(shipLaser3.contentSize.width/2, -8));
        [shipLaser3 revive];
        
        [shipLaser3 runAction:
         [CCSequence actions:
          [CCMoveBy actionWithDuration:0.5 position:ccp(winSize.width, -16)],
          [CCCallFuncN actionWithTarget:self selector:@selector(invisNode:)],
          nil]];
    }
    if([(ShipConfig*)[mySingleton shipConfigSelected] shipPowerLevel]>3){
        GameObject *shipLaser4 = [_laserArray nextSprite];
        [shipLaser4 stopAllActions];
        shipLaser4.position = ccpAdd(_ship.position, ccp(shipLaser4.contentSize.width/2, 16));
        [shipLaser4 revive];
        [shipLaser4 runAction:
         [CCSequence actions:
          [CCMoveBy actionWithDuration:0.5 position:ccp(winSize.width, 24)],
          [CCCallFuncN actionWithTarget:self selector:@selector(invisNode:)],
          nil]];
    }
    if([(ShipConfig*)[mySingleton shipConfigSelected] shipPowerLevel]>4){
        GameObject *shipLaser4 = [_laserArray nextSprite];
        [shipLaser4 stopAllActions];
        shipLaser4.position = ccpAdd(_ship.position, ccp(shipLaser4.contentSize.width/2, -16));
        [shipLaser4 revive];
        [shipLaser4 runAction:
         [CCSequence actions:
          [CCMoveBy actionWithDuration:0.5 position:ccp(winSize.width, -24)],
          [CCCallFuncN actionWithTarget:self selector:@selector(invisNode:)],
          nil]];
    }
}

-(void) draw {   
    glDisable(GL_TEXTURE_2D);
    //glDisableClientState(GL_COLOR_ARRAY);
    //glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    
    //world_->DrawDebugData();
    
    glEnable(GL_TEXTURE_2D);
    //glEnableClientState(GL_COLOR_ARRAY);
    //glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    
    /*if (_levelManager.gameState == GameStateNormal &&
        [_levelManager boolForProp:@"SpawnAlienSwarm"]) {
        
        ccDrawCubicBezier(_alienSpawnStart, _bezierConfig.controlPoint_1, _bezierConfig.controlPoint_2, _bezierConfig.endPosition, 16);
        ccDrawLine(_alienSpawnStart, _bezierConfig.controlPoint_1);
        ccDrawLine(_bezierConfig.endPosition, _bezierConfig.controlPoint_2);
        
    }*/
}

- (void)boostDone {
    
    _invincible = NO;
    for (CCParticleSystemQuad * boostEffect in _boostEffects.array) {
        [boostEffect stopSystem];
    }
    
}

- (void)beginContact:(b2Contact *)contact {
    
    b2Fixture *fixtureA = contact->GetFixtureA();
    b2Fixture *fixtureB = contact->GetFixtureB();
    b2Body *bodyA = fixtureA->GetBody();
    b2Body *bodyB = fixtureB->GetBody();
    GameObject *spriteA = (GameObject *) bodyA->GetUserData();
    GameObject *spriteB = (GameObject *) bodyB->GetUserData();
    
    if (!spriteA.visible || !spriteB.visible) return;
    
    b2WorldManifold manifold;
    contact->GetWorldManifold(&manifold);
    b2Vec2 b2ContactPoint = manifold.points[0];
    CGPoint contactPoint = ccp(b2ContactPoint.x * PTM_RATIO, b2ContactPoint.y * PTM_RATIO);
    
    CGSize winSize = [CCDirector sharedDirector].winSize;
    
    if ((fixtureA->GetFilterData().categoryBits & kCategoryShipLaser && fixtureB->GetFilterData().categoryBits & kCategoryEnemy) ||
        (fixtureB->GetFilterData().categoryBits & kCategoryShipLaser && fixtureA->GetFilterData().categoryBits & kCategoryEnemy)) { 
        
        // Determine enemy ship and laser
        GameObject *enemyShip = (GameObject*) spriteA;
        GameObject *laser = (GameObject *) spriteB;
        if (fixtureB->GetFilterData().categoryBits & kCategoryEnemy) {
            enemyShip = (GameObject*) spriteB;
            laser = (GameObject*) spriteA;
        }
        
        // Make sure not already dead
        if (!enemyShip.dead && !laser.dead) {
            
            [enemyShip takeHit];
            [laser takeHit];
            
            if ([enemyShip dead]) {
                GlobalSingleton* mySingleton = [GlobalSingleton sharedInstance];
                if([[GlobalSingleton sharedInstance] audioOn])[[SimpleAudioEngine sharedEngine] playEffect:@"explosion_large.caf" pitch:1.0f pan:0.0f gain:0.25f];
                
                CCParticleSystemQuad *explosion = [_explosions nextParticleSystem];
                
                if (enemyShip == _boss) {
                    _wantNextStage = YES;
                    [self shakeScreen:6];
                    explosion.scale *= 1.0;
                    [mySingleton setMyNukes:([mySingleton myNukes]+(5000 * ([mySingleton gameLevel])))];
                    [mySingleton setMyPoints:([mySingleton myPoints]+(500 * ([mySingleton gameLevel])))];
                }else{
                    if (enemyShip.maxHp >= 5*([mySingleton gameLevel])) {
                        [self shakeScreen:6];
                        explosion.scale *= 1.0;
                        [mySingleton setMyNukes:([mySingleton myNukes]+(500 * ([mySingleton gameLevel])))];
                        [mySingleton setMyPoints:([mySingleton myPoints]+(50 * ([mySingleton gameLevel])))];
                    
                    } else if (enemyShip.maxHp >= 3*([mySingleton gameLevel])) {
                        [self shakeScreen:3*([mySingleton gameLevel])];
                        explosion.scale *= 0.5;
                        [mySingleton setMyNukes:([mySingleton myNukes]+(100 * ([mySingleton gameLevel])))];
                        [mySingleton setMyPoints:([mySingleton myPoints]+(10 * ([mySingleton gameLevel])))];
                    
                    } else {
                        [self shakeScreen:1];
                        explosion.scale *= 0.25;
                        [mySingleton setMyNukes:([mySingleton myNukes]+(50 * ([mySingleton gameLevel])))];
                        [mySingleton setMyPoints:([mySingleton myPoints]+(5 * ([mySingleton gameLevel])))];
                    }    
                }
                [self updateNukes:[mySingleton myNukes]];
                [self updatePoints:[mySingleton myPoints]];
                
                explosion.position = contactPoint;
                [explosion resetSystem];   
                
            } else {
                
                if([[GlobalSingleton sharedInstance] audioOn])[[SimpleAudioEngine sharedEngine] playEffect:@"explosion_small.caf" pitch:1.0f pan:0.0f gain:0.25f];

                CCParticleSystemQuad *explosion = [_explosions nextParticleSystem];
                explosion.scale *= 0.25;
                explosion.position = contactPoint;
                [explosion resetSystem]; 
                
            }
            
        }
        
    }
    
    if ((fixtureA->GetFilterData().categoryBits & kCategoryShip && fixtureB->GetFilterData().categoryBits & kCategoryEnemy) ||
        (fixtureB->GetFilterData().categoryBits & kCategoryShip && fixtureA->GetFilterData().categoryBits & kCategoryEnemy)) { 
        
        // Determine enemy ship
        GameObject *enemyShip = (GameObject*) spriteA;
        if (fixtureB->GetFilterData().categoryBits & kCategoryEnemy) {
            enemyShip = spriteB;
        }
        // ship touches update start 
        // Make sure not already dead
       // if([enemyShip  isKindOfClass:[BossShip class]]){
         if (enemyShip == _boss) {
             _wantNextStage = YES;
             
             [enemyShip takeHit];
             if ([enemyShip dead]) {
                 GlobalSingleton* mySingleton = [GlobalSingleton sharedInstance];
                 if([[GlobalSingleton sharedInstance] audioOn])[[SimpleAudioEngine sharedEngine] playEffect:@"explosion_large.caf" pitch:1.0f pan:0.0f gain:0.25f];
                 
                 CCParticleSystemQuad *explosion = [_explosions nextParticleSystem];
                 
                [self shakeScreen:6];
                explosion.scale *= 1.0;
                [mySingleton setMyNukes:([mySingleton myNukes]+(5000 * ([mySingleton gameLevel])))];
                [mySingleton setMyPoints:([mySingleton myPoints]+(500 * ([mySingleton gameLevel])))];
                
                 [self updateNukes:[mySingleton myNukes]];
                 [self updatePoints:[mySingleton myPoints]];
                 
                 explosion.position = contactPoint;
                 [explosion resetSystem];   
                 
             } else 
                 // ship touches update end 
            if (!enemyShip.dead) {
                     if([[GlobalSingleton sharedInstance] audioOn])[[SimpleAudioEngine sharedEngine] playEffect:@"explosion_large.caf" pitch:1.0f pan:0.0f gain:0.25f];
                     [self shakeScreen:1];
                     CCParticleSystemQuad *explosion = [_explosions nextParticleSystem];    
                     explosion.scale *= 0.5;
                     explosion.position = contactPoint;
                     [explosion resetSystem];
                     [enemyShip destroy];    
                     
                     GlobalSingleton* mySingleton = [GlobalSingleton sharedInstance];
                     int shieldRemoveValue = 2*(([[mySingleton levelSelected] intValue])+1);
                     [self removeShield:shieldRemoveValue];
                     [_ship takeHitWithValue:shieldRemoveValue];
                     [enemyShip destroy];
                     
                     if (_ship.dead) {
                         
                         _levelManager.gameState = GameStateDone;
                         [self endScene:NO];
                     }
            }
        //not bosss  
        }else{
            
            if (!enemyShip.dead) {
                if( _invincible == YES){
                    GlobalSingleton* mySingleton = [GlobalSingleton sharedInstance];
                    
                    if (enemyShip.maxHp >= 5*[mySingleton gameLevel]) {
                        [mySingleton setMyNukes:([mySingleton myNukes]+(500 * ([mySingleton gameLevel])))];
                        [mySingleton setMyPoints:([mySingleton myPoints]+(50 * ([mySingleton gameLevel])))];
                        
                    } else if (enemyShip.maxHp >= 3*[mySingleton gameLevel]) {
                        [mySingleton setMyNukes:([mySingleton myNukes]+(100 * ([mySingleton gameLevel])))];
                        [mySingleton setMyPoints:([mySingleton myPoints]+(10 * ([mySingleton gameLevel])))];
                        
                    } else {
                        [mySingleton setMyNukes:([mySingleton myNukes]+(50 * ([mySingleton gameLevel])))];
                        [mySingleton setMyPoints:([mySingleton myPoints]+(5 * ([mySingleton gameLevel])))];
                    }  
                    [self updateNukes:[mySingleton myNukes]];
                    [self updatePoints:[mySingleton myPoints]];
                
                
                }
                if([[GlobalSingleton sharedInstance] audioOn])[[SimpleAudioEngine sharedEngine] playEffect:@"explosion_large.caf" pitch:1.0f pan:0.0f gain:0.25f];
                
               // [self shakeScreen:1];
                CCParticleSystemQuad *explosion = [_explosions nextParticleSystem];    
                explosion.scale *= 0.5;
                explosion.position = contactPoint;
                [explosion resetSystem];        
                
                [enemyShip destroy];
                if (!_invincible) {
                    GlobalSingleton* mySingleton = [GlobalSingleton sharedInstance];
                    int shieldRemoveValue = 2*([mySingleton gameLevel]);
                    [self removeShield:shieldRemoveValue];
                    [_ship takeHitWithValue:shieldRemoveValue];
                }
                
                if (_ship.dead) {
                    _levelManager.gameState = GameStateDone;
                    [self endScene:NO];
                }
                
            }
            
        }
       
        
        
    }
   
    
    if ((fixtureA->GetFilterData().categoryBits & kCategoryShip && fixtureB->GetFilterData().categoryBits & kCategoryPowerup) ||
        (fixtureB->GetFilterData().categoryBits & kCategoryShip && fixtureA->GetFilterData().categoryBits & kCategoryPowerup)) { 
        
        // Determine power up
        GameObject *powerUp = (GameObject*) spriteA;
        if (fixtureB->GetFilterData().categoryBits & kCategoryPowerup) {
            powerUp = spriteB;
        }
        
        if (!powerUp.dead) {            
            if([[GlobalSingleton sharedInstance] audioOn])[[SimpleAudioEngine sharedEngine] playEffect:@"powerup.caf" pitch:1.0 pan:0.0 gain:1.0];
            
            [powerUp destroy];
            // TODO: Make the powerup do something! 
            float scaleDuration = 1.0;
            float waitDuration = 5.0;
            _invincible = YES;
            CCParticleSystemQuad *boostEffect = [_boostEffects nextParticleSystem];
            [boostEffect resetSystem];

            [_ship runAction:
             [CCSequence actions:
              [CCMoveBy actionWithDuration:scaleDuration position:ccp(winSize.width * 0.6, 0)],
              [CCDelayTime actionWithDuration:waitDuration/10*8],
              [CCMoveBy actionWithDuration:scaleDuration position:ccp(-winSize.width * 0.6, 0)],
              nil]];

            [self runAction:
             [CCSequence actions:
              [CCDelayTime actionWithDuration:scaleDuration],
              //[CCScaleTo actionWithDuration:scaleDuration scale:0.75],
              [CCDelayTime actionWithDuration:waitDuration],
              [CCDelayTime actionWithDuration:scaleDuration],
              //[CCScaleTo actionWithDuration:scaleDuration scale:1.0],
              [CCCallFunc actionWithTarget:self selector:@selector(boostDone)],
              nil]]; 
        }                
    }
    
}

- (void)endContact:(b2Contact *)contact {
    
}

- (void)dealloc {
    [_shield1Effect release];
    [_shield2Effect release];
    [_shield3Effect release];
    
    [_fire1Effect release];
    [_fire12Effect release];
    
    [_smoke1Effect release];
    [_smoke11Effect release];
    [_smoke2Effect release];
    [_smoke21Effect release];

    [_explosions release];
    [_asteroidsArray release];
    [_laserArray release];
    [_levelManager release];
    [_alienArray release];
    [_enemyLasers release];
    [_powerups release];
    [_boostEffects release];
    [_trailEffects release];
    [_cannonBalls release];
    
    [animationUp release];
    [animationUpStop release];
    [animationDown release];
    [animationDownStop release];

    
    [super dealloc];
}

@end


