//
//  BotConfig.h
//  MiniSpaceFighters
//
//  Created by Gil Beyruth on 9/12/11.
//  Copyright 2011 Ray Wenderlich. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface BotConfig : NSObject {
    NSString* botName;
    int botType;
    int botValue;
}
-(void)initNewBot:(NSString*)_botName withType:(int)_botType withValue:(int)_botValue;

@property (nonatomic,readwrite) int botValue,botType;

@end
