//
//  HUDLayer.mm .h
//  MiniSpaceFighters
//
//  Created by Gil Beyruth on 9/8/11.
//  Copyright 2011 Ray Wenderlich. All rights reserved.
//

#import "cocos2d.h"

@interface HUDLayer : CCLayer {
    

    CCLabelBMFont * _nukesLabel;
    CCLabelBMFont * _powerLabel;
    CCLabelBMFont * _shieldLabel;
    CCLabelBMFont * _pointsLabel;
}

- (void)setNukesString:(NSString *)string;

- (void)setPower:(NSString *)string;
- (void)setShield:(NSString *)string;
- (void)setPoints:(NSString *)string;
@end
