//
//  IntroNode.h
//  MiniSpaceFighter
//
//  Created by Gil Beyruth on 8/29/11.
//  Copyright 2011 Ray Wenderlich. All rights reserved.
//
//  DO NOT DISTRIBUTE THIS FILE WITHOUT PRIOR AUTHORIZATION


#import "cocos2d.h"


@interface IntroScene : CCLayer {
}

+(id) scene;

@end
