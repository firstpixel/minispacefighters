//
//  GlobalSingleton.m
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 9/23/09.
//  Copyright 2009 Firstpixel. All rights reserved.
//

#import "GlobalSingleton.h"

@implementation GlobalSingleton

static GlobalSingleton *_sharedInstance;

@synthesize bannerType,levelSelected,audioOn,myNukes,myPoints, gameType, shipType, deviceType, gameKitState, gameLevel, ship1, ship2, ship3, shipSelected, botSelected, bot11, bot12, bot13, bot14, bot21, bot22, bot23, bot24, bot31, bot32, bot33, bot34, powerDictionaryShip1, powerDictionaryShip2, powerDictionaryShip3, shieldDictionaryShip1, shieldDictionaryShip2, shieldDictionaryShip3,shipConfigSelected,isPlayingMenuMusic;

- (id) init {
	self = [super init];
	if (self != nil){
        audioOn = YES;
        isPlayingMenuMusic = NO;
        powerDictionaryShip1 = [[NSDictionary alloc] initWithObjectsAndKeys:@"25", @"0", @"50", @"1", @"75", @"2", @"100", @"3", @"150", @"4", @"200", @"5", nil];
        shieldDictionaryShip1 = [[NSDictionary alloc] initWithObjectsAndKeys:@"25", @"0", @"50", @"1", @"75", @"2", @"100", @"3", @"150", @"4", @"200", @"5", nil];
        powerDictionaryShip2 = [[NSDictionary alloc] initWithObjectsAndKeys:@"350", @"0", @"400", @"1", @"450", @"2", @"500", @"3", @"550", @"4", @"600", @"5", nil];
        shieldDictionaryShip2 = [[NSDictionary alloc] initWithObjectsAndKeys:@"350", @"0", @"400", @"1", @"450", @"2", @"500", @"3", @"550", @"4", @"600", @"5", nil];
        powerDictionaryShip3 = [[NSDictionary alloc] initWithObjectsAndKeys:@"600", @"0", @"650", @"1", @"700", @"2", @"800", @"3", @"900", @"4", @"1000", @"5", nil];
        shieldDictionaryShip3 = [[NSDictionary alloc] initWithObjectsAndKeys:@"600", @"0", @"650", @"1", @"700", @"2", @"800", @"3", @"900", @"4", @"1000", @"5", nil];
        
        //Get selected ship 
        if ([self getIntPrefForKey:@"myNukes"]!=0) {
            myNukes = [self getIntPrefForKey:@"myNukes"];
        }else{
            myNukes = 0;
        }
        
        //Get Points
        if ([self getIntPrefForKey:@"myPoints"]!=0) {
            myPoints = [self getIntPrefForKey:@"myPoints"];
        }else{
            myPoints = 0;
        }
        
        //Get selected ship 
        if ([self getIntPrefForKey:@"shipSelected"]!=0) {
            shipSelected = [self getIntPrefForKey:@"shipSelected"];
        }else{
            shipSelected = 1;
        }
        
        //Get selected ship 
        if ([self getIntPrefForKey:@"botSelected"]!=0) {
            botSelected = [self getIntPrefForKey:@"botSelected"];
        }else{
            botSelected = 1;
        }
        ship1 =  [[ShipConfig alloc] init];
        ship2 =  [[ShipConfig alloc] init];
        ship3 =  [[ShipConfig alloc] init];
        
        //Set ship1 state
        if ([self isItemUnlocked:@"shipUnlocked1"]==YES) {
            ship1 = [self getShipFromPref:1];
            NSLog(@"ship1 is not nil");
            NSLog(@"%@",[ship1 shipName]);
            NSLog(@"%@",[[_sharedInstance ship1] shipName]);
            NSLog(@"SHIP POWER LEVEL :%i",ship1.shipPowerLevel);
            NSLog(@"SHIP NAME :%@",ship1.shipName);
            NSLog(@"SHIP POWER :%i",ship1.power);
        }else{
            NSLog(@"ship1 is nil - setup a new one");
           [ship1  init:@"COLUMBIOS" withType:1  withValue:25000  withPower:25         withPowerLevel:0 withShield:25     
        withShieldLevel:0   withSlots:1     withBot1:0       withBot2:0      withBot3:0     withBot4:0
       withRepairValue1:1000 
       withRepairValue2:2000 
       withRepairValue3:3000 
       withRepairValue4:4000
       withRepairValue5:6000
       withRepairValue6:8000
            
      withUpgradeValue1:10000 
      withUpgradeValue2:20000 
      withUpgradeValue3:30000 
      withUpgradeValue4:50000
      withUpgradeValue5:70000
        withUpgradeValue6:0];
            NSLog(@"SHIP NAME :%@",[ship1 shipName]);
            
        }
        //Set ship2 state
        if ([self isItemUnlocked:@"shipUnlocked2"]==YES) {
            ship2 = [self getShipFromPref:2];
        }else{
            [ship2 init:@"STARFLEET1" withType:2 withValue:40000  withPower:350         withPowerLevel:0 withShield:350     
        withShieldLevel:0   withSlots:1     withBot1:0       withBot2:0      withBot3:0     withBot4:0
       withRepairValue1:2500 
       withRepairValue2:5000 
       withRepairValue3:7000 
       withRepairValue4:9000
       withRepairValue5:11000
       withRepairValue6:13000
             
      withUpgradeValue1:20000 
      withUpgradeValue2:40000 
      withUpgradeValue3:60000 
      withUpgradeValue4:80000
      withUpgradeValue5:100000
        withUpgradeValue6:0];
        }
        //Set ship3 state
        if ([self isItemUnlocked:@"shipUnlocked3"]==YES) {
            ship3 = [self getShipFromPref:3];
        }else{
            [ship3 release];
            ship3 =  [[ShipConfig alloc] init];
            [ship3 init:@"STARFLEET2" withType:3  withValue:90000  withPower:600         withPowerLevel:0 withShield:600      
        withShieldLevel:0   withSlots:1     withBot1:0       withBot2:0      withBot3:0     withBot4:0              
       withRepairValue1:4000 
       withRepairValue2:8000 
       withRepairValue3:10000 
       withRepairValue4:14000 
       withRepairValue5:16000 
       withRepairValue6:18000 
             
      withUpgradeValue1:40000 
      withUpgradeValue2:60000 
      withUpgradeValue3:80000 
      withUpgradeValue4:100000
      withUpgradeValue5:120000
      withUpgradeValue6:0];
        }
        
        bot11 =  [[BotConfig alloc] init];
        [bot11 initNewBot:@"REPAIR POWER" withType:BotRepairPower withValue:15000];
        
        bot12 =  [[BotConfig alloc] init];
        [bot12 initNewBot:@"REPAIR SHIELD" withType:BotRepairShield withValue:17000];
        
        bot13 =  [[BotConfig alloc] init];
        [bot13 initNewBot:@"LASER BOT" withType:BotLaser withValue:30000];
        
        bot14 =  [[BotConfig alloc] init];
        [bot14 initNewBot:@"ROCKET BOT" withType:BotRocket withValue:40000];
        
        bot21 =  [[BotConfig alloc] init];
        [bot21 initNewBot:@"REPAIR POWER" withType:BotRepairPower withValue:25000];
        
        bot22 =  [[BotConfig alloc] init];
        [bot22 initNewBot:@"REPAIR SHIELD" withType:BotRepairShield withValue:30000];
        
        bot23 =  [[BotConfig alloc] init];
        [bot23 initNewBot:@"LASER BOT" withType:BotLaser withValue:50000];
        
        bot24 =  [[BotConfig alloc] init];
        [bot24 initNewBot:@"ROCKET BOT" withType:BotRocket withValue:70000];
        
        bot31 =  [[BotConfig alloc] init];
        [bot31 initNewBot:@"REPAIR POWER" withType:BotRepairPower withValue:45000];
        
        bot32 =  [[BotConfig alloc] init];
        [bot32 initNewBot:@"REPAIR SHIELD" withType:BotRepairShield withValue:60000];
        
        bot33 =  [[BotConfig alloc] init];
        [bot33 initNewBot:@"LASER BOT" withType:BotLaser withValue:80000];
        
        bot34 =  [[BotConfig alloc] init];
        [bot34 initNewBot:@"ROCKET BOT" withType:BotRocket withValue:100000];
	}
	return self;
}


+ (GlobalSingleton *) sharedInstance
{
    
	if (!_sharedInstance)
	{
		_sharedInstance = [[GlobalSingleton alloc] init];
       // [_sharedInstance unlockItem:@"car0"];
    }
	return _sharedInstance;
    
}

-(NSString*)getOpenFeintLeaderboardFromSelected
{
    NSString* leaderboard;
    /*switch([self selectedTrackInt]){
        case 0:
            //usa
            leaderboard = [NSString stringWithString:@"865696"];
            break;
        case 1:
            //australia
            leaderboard = [NSString stringWithString:@"865656"];
            break;
    }*/
    return leaderboard;
}

-(void)setMyNukes:(int)_myNukes{
    myNukes = _myNukes;
    //[self intPref:myNukes forKey:@"myNukes"];
    
}

-(void)setMyPower:(int)_myPower{
    if(shipSelected ==1){
        powerShip1 = _myPower;
    }
    if(shipSelected ==2){
        powerShip2 = _myPower;
    }
    if(shipSelected ==3){
        powerShip3 = _myPower;
    }
    [self intPref:myNukes forKey:@"myNukes"];
}

-(void)setMyPoints:(int)_myPoints{
    myPoints = _myPoints;
    //[self intPref:myPoints forKey:@"myPoints"];
}

-(BOOL)saveRecordForLevel:(NSString*)level time:(int)time
{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if([prefs integerForKey:level] > time || [prefs integerForKey:level] == 0){
        if( time > 1000 ){
            [prefs setInteger:time forKey:level];
            CCLOG(@"***********************************************");
            CCLOG(@" RECORD SAVED FOR %@! YOUR TIME NOW IS: %i",[level uppercaseString], time);
            CCLOG(@"***********************************************");
            return YES;
        }else{
            return NO;
        }
    }else{
        return NO;
    }
}

-(int)getRecordForLevel:(NSString*)level {
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    return [prefs integerForKey:level];
    
}

-(void)unlockItem:(NSString*)item{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setBool:YES forKey:item];
    
}

-(void)lockItem:(NSString*)item{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setBool:NO forKey:item];
    
}

-(BOOL)isItemUnlocked:(NSString*)item{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    if([prefs boolForKey:item]){
        return YES;
    }else{
        return NO;
    }
    
}

//save int preferences
-(void)intPref:(int)integ forKey:(NSString*)key{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setInteger:integ forKey:key];
    
}

-(int)getIntPrefForKey:(NSString*)key{
    
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
   return [prefs integerForKey:key];
    
}

-(void)saveShip:(ShipConfig*)_ship forKey:(int)key{
   // CCLOG(@"SAVE SHIP shipName %@, power %i, shipPowerLevel %i,shield %i, shipShieldLevel %i",_ship.shipName,_ship.power, _ship.shipPowerLevel,_ship.shield, _ship.shipShieldLevel);
    
    [self objPref:_ship.shipName forKey:[NSString stringWithFormat:@"ship%i",key]];
    
    [self intPref:_ship.shipType forKey:[NSString stringWithFormat:@"shipType%i",key]];
    [self intPref:_ship.shipValue forKey:[NSString stringWithFormat:@"shipValue%i",key]];
    [self intPref:_ship.power forKey:[NSString stringWithFormat:@"power%i",key]];
    [self intPref:_ship.shipPowerLevel forKey:[NSString stringWithFormat:@"shipPowerLevel%i",key]];
    [self intPref:_ship.shield forKey:[NSString stringWithFormat:@"shield%i",key]];
    [self intPref:_ship.shipShieldLevel forKey:[NSString stringWithFormat:@"shipShieldLevel%i",key]];
    [self intPref:_ship.bot1 forKey:[NSString stringWithFormat:@"bot1%i",key]];
    [self intPref:_ship.bot2 forKey:[NSString stringWithFormat:@"bot2%i",key]];
    [self intPref:_ship.bot3 forKey:[NSString stringWithFormat:@"bot3%i",key]];
    [self intPref:_ship.bot4 forKey:[NSString stringWithFormat:@"bot4%i",key]];
    [self intPref:_ship.repairValue1 forKey:[NSString stringWithFormat:@"repairValue1%i",key]];
    [self intPref:_ship.repairValue2 forKey:[NSString stringWithFormat:@"repairValue2%i",key]];
    [self intPref:_ship.repairValue3 forKey:[NSString stringWithFormat:@"repairValue3%i",key]];
    [self intPref:_ship.repairValue4 forKey:[NSString stringWithFormat:@"repairValue4%i",key]];
    [self intPref:_ship.repairValue5 forKey:[NSString stringWithFormat:@"repairValue5%i",key]];
    [self intPref:_ship.repairValue6 forKey:[NSString stringWithFormat:@"repairValue6%i",key]];
    [self intPref:_ship.upgradeValue1 forKey:[NSString stringWithFormat:@"upgradeValue1%i",key]];
    [self intPref:_ship.upgradeValue2 forKey:[NSString stringWithFormat:@"upgradeValue2%i",key]];
    [self intPref:_ship.upgradeValue3 forKey:[NSString stringWithFormat:@"upgradeValue3%i",key]];
    [self intPref:_ship.upgradeValue4 forKey:[NSString stringWithFormat:@"upgradeValue4%i",key]];
    [self intPref:_ship.upgradeValue5 forKey:[NSString stringWithFormat:@"upgradeValue5%i",key]];
    [self intPref:_ship.upgradeValue6 forKey:[NSString stringWithFormat:@"upgradeValue6%i",key]];

    
}
-(ShipConfig*)getShipFromPref:(int)key{
    if( key == 1 ){
        ship1.shipName = [self getObjPrefForKey:[NSString stringWithFormat:@"ship%i",key]];
        ship1.shipType = [self getIntPrefForKey:[NSString stringWithFormat:@"shipType%i",key]];
        ship1.shipValue = [self getIntPrefForKey:[NSString stringWithFormat:@"shipValue%i",key]];
        ship1.power = [self getIntPrefForKey:[NSString stringWithFormat:@"power%i",key]];
        ship1.shipPowerLevel = [self getIntPrefForKey:[NSString stringWithFormat:@"shipPowerLevel%i",key]];
        ship1.shield = [self getIntPrefForKey:[NSString stringWithFormat:@"shield%i",key]];
        ship1.shipShieldLevel = [self getIntPrefForKey:[NSString stringWithFormat:@"shipShieldLevel%i",key]];
        ship1.bot1 = [self getIntPrefForKey:[NSString stringWithFormat:@"bot1%i",key]];
        ship1.bot2 = [self getIntPrefForKey:[NSString stringWithFormat:@"bot2%i",key]];
        ship1.bot3 = [self getIntPrefForKey:[NSString stringWithFormat:@"bot3%i",key]];
        ship1.bot4 = [self getIntPrefForKey:[NSString stringWithFormat:@"bot4%i",key]];
        ship1.repairValue1 = [self getIntPrefForKey:[NSString stringWithFormat:@"repairValue1%i",key]];
        ship1.repairValue2 = [self getIntPrefForKey:[NSString stringWithFormat:@"repairValue2%i",key]];
        ship1.repairValue3 = [self getIntPrefForKey:[NSString stringWithFormat:@"repairValue3%i",key]];
        ship1.repairValue4 = [self getIntPrefForKey:[NSString stringWithFormat:@"repairValue4%i",key]];
        ship1.repairValue5 = [self getIntPrefForKey:[NSString stringWithFormat:@"repairValue5%i",key]];
        ship1.repairValue6 = [self getIntPrefForKey:[NSString stringWithFormat:@"repairValue6%i",key]];
        ship1.upgradeValue1 = [self getIntPrefForKey:[NSString stringWithFormat:@"upgradeValue1%i",key]];
        ship1.upgradeValue2 = [self getIntPrefForKey:[NSString stringWithFormat:@"upgradeValue2%i",key]];
        ship1.upgradeValue3 = [self getIntPrefForKey:[NSString stringWithFormat:@"upgradeValue3%i",key]];
        ship1.upgradeValue4 = [self getIntPrefForKey:[NSString stringWithFormat:@"upgradeValue4%i",key]];
        ship1.upgradeValue5 = [self getIntPrefForKey:[NSString stringWithFormat:@"upgradeValue5%i",key]];
        ship1.upgradeValue6 = [self getIntPrefForKey:[NSString stringWithFormat:@"upgradeValue6%i",key]];
        return ship1;
    }
    if( key == 2 ){
        ship2.shipName = [self getObjPrefForKey:[NSString stringWithFormat:@"ship%i",key]];
        ship2.shipType = [self getIntPrefForKey:[NSString stringWithFormat:@"shipType%i",key]];
        ship2.shipValue = [self getIntPrefForKey:[NSString stringWithFormat:@"shipValue%i",key]];
        ship2.power = [self getIntPrefForKey:[NSString stringWithFormat:@"power%i",key]];
        ship2.shipPowerLevel = [self getIntPrefForKey:[NSString stringWithFormat:@"shipPowerLevel%i",key]];
        ship2.shield = [self getIntPrefForKey:[NSString stringWithFormat:@"shield%i",key]];
        ship2.shipShieldLevel = [self getIntPrefForKey:[NSString stringWithFormat:@"shipShieldLevel%i",key]];
        ship2.bot1 = [self getIntPrefForKey:[NSString stringWithFormat:@"bot1%i",key]];
        ship2.bot2 = [self getIntPrefForKey:[NSString stringWithFormat:@"bot2%i",key]];
        ship2.bot3 = [self getIntPrefForKey:[NSString stringWithFormat:@"bot3%i",key]];
        ship2.bot4 = [self getIntPrefForKey:[NSString stringWithFormat:@"bot4%i",key]];
        ship2.repairValue1 = [self getIntPrefForKey:[NSString stringWithFormat:@"repairValue1%i",key]];
        ship2.repairValue2 = [self getIntPrefForKey:[NSString stringWithFormat:@"repairValue2%i",key]];
        ship2.repairValue3 = [self getIntPrefForKey:[NSString stringWithFormat:@"repairValue3%i",key]];
        ship2.repairValue4 = [self getIntPrefForKey:[NSString stringWithFormat:@"repairValue4%i",key]];
        ship2.repairValue5 = [self getIntPrefForKey:[NSString stringWithFormat:@"repairValue5%i",key]];
        ship2.repairValue6 = [self getIntPrefForKey:[NSString stringWithFormat:@"repairValue6%i",key]];
        ship2.upgradeValue1 = [self getIntPrefForKey:[NSString stringWithFormat:@"upgradeValue1%i",key]];
        ship2.upgradeValue2 = [self getIntPrefForKey:[NSString stringWithFormat:@"upgradeValue2%i",key]];
        ship2.upgradeValue3 = [self getIntPrefForKey:[NSString stringWithFormat:@"upgradeValue3%i",key]];
        ship2.upgradeValue4 = [self getIntPrefForKey:[NSString stringWithFormat:@"upgradeValue4%i",key]];
        ship2.upgradeValue5 = [self getIntPrefForKey:[NSString stringWithFormat:@"upgradeValue5%i",key]];
        ship2.upgradeValue6 = [self getIntPrefForKey:[NSString stringWithFormat:@"upgradeValue6%i",key]];
        return ship2;
    }
    if( key == 3 ){
        ship3.shipName = [self getObjPrefForKey:[NSString stringWithFormat:@"ship%i",key]];
        ship3.shipType = [self getIntPrefForKey:[NSString stringWithFormat:@"shipType%i",key]];
        ship3.shipValue = [self getIntPrefForKey:[NSString stringWithFormat:@"shipValue%i",key]];
        ship3.power = [self getIntPrefForKey:[NSString stringWithFormat:@"power%i",key]];
        ship3.shipPowerLevel = [self getIntPrefForKey:[NSString stringWithFormat:@"shipPowerLevel%i",key]];
        ship3.shield = [self getIntPrefForKey:[NSString stringWithFormat:@"shield%i",key]];
        ship3.shipShieldLevel = [self getIntPrefForKey:[NSString stringWithFormat:@"shipShieldLevel%i",key]];
        ship3.bot1 = [self getIntPrefForKey:[NSString stringWithFormat:@"bot1%i",key]];
        ship3.bot2 = [self getIntPrefForKey:[NSString stringWithFormat:@"bot2%i",key]];
        ship3.bot3 = [self getIntPrefForKey:[NSString stringWithFormat:@"bot3%i",key]];
        ship3.bot4 = [self getIntPrefForKey:[NSString stringWithFormat:@"bot4%i",key]];
        ship3.repairValue1 = [self getIntPrefForKey:[NSString stringWithFormat:@"repairValue1%i",key]];
        ship3.repairValue2 = [self getIntPrefForKey:[NSString stringWithFormat:@"repairValue2%i",key]];
        ship3.repairValue3 = [self getIntPrefForKey:[NSString stringWithFormat:@"repairValue3%i",key]];
        ship3.repairValue4 = [self getIntPrefForKey:[NSString stringWithFormat:@"repairValue4%i",key]];
        ship3.repairValue5 = [self getIntPrefForKey:[NSString stringWithFormat:@"repairValue5%i",key]];
        ship3.repairValue6 = [self getIntPrefForKey:[NSString stringWithFormat:@"repairValue6%i",key]];
        ship3.upgradeValue1 = [self getIntPrefForKey:[NSString stringWithFormat:@"upgradeValue1%i",key]];
        ship3.upgradeValue2 = [self getIntPrefForKey:[NSString stringWithFormat:@"upgradeValue2%i",key]];
        ship3.upgradeValue3 = [self getIntPrefForKey:[NSString stringWithFormat:@"upgradeValue3%i",key]];
        ship3.upgradeValue4 = [self getIntPrefForKey:[NSString stringWithFormat:@"upgradeValue4%i",key]];
        ship3.upgradeValue5 = [self getIntPrefForKey:[NSString stringWithFormat:@"upgradeValue5%i",key]];
        ship3.upgradeValue6 = [self getIntPrefForKey:[NSString stringWithFormat:@"upgradeValue6%i",key]];
        return ship3;
    }
    return nil;
}

// save object
-(void)objPref:(id)obj forKey:(NSString*)key{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:obj forKey:key];
    [prefs synchronize]; 
}

-(id)getObjPrefForKey:(NSString*)key{
    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    NSData *obj = [prefs objectForKey:key];
    return obj;
}

-(NSString*)timeToString:(int)t{
    int minutes = t / 60000;
    int seconds = t % 60000 / 1000;
    int milliseconds = t % 1000;
    NSString* minutesString;
    NSString* secondsString;
    NSString* millisecondsString;
    
   // if(minutes<=9){
   //     minutesString = [NSString stringWithFormat:@"0%d",minutes];
   // }else{
        minutesString = [NSString stringWithFormat:@"%d",minutes];
    //}
    if(seconds<=9){
        secondsString = [NSString stringWithFormat:@"0%d",seconds];
    }else{
        secondsString = [NSString stringWithFormat:@"%d",seconds];
    }
    if(milliseconds<=9){
        millisecondsString = [NSString stringWithFormat:@"00%d",milliseconds];
    }else if(milliseconds<=99){
        millisecondsString = [NSString stringWithFormat:@"0%d",milliseconds];
    }else{
        millisecondsString = [NSString stringWithFormat:@"%d",milliseconds];
    }
    // NSLog(@"TIMER TIME - %@ : %@ : %@ ",minutesString, secondsString, millisecondsString);
    return [NSString stringWithFormat:@"%@ : %@ : %@ ",minutesString, secondsString, millisecondsString];
    
}

-(void) clearData {
	
	audioOn = YES;
    
}

- (id)retain

{
	
    return self;
	
}

- (unsigned)retainCount

{
	
    return UINT_MAX;  //denotes an object that cannot be released
	
}

- (void)release

{
    [ship1 release];
    [ship1 dealloc];
    [ship2 release];
    [ship2 dealloc];
    [ship3 release];
    [ship3 dealloc];
    [shieldDictionaryShip1 release];
    [powerDictionaryShip1 release];
    
    [shieldDictionaryShip2 release];
    [powerDictionaryShip2 release];
    
    [shieldDictionaryShip3 release];
    [powerDictionaryShip3 release];
    //do nothing
}

- (id)autorelease

{
	
    return self;
	
}

@end





