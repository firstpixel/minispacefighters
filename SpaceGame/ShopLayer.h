//
//  MenuLayer.h
//  MiniSpaceFighters
//
//  Created by Gil Beyruth on 8/29/11.
//  Copyright 2011 Ray Wenderlich. All rights reserved.
//

#import "cocos2d.h"
#import "GlobalSingleton.h"
@interface ShopLayer : CCLayer {
    CCSpriteBatchNode *_batchNode;
    CCSpriteBatchNode *_batchNode2;
    CCSpriteBatchNode *_batchNode3;
    CCSpriteBatchNode *_batchNode4;
    CCSpriteBatchNode *_batchNodeFighter;
    CCSpriteBatchNode *_batchNodeBot;
    
    CCAnimation *animationFighterRotation;
    CCAnimation *animationBotRotation;
    BotType botType;
    NSString* botName;

    BotConfig *bot;
    ShipConfig* ship;

    
    CCLabelBMFont* myNukesLabel;
    CCLabelBMFont* myPointsLabel;
    
    NSDictionary *dictionaryPowerShip;
    NSDictionary *dictionaryShieldShip;
}

+(id)scene;
- (void)setupBatchNode;
-(void)setupBatchNodeFighter;

-(void)setupBatchNodeBot;

-(void)subtractMyNukes:(int)value;
-(void)addMyNukes:(int)value;

@end;