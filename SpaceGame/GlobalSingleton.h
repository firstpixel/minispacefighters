//
//  GlobalSingleton.h
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 9/23/09.
//  Copyright 2009 Firstpixel. All rights reserved.
//




#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "ShipConfig.h"
#import "BotConfig.h"

typedef enum {
    BotRepairPower = 1,
    BotRepairShield,
    BotLaser,
    BotRocket
} BotType;

@interface GlobalSingleton : NSObject {
    
    NSString* levelSelected;
    BOOL isPlayingMenuMusic;
    ShipConfig *ship1;
    ShipConfig *ship2;
    ShipConfig *ship3;
    
    ShipConfig *shipConfigSelected;
    
    
    BotConfig *bot11;
    BotConfig *bot12;
    BotConfig *bot13;
    BotConfig *bot14;
    
    BotConfig *bot21;
    BotConfig *bot22;
    BotConfig *bot23;
    BotConfig *bot24;
    
    BotConfig *bot31;
    BotConfig *bot32;
    BotConfig *bot33;
    BotConfig *bot34;
    
    int shipSelected,botSelected;
    
    int myNukes,myPoints;
	int gameType;
	int gameKitState;
	int shipType;
    BOOL audioOn;
    
       int bannerType;
    
    NSString* deviceType;
    int gameLevel,
    shieldLevelShip1, powerLevelShip1, shieldShip1, powerShip1, bot1Ship1, bot2Shit1,
    shieldLevelShip2, powerLevelShip2, shieldShip2, powerShip2, bot1Ship2, bot2Shit2, bot3Shit2, 
    shieldLevelShip3, powerLevelShip3, shieldShip3, powerShip3, bot1Ship3, bot2Shit3, bot3Shit3, bot4Shit3;
    
    NSMutableDictionary *shipDictionary, * shieldDictionaryShip1, * powerDictionaryShip1, * shieldDictionaryShip2, * powerDictionaryShip2, * shieldDictionaryShip3, * powerDictionaryShip3;
    
}
@property (nonatomic, readwrite) int bannerType;
@property (nonatomic, retain) ShipConfig *shipConfigSelected;
@property (nonatomic, retain) NSString *deviceType,*levelSelected;
@property (nonatomic, readwrite) int gameType,gameKitState,shipType,gameLevel,shipSelected,botSelected, myNukes,myPoints;
@property (nonatomic, readwrite) BOOL audioOn,isPlayingMenuMusic;
@property (nonatomic, retain) ShipConfig *ship1, *ship2, *ship3;
@property (nonatomic, retain) BotConfig *bot11, *bot12, *bot13, *bot14, *bot21, *bot22, *bot23, *bot24, *bot31, *bot32, *bot33, *bot34;
@property (nonatomic, retain) NSDictionary * shieldDictionaryShip1, *shieldDictionaryShip2, *shieldDictionaryShip3, *powerDictionaryShip1, *powerDictionaryShip2, *powerDictionaryShip3;

+ (GlobalSingleton *) sharedInstance;

//record

-(void)unlockItem:(NSString*)item;
-(void)lockItem:(NSString*)item;
-(BOOL)isItemUnlocked:(NSString*)item;
-(ShipConfig*)getShipFromPref:(int)key;
-(void)saveShip:(ShipConfig*)_ship forKey:(int)key;

-(void) clearData;


-(BOOL)saveRecordForLevel:(NSString*)level time:(int)time;

//
-(int)getRecordForLevel:(NSString*)level;
-(NSString*)timeToString:(int)t;

//
-(void)intPref:(int)integ forKey:(NSString*)key;
-(int)getIntPrefForKey:(NSString*)key;

-(void)objPref:(id)obj forKey:(NSString*)key;
-(id)getObjPrefForKey:(NSString*)key;

-(NSString*)getOpenFeintLeaderboardFromSelected;

@end