//
//  GameBuyNowScene.m
//  SoccerVirtualCup
//
//  Created by Gil Beyruth on 9/20/09.
//  Copyright Firstpixel 2009. All rights reserved.
//

#import "GameBuyNowScene.h"
#import "ShoppingCartScene.h"

#import "SoundMenuItem.h"
#import "AppDelegate.h"

@implementation GameBuyNowScene

+(id) scene {
	CCScene *s = [CCScene node];
	id node = [GameBuyNowScene node];
	[s addChild:node];
	return s;
}

- (id) init {
    self = [super init];
    if (self != nil) {
        CGSize s = [[CCDirector sharedDirector] winSize];
        
        SoundMenuItem *selectButton = nil;
		
        if([(AppDelegate*)[[UIApplication sharedApplication] delegate] movieIsPlaying])[(AppDelegate*)[[UIApplication sharedApplication] delegate] movieStop];
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            selectButton = [SoundMenuItem itemFromNormalSprite:[CCSprite spriteWithFile:@"certificate.png"] selectedSprite:[CCSprite spriteWithFile:@"certificate.png"] target:self selector:@selector(startShopping:)];
        }else{
            selectButton = [SoundMenuItem itemFromNormalSprite:[CCSprite spriteWithFile:@"certificate.png"] selectedSprite:[CCSprite spriteWithFile:@"certificate.png"] target:self selector:@selector(startShopping:)];
        }
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"hud.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"Sprites.plist"];

        scaleFactor = 1;
        px = 0.0;
        py = 0.0;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            scaleFactor = 2;
            px = 32.0f;
            py = 64.0f;
        }
        
        CCLabelBMFont*text = [CCLabelBMFont labelWithString:@"Welcome Head Hunter, to help the Intergalactic Confederation fighting on Saturn, Jupiter, Mars, Earth, Venus, Mercury and Sun you must have the Certification(Full Version). Please get certified by the Intergalatic Confederation, and help us clean the Solar System. Tap on the screen to proceed." fntFile:@"Arial-gray.fnt" width:(300.0*scaleFactor) alignment:CCTextAlignmentLeft];
        
        [selectButton addChild:text];
        text.position = ccp((selectButton.contentSize.width/2+40*scaleFactor)+px,(selectButton.contentSize.height/3*2-(60*scaleFactor))+py);
        
        
        CCLabelBMFont*text2 = [CCLabelBMFont labelWithString:@"GET CERTIFIED! \n \nBUY THE FULL VERSION AND BECOME \n \nA INTERGALACTIC CONFEDERATION RANGER! \n \nNO MORE BANNERS!" fntFile:@"XitRam.fnt" width:(300.0*scaleFactor) alignment:CCTextAlignmentLeft];
        
        [selectButton addChild:text2];
        text2.position = ccp((selectButton.contentSize.width/2+40*scaleFactor)+px,(selectButton.contentSize.height/2-(80*scaleFactor))+py);
        
        
        CCMenu *menu = [CCMenu menuWithItems:selectButton, nil];
		menu.position = ccp(s.width/2,s.height/2);
		[self addChild: menu z:0];
    }
    return self;
}

-(void)dealloc{
    [[CCTextureCache sharedTextureCache] removeUnusedTextures];
    [super dealloc];
}

-(void)startShopping:(id)sender
{
     [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:0.5f scene:[ShoppingCartScene scene]]];
}

@end
