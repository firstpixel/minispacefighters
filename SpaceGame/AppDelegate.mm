//
//  AppDelegate.m
//  SpaceGame
//
//  Created by Ray Wenderlich on 6/24/11.
//  Copyright Ray Wenderlich 2011. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// DO NOT DISTRIBUTE WITHOUT PRIOR AUTHORIZATION (SEE LICENSE.TXT)
//


#import "AppDelegate.h"
#import "GameConfig.h"
//#import "HelloWorldLayer.h"
#import "ActionLayer.h"

#import "IntroScene.h"
#import "MenuLayer.h"
#import "RootViewController.h"
#import "MKStoreManager.h"
#import "GlobalSingleton.h"

@implementation AppDelegate

@synthesize window, moviePlaying;

- (void) removeStartupFlicker
{
	//
	// THIS CODE REMOVES THE STARTUP FLICKER
	//
	// Uncomment the following code if you Application only supports landscape mode
	//
#if GAME_AUTOROTATION == kGameAutorotationUIViewController
	
	//	CC_ENABLE_DEFAULT_GL_STATES();
	//	CCDirector *director = [CCDirector sharedDirector];
	//	CGSize size = [director winSize];
	//	CCSprite *sprite = [CCSprite spriteWithFile:@"Default.png"];
	//	sprite.position = ccp(size.width/2, size.height/2);
	//	sprite.rotation = -90;
	//	[sprite visit];
	//	[[director openGLView] swapBuffers];
	//	CC_ENABLE_DEFAULT_GL_STATES();
	
#endif // GAME_AUTOROTATION == kGameAutorotationUIViewController	
}

- (void) applicationDidFinishLaunching:(UIApplication*)application
{
    [[GlobalSingleton sharedInstance] setMyNukes:[[GlobalSingleton sharedInstance] getIntPrefForKey:@"myNukes"]];
    [[GlobalSingleton sharedInstance] setMyPoints:[[GlobalSingleton sharedInstance] getIntPrefForKey:@"myPoints"]];
    
    [MKStoreManager sharedManager];
    
    
    /*
     NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
     [userDefaults setBool:YES forKey:@"com.firstpixel.minispacefighters.full1"];
     
     */
   
    // Main Window
    window_ = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Director
    director_ = (CCDirectorIOS*)[CCDirector sharedDirector];
    [director_ setDisplayStats:NO];
    [director_ setAnimationInterval:1.0/60];
    
    // GL View
    CCGLView *__glView = [CCGLView viewWithFrame:[window_ bounds]
                                     pixelFormat:kEAGLColorFormatRGB565
                                     depthFormat:0 /* GL_DEPTH_COMPONENT24_OES */
                              preserveBackbuffer:NO
                                      sharegroup:nil
                                   multiSampling:NO
                                 numberOfSamples:0
                          ];
    
    __glView.multipleTouchEnabled = YES;
    [director_ setView:__glView];
    [director_ setDelegate:self];
    director_.wantsFullScreenLayout = YES;
    
    // 2D projection
    [director_ setProjection:kCCDirectorProjection2D];
    //	[director setProjection:kCCDirectorProjection3D];
    
    // Enables High Res mode (Retina Display) on iPhone 4 and maintains low res on all other devices
    if( ! [director_ enableRetinaDisplay:YES] )
        CCLOG(@"Retina Display Not supported");
    
    // Default texture format for PNG/BMP/TIFF/JPEG/GIF images
    // It can be RGBA8888, RGBA4444, RGB5_A1, RGB565
    // You can change anytime.
    [CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGBA8888];
    
    // Assume that PVR images have premultiplied alpha
    [CCTexture2D PVRImagesHavePremultipliedAlpha:YES];
    
    CCFileUtils *sharedFileUtils = [CCFileUtils sharedFileUtils];
    [sharedFileUtils setEnableFallbackSuffixes:NO];				// Default: NO. No fallback suffixes are going to be used
    [sharedFileUtils setiPhoneRetinaDisplaySuffix:@"-hd"];		// Default on iPhone RetinaDisplay is "-hd"
    [sharedFileUtils setiPadSuffix:@"-hd"];					// Default on iPad is "ipad"
    [sharedFileUtils setiPadRetinaDisplaySuffix:@"-ipadhd"];	// Default on iPad RetinaDisplay is "-ipadhd"
    
    
    
    // Navigation Controller
    navController_ = [[RootViewController alloc] initWithRootViewController:director_];
    navController_.navigationBarHidden = YES;
    
    // AddSubView doesn't work on iOS6
    //	[window_ addSubview:navController_.view];
    [window_ setRootViewController:navController_];
    
    [window_ makeKeyAndVisible];
	


    //BANNER
    if(![MKStoreManager isFeaturePurchased:kFeatureAId]){
        [(RootViewController*)[self navController] addBanner];
    }
    
		
	// Removes the startup flicker
	[self removeStartupFlicker];
	
	// Run the menu Scene
	//[[CCDirector sharedDirector] runWithScene:[MenuLayer scene]];
    // Run the intro Scene
	//CGRect win = [[UIScreen mainScreen] bounds];
   // CGSize winSize = [[CCDirector sharedDirector] winSize];
 //   [[CCDirector sharedDirector] runWithScene: [MenuLayer scene]];
   [[CCDirector sharedDirector] runWithScene: [IntroScene scene]];
    
   // [self startMovie];
}

-(void)startMovie:(NSString*)video ofType:(NSString*)ext
{
    
    if(![[[GlobalSingleton sharedInstance] deviceType] isEqualToString:@"iPod Touch 1G"]){
    
        NSLog(@"Start Playing Video %@.%@",video,ext);
        
        moviePlaying = [NSString stringWithString:video];
        
        [self movieStop];
            
        NSURL *url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:video ofType:ext]];
        moviePlayer = [[MPMoviePlayerController alloc] initWithContentURL:url];
        //moviePlayer.useApplicationAudioSession = YES;
        
        
            
        if ([moviePlayer respondsToSelector:@selector(setFullscreen:animated:)]) {
            NSLog(@"Start Playing Video %@.%@",video,ext);
            // Use the new 3.2 style API9
            if ([moviePlayer respondsToSelector:@selector(setControlStyle:)])
            {
                [moviePlayer setControlStyle: MPMovieControlStyleNone ];
            }
            
            moviePlayer.shouldAutoplay = YES;
            moviePlayer.repeatMode = MPMovieRepeatModeOne;
            CGRect win = [[UIScreen mainScreen] bounds];
            
             moviePlayer.view.frame = CGRectMake(0, 0, win.size.height, win.size.width);
            [_navController.view  addSubview:moviePlayer.view];
            [_navController.view  sendSubviewToBack:moviePlayer.view];
            
            // Register for the playback finished notification.
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(movieFinishedCallback:)
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:moviePlayer];
            
            if ([moviePlayer respondsToSelector:@selector(prepareToPlay)]) {
                [[NSNotificationCenter defaultCenter] addObserver:self
                                                         selector:@selector(preparedToPlayerCallback:)
                                                             name:MPMediaPlaybackIsPreparedToPlayDidChangeNotification
                                                           object:moviePlayer];
                
                [moviePlayer prepareToPlay];
            }
        } 
        else
        {
            // Use the old 2.0 style API
            [moviePlayer setControlStyle:MPMovieControlStyleNone];
            [moviePlayer play];

        }
    }
}
-(BOOL)movieIsPlaying:(NSString*)movie{
    if(![[[GlobalSingleton sharedInstance] deviceType] isEqualToString:@"iPod Touch 1G"]){
        
        if ([moviePlayer respondsToSelector:@selector(playbackState)]) {
            if([moviePlayer playbackState]==MPMoviePlaybackStatePlaying){
                if([moviePlaying isEqualToString:movie]){
                    return YES;
                }else{
                    return NO;
                }
            }else{
                return NO;
            }
        }else{
            return NO;
        }
    }
    return NO;
}

-(BOOL)movieIsPlaying {
    if(![[[GlobalSingleton sharedInstance] deviceType] isEqualToString:@"iPod Touch 1G"]){
        
        if ([moviePlayer respondsToSelector:@selector(playbackState)]) {
            if([moviePlayer playbackState]==MPMoviePlaybackStatePlaying){
                return YES;
            }else{
                return NO;
            }
        }else{
            return NO;
        }
    }
    return NO;
}

-(void)movieFinishedCallback:(NSNotification*)aNotification
{
    NSLog(@"Finished Playing Video %@",aNotification);
    
    //MPMoviePlayerController* theMovie = [aNotification object];
    //add code here
}
-(void)preparedToPlayerCallback:(NSNotification*)aNotification
{
    NSLog(@"preparedToPlayerCallback Playing Video %@",aNotification);
    if(![[[GlobalSingleton sharedInstance] deviceType] isEqualToString:@"iPod Touch 1G"]){
        
    MPMoviePlayerController* theMovie = [aNotification object];
    if (theMovie.isPreparedToPlay) {
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                        name:MPMediaPlaybackIsPreparedToPlayDidChangeNotification
                                                      object:theMovie];
        //add code here 
    }
    }
}

-(void)movieStop {
    if(![[[GlobalSingleton sharedInstance] deviceType] isEqualToString:@"iPod Touch 1G"]){
        
        [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:MPMoviePlayerPlaybackDidFinishNotification
                                                  object:moviePlayer];
        if(moviePlayer!=nil){
            [moviePlayer stop];
            if ([moviePlayer respondsToSelector:@selector(view)]) {
                [moviePlayer.view removeFromSuperview];
            }
            [moviePlayer release];
            moviePlayer= nil;
        }
    }
}
- (void)applicationWillResignActive:(UIApplication *)application {
    
    [[GlobalSingleton sharedInstance] intPref:[[GlobalSingleton sharedInstance] myNukes] forKey:@"myNukes"];
    [[GlobalSingleton sharedInstance] intPref:[[GlobalSingleton sharedInstance] myPoints] forKey:@"myPoints"];
	[[CCDirector sharedDirector] pause];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
    [[GlobalSingleton sharedInstance] setMyNukes:[[GlobalSingleton sharedInstance] getIntPrefForKey:@"myNukes"]];
    [[GlobalSingleton sharedInstance] setMyPoints:[[GlobalSingleton sharedInstance] getIntPrefForKey:@"myPoints"]];
    [[CCDirector sharedDirector] resume];
}

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    [[CCDirector sharedDirector] purgeCachedData];
}

-(void) applicationDidEnterBackground:(UIApplication*)application {
    
    [[GlobalSingleton sharedInstance] intPref:[[GlobalSingleton sharedInstance] myNukes] forKey:@"myNukes"];
    [[GlobalSingleton sharedInstance] intPref:[[GlobalSingleton sharedInstance] myPoints] forKey:@"myPoints"];
	[[CCDirector sharedDirector] stopAnimation];
}

-(void) applicationWillEnterForeground:(UIApplication*)application {
	[[GlobalSingleton sharedInstance] setMyNukes:[[GlobalSingleton sharedInstance] getIntPrefForKey:@"myNukes"]];
    [[GlobalSingleton sharedInstance] setMyPoints:[[GlobalSingleton sharedInstance] getIntPrefForKey:@"myPoints"]];
    [[CCDirector sharedDirector] startAnimation];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    
    [[GlobalSingleton sharedInstance] intPref:[[GlobalSingleton sharedInstance] myNukes] forKey:@"myNukes"];
    [[GlobalSingleton sharedInstance] intPref:[[GlobalSingleton sharedInstance] myPoints] forKey:@"myPoints"];
	CCDirector *director = [CCDirector sharedDirector];
	[[director openGLView] removeFromSuperview];
	[_navController release];
	[window release];
	[director end];	
}

- (void)applicationSignificantTimeChange:(UIApplication *)application {
	[[CCDirector sharedDirector] setNextDeltaTimeZero:YES];
}

- (void)dealloc {
	[[CCDirector sharedDirector] end];
	[window release];
	[super dealloc];
}

@end
