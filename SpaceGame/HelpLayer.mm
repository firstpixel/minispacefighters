//
//  MenuLayer.cpp
//  MiniSpaceFighters
//
//  Created by Gil Beyruth on 8/29/11.
//  Copyright 2011 Ray Wenderlich. All rights reserved.
//

#import "HelpLayer.h"
#import "SimpleAudioEngine.h"
#import "GlobalSingleton.h"
#import "SoundMenuItem.h"
#import "AppDelegate.h"
#import "ShopLayer.h"
#import "MenuLayer.h"
@implementation HelpLayer

+(id)scene {
    CCScene *scene = [CCScene node];
    HelpLayer *layer = [HelpLayer node];
    [scene addChild:layer];
    return scene;
}

-(id)init
{
    if( (self=[super init]) )
	{
        //GlobalSingleton*mySingleton = [GlobalSingleton sharedInstance];
        if ([SimpleAudioEngine sharedEngine] != nil) {
            [[GlobalSingleton sharedInstance] setIsPlayingMenuMusic:NO];
            [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
        } 
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            [(AppDelegate*)[[UIApplication sharedApplication] delegate] startMovie:@"intro" ofType:@"m4v"];
        } else {
            [(AppDelegate*)[[UIApplication sharedApplication] delegate] startMovie:@"intro" ofType:@"m4v"];
        }
        scaleFactor = 1;
        px = 0.0;
        py = 0.0;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            scaleFactor = 2;
            px = 32.0f;
            py = 64.0f;
        }
        //Menu back
        SoundMenuItem *back = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt-back.png" selectedSpriteFrameName:@"bt-back-over.png" target:self selector:@selector(goBack)];
        CCMenu *menuBack = [CCMenu menuWithItems: back, nil];
        [menuBack alignItemsVerticallyWithPadding:12.0];
        [menuBack setPosition:ccp(( 22*scaleFactor )+px,( 270*scaleFactor)+py )];
        [self addChild:menuBack z:2];
        [self schedule:@selector(goShop) interval:10];
    }
    return self;
}

-(void)goBack{
    [self unschedule: @selector(goShop)];
    
    [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[MenuLayer scene]]];
}

-(void)goShop{
    GlobalSingleton * mySingleton = [GlobalSingleton sharedInstance];
    [mySingleton unlockItem:@"GameIntro"];
    [self unschedule: @selector(goShop)];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ShopLayer scene]]];
}
@end;