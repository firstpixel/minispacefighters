//
//  DeviceSettings.h
//  MiniSpaceFighters
//
//  Created by Gil Beyruth on 12/15/11.
//  Copyright (c) 2011 com.firstpixel. All rights reserved.
//

#ifndef MiniSpaceFighters_DeviceSettings_h
#define MiniSpaceFighters_DeviceSettings_h

#import <UIKit/UIDevice.h>

/*  DETERMINE THE DEVICE USED  */

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad && [[UIScreen mainScreen] scale] == 1 )
#define IS_IPADHD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad && [[UIScreen mainScreen] scale] > 1 )

#define SIZE_HEIGHT (  fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )320 ) )

#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] == 2.0)
#define IS_RETINA3X ([[UIScreen mainScreen] scale] > 2.0)

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPHONE4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < 568.0)
#define IS_IPHONE5 (IS_IPHONE && SCREEN_MAX_LENGTH == 568.0)
#define IS_IPHONE6 (IS_IPHONE && SCREEN_MAX_LENGTH == 667.0)
#define IS_IPHONE6P (IS_IPHONE && SCREEN_MAX_LENGTH == 736.0)
//#define IS_IPADHD (IS_IPAD && SCREEN_MAX_LENGTH == 1024.0)

#define realHeight() ([ [ UIScreen mainScreen ] bounds ].size.height);
#define realWidth() ([ [ UIScreen mainScreen ] bounds ].size.width);

/*  NORMAL DETAILS */
#define kScreenHeight       480
#define kScreenWidth        320

/* OFFSETS TO ACCOMMODATE IPAD */

#define kXoffsetiPad        32
#define kYoffsetiPad        64


#define kXoffsetiPhone5     44
#define kYoffsetiPhone5     0 //32

#define kXoffsetiPhone6     96
#define kYoffsetiPhone6     28

#define kXoffsetiPhone6p     128
#define kYoffsetiPhone6p     47

#define kXoffsetiPadhd     32
#define kYoffsetiPadhd     64

#define SD_PNG      @".png"
#define HD_PNG      @"-hd.png"

#define ADJUST_CCP(__p__)       \
(IS_IPAD==YES?     ccp( (  __p__.x * 2 )        +   kXoffsetiPad,      (   __p__.y * 2 )    +    kYoffsetiPad ) : \
IS_IPHONE5        ?     ccp(    __p__.x         +   kXoffsetiPhone5,       __p__.y          +    kYoffsetiPhone5 ): \
IS_IPHONE6        ?     ccp(    __p__.x         +   kXoffsetiPhone6,       __p__.y          +    kYoffsetiPhone6 ): \
IS_IPHONE6P       ?     ccp(    __p__.x         +   kXoffsetiPhone6p,      __p__.y          +    kYoffsetiPhone6p ): \
IS_IPADHD         ?     ccp(  ( __p__.x * 2 )   +   kXoffsetiPadhd,     ( __p__.y * 2 )     +    kYoffsetiPadhd ): \
__p__)

#define ADJUST_INTERNAL_SPRITE_CCP(__p__)       \
(IS_IPAD || IS_IPADHD ?             \
ccp( ( __p__.x * 2 ), ( __p__.y * 2 )  ) : \
__p__)

#define ADJUST_CGSIZE(__p__)       \
(IS_IPAD || IS_IPADHD ?             \
CGSizeMake( ( __p__.width * 2 ) , ( __p__.height * 2 )  ) : \
__p__)

#define REVERSE_CCP(__p__)      \
(IS_IPAD || IS_IPADHD ?             \
ccp( ( __p__.x - kXoffsetiPad ) / 2, ( __p__.y - kYoffsetiPad ) / 2 ) : \
__p__)

#define ADJUST_XY(__x__, __y__)     \
(IS_IPAD || IS_IPADHD? ccp( ( __x__ * 2 ) + kXoffsetiPad, ( __y__ * 2 ) + kYoffsetiPad ) : \
IS_IPHONE5?ccp(__x__+ kXoffsetiPhone5, __y__ ): \
IS_IPHONE6?ccp(__x__+ kXoffsetiPhone6, __y__ + kYoffsetiPhone6): \
ccp(__x__, __y__))

#define ADJUST_X(__x__)         \
(IS_IPAD || IS_IPADHD?             \
( __x__ * 2 ) + kXoffsetiPad :      \
(IS_IPHONE5)?__x__ + kXoffsetiPhone5:\
IS_IPHONE6?__x__+ kXoffsetiPhone6: \
IS_IPHONE6P?__x__+ kXoffsetiPhone6p: \
__x__)

#define ADJUST_Y(__y__)         \
(IS_IPAD || IS_IPADHD?             \
( __y__ * 2 ) + kYoffsetiPad : \
IS_IPHONE5?__y__ + kYoffsetiPhone5:\
IS_IPHONE6?__y__+ kYoffsetiPhone6: \
IS_IPHONE6P?__y__+ kYoffsetiPhone6p: \
__y__)

#define HD_PIXELS(__pixels__)       \
(IS_IPAD || IS_IPADHD?             \
( __pixels__ * 2.0) :                \
__pixels__)

#define HD_TEXT(__size__)   \
(IS_IPAD?                   \
( __size__ * 1.6 ) :            \
(IS_IPADHD?                   \
( __size__ * 3.2 ) :            \
__size__)
/*
#define SD_OR_HD(__filename__)  \
(IS_IPAD == YES ?             \
[__filename__ stringByReplacingOccurrencesOfString:SD_PNG withString:HD_PNG] :  \
__filename__)
*/
#endif
