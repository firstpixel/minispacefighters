//
//  MenuLayer.cpp
//  MiniSpaceFighters
//
//  Created by Gil Beyruth on 8/29/11.
//  Copyright 2011 Ray Wenderlich. All rights reserved.
//

#import "LevelLayer.h"
#import "ActionLayer.h"
#import "SimpleAudioEngine.h"
#import "GlobalSingleton.h"
#import "SoundMenuItem.h"
#import "AppDelegate.h"
#import "GameBuyNowScene.h"
#import "ShopLayer.h"
#import "MKStoreManager.h"
@implementation LevelLayer

+(id)scene {
    
    CCScene *scene = [CCScene node];
    LevelLayer *layer = [LevelLayer node];
    [scene addChild:layer];
    return scene;
    
}
-(id)init
{
    if( (self=[super init]) )
	{
        GlobalSingleton*mySingleton = [GlobalSingleton sharedInstance];
         
        if([[GlobalSingleton sharedInstance] audioOn]){
            if ([SimpleAudioEngine sharedEngine] != nil) {
                if([[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying]==NO || ![[GlobalSingleton sharedInstance] isPlayingMenuMusic]){
                    [[GlobalSingleton sharedInstance] setIsPlayingMenuMusic:YES];
                    [[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"menu.m4a"];
                    [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"menu.m4a" loop:YES];
                }
            }
        }else{
            if ([[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying]==YES){
                [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
            }
            [[GlobalSingleton sharedInstance] setIsPlayingMenuMusic:NO];    
        }

        
        
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            [(AppDelegate*)[[UIApplication sharedApplication] delegate] startMovie:@"select-level-ipad" ofType:@"mov"];
               
        } else {
            [(AppDelegate*)[[UIApplication sharedApplication] delegate] startMovie:@"select-level" ofType:@"mov"];
            
        }
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"hud.plist"];
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"Sprites.plist"];
        
        scaleFactor = 1;
        px = 0.0;
        py = 0.0;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            scaleFactor = 2;
            px = 32.0f;
            py = 64.0f;
        }
        
        //Menu back
        SoundMenuItem *back = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt-back.png" selectedSpriteFrameName:@"bt-back-over.png" target:self selector:@selector(goBack)];
        CCMenu *menuBack = [CCMenu menuWithItems: back, nil];
        [menuBack alignItemsVerticallyWithPadding:12.0];
        [menuBack setPosition:ccp(( 22*scaleFactor )+px,( 270*scaleFactor)+py )];
        [self addChild:menuBack z:2];
        
        CCSprite *connection1 = [CCSprite spriteWithSpriteFrameName:@"connection1.png"];
        connection1.position = ccp(( 440*scaleFactor )+px ,( 233*scaleFactor )+py );
        [self addChild:connection1];
        
        CCSprite *connection2 = [CCSprite spriteWithSpriteFrameName:@"connection2.png"];
        connection2.position = ccp((440*scaleFactor)+px,(166*scaleFactor)+py);
        [self addChild:connection2];
        
        CCSprite *connection3 = [CCSprite spriteWithSpriteFrameName:@"connection3.png"];
        connection3.position = ccp((430*scaleFactor)+px,(97*scaleFactor)+py);
        [self addChild:connection3];
        
        CCSprite *connection4 = [CCSprite spriteWithSpriteFrameName:@"connection4.png"];
        connection4.position = ccp((350*scaleFactor)+px,(138*scaleFactor)+py);
        [self addChild:connection4];

        CCSprite *connection5 = [CCSprite spriteWithSpriteFrameName:@"connection5.png"];
        connection5.position = ccp((284*scaleFactor)+px,(267*scaleFactor)+py);
        [self addChild:connection5];

        CCSprite *connection6 = [CCSprite spriteWithSpriteFrameName:@"connection6.png"];
        connection6.position = ccp((220*scaleFactor)+px,(216*scaleFactor)+py);
        [self addChild:connection6];

        CCSprite *connection7 = [CCSprite spriteWithSpriteFrameName:@"connection2.png"];
        connection7.scaleY = 3.8f;
        connection7.position = ccp((216*scaleFactor)+px,(133*scaleFactor)+py);
        [self addChild:connection7];
        
        CCSprite *connection8 = [CCSprite spriteWithSpriteFrameName:@"connection8.png"];
        connection8.position = ccp((212*scaleFactor)+px,(50*scaleFactor)+py);
        [self addChild:connection8];

        CCSprite *connection9 = [CCSprite spriteWithSpriteFrameName:@"connection9.png"];
        connection9.position = ccp((97*scaleFactor)+px,(52*scaleFactor)+py);
        [self addChild:connection9];

        //LEVEL 1
        SoundMenuItem *plutoItem = [SoundMenuItem itemFromNormalSpriteFrameName:@"pluto.png" selectedSpriteFrameName:@"pluto.png"  target:self selector:@selector(playPluto)]; 
        plutoItem.position = ccp((410*scaleFactor)+px,(242*scaleFactor)+py);
       
        //LEVEL 2
        SoundMenuItem *neptuneItem;
        if([mySingleton isItemUnlocked:@"level2"]){
            neptuneItem = [SoundMenuItem itemFromNormalSpriteFrameName:@"neptune.png" selectedSpriteFrameName:@"neptune.png"  target:self selector:@selector(playNeptune)]; 
            
        }else{
            //alert
            neptuneItem = [SoundMenuItem itemFromNormalSpriteFrameName:@"neptune.png" selectedSpriteFrameName:@"neptune.png"  target:self selector:@selector(alertNeptune)];
            CCSprite* neptuneLocker = [CCSprite spriteWithSpriteFrameName:@"locker.png"];
            [neptuneItem addChild:neptuneLocker];
            neptuneLocker.position = ccp(neptuneItem.contentSize.width/2,neptuneItem.contentSize.height/2);
            
        }
        neptuneItem.position = ccp((435*scaleFactor)+px,(192*scaleFactor)+py);
        
        
        
        
        //[MKStoreManager sharedManager];
        
        //LEVEL 3
        SoundMenuItem *uranusItem;
        if([mySingleton isItemUnlocked:@"level3"] && [MKStoreManager isFeaturePurchased:kFeatureAId]){
            uranusItem = [SoundMenuItem itemFromNormalSpriteFrameName:@"uranus.png" selectedSpriteFrameName:@"uranus.png"  target:self selector:@selector(playUranus)]; 
            
            
        }else{
            if(![MKStoreManager isFeaturePurchased:kFeatureAId]){
                uranusItem = [SoundMenuItem itemFromNormalSpriteFrameName:@"uranus.png" selectedSpriteFrameName:@"uranus.png"  target:self selector:@selector(goToBuyLayer)]; 
            }else{
                //alert
                uranusItem = [SoundMenuItem itemFromNormalSpriteFrameName:@"uranus.png" selectedSpriteFrameName:@"uranus.png"  target:self selector:@selector(alertUranos)];
            }
            CCSprite* uranosLocker = [CCSprite spriteWithSpriteFrameName:@"locker.png"];
            [uranusItem addChild:uranosLocker];
            uranosLocker.position = ccp(uranusItem.contentSize.width/2,uranusItem.contentSize.height/2);
            
        
        }
        uranusItem.position = ccp((440*scaleFactor)+px,(140*scaleFactor)+py);
       
        //LEVEL 4
        SoundMenuItem *saturnItem;
        if([mySingleton isItemUnlocked:@"level4"] && [MKStoreManager isFeaturePurchased:kFeatureAId]){
            saturnItem = [SoundMenuItem itemFromNormalSpriteFrameName:@"saturn.png" selectedSpriteFrameName:@"saturn.png"  target:self selector:@selector(playSaturn)]; 
            
            
        }else{
            if(![MKStoreManager isFeaturePurchased:kFeatureAId]){
                saturnItem = [SoundMenuItem itemFromNormalSpriteFrameName:@"saturn.png" selectedSpriteFrameName:@"saturn.png"  target:self selector:@selector(goToBuyLayer)];
            }else{
                //alert
                saturnItem = [SoundMenuItem itemFromNormalSpriteFrameName:@"saturn.png" selectedSpriteFrameName:@"saturn.png"  target:self selector:@selector(alertSaturn)];
            }
            CCSprite* saturnLocker = [CCSprite spriteWithSpriteFrameName:@"locker.png"];
            [saturnItem addChild:saturnLocker];
            saturnLocker.position = ccp(saturnItem.contentSize.width/2,saturnItem.contentSize.height/2);
            
        }
        saturnItem.position = ccp((365*scaleFactor)+px,(70*scaleFactor)+py);
        
        
        
      //LEVEL 5
        SoundMenuItem *jupiterItem;
        if([mySingleton isItemUnlocked:@"level5"] && [MKStoreManager isFeaturePurchased:kFeatureAId]){
           jupiterItem = [SoundMenuItem itemFromNormalSpriteFrameName:@"jupiter.png" selectedSpriteFrameName:@"jupiter.png"  target:self selector:@selector(playJupiter)];
            
            
        }else{
            if(![MKStoreManager isFeaturePurchased:kFeatureAId]){
                jupiterItem = [SoundMenuItem itemFromNormalSpriteFrameName:@"jupiter.png" selectedSpriteFrameName:@"jupiter.png"  target:self selector:@selector(goToBuyLayer)];
            }else{
                //alert
                jupiterItem = [SoundMenuItem itemFromNormalSpriteFrameName:@"jupiter.png" selectedSpriteFrameName:@"jupiter.png"  target:self selector:@selector(alertJupiter)];
            }
            CCSprite* jupiterLocker = [CCSprite spriteWithSpriteFrameName:@"locker.png"];
            [jupiterItem addChild:jupiterLocker];
            jupiterLocker.position = ccp(jupiterItem.contentSize.width/2,jupiterItem.contentSize.height/2);
            
        }
        jupiterItem.position = ccp((330*scaleFactor)+px,(202*scaleFactor)+py);
        
       //LEVEL 6
        SoundMenuItem *marsItem;
        if([mySingleton isItemUnlocked:@"level6"] && [MKStoreManager isFeaturePurchased:kFeatureAId]){
            marsItem = [SoundMenuItem itemFromNormalSpriteFrameName:@"mars.png" selectedSpriteFrameName:@"mars.png"  target:self selector:@selector(playMars)];
        }else{
            if(![MKStoreManager isFeaturePurchased:kFeatureAId]){
                marsItem = [SoundMenuItem itemFromNormalSpriteFrameName:@"mars.png" selectedSpriteFrameName:@"mars.png"  target:self selector:@selector(goToBuyLayer)];
            }else{
                //alert
                marsItem = [SoundMenuItem itemFromNormalSpriteFrameName:@"mars.png" selectedSpriteFrameName:@"mars.png"  target:self selector:@selector(alertMars)];
            }
            CCSprite* marsLocker = [CCSprite spriteWithSpriteFrameName:@"locker.png"];
            [marsItem addChild:marsLocker];
            marsLocker.position = ccp(marsItem.contentSize.width/2,marsItem.contentSize.height/2);
            
        }
        marsItem.position = ccp((238*scaleFactor)+px,(250*scaleFactor)+py);
        
        //LEVEL 7
        SoundMenuItem *earthItem;
        if([mySingleton isItemUnlocked:@"level7"] && [MKStoreManager isFeaturePurchased:kFeatureAId]){
            earthItem = [SoundMenuItem itemFromNormalSpriteFrameName:@"earth.png" selectedSpriteFrameName:@"earth.png"  target:self selector:@selector(playEarth)];
        }else{
            if(![MKStoreManager isFeaturePurchased:kFeatureAId]){
                earthItem = [SoundMenuItem itemFromNormalSpriteFrameName:@"earth.png" selectedSpriteFrameName:@"earth.png"  target:self selector:@selector(goToBuyLayer)];
            }else{
                //alert
                earthItem = [SoundMenuItem itemFromNormalSpriteFrameName:@"earth.png" selectedSpriteFrameName:@"earth.png"  target:self selector:@selector(alertEarth)];
            }
            CCSprite* earthLocker = [CCSprite spriteWithSpriteFrameName:@"locker.png"];
            [earthItem addChild:earthLocker];
            earthLocker.position = ccp(earthItem.contentSize.width/2,earthItem.contentSize.height/2);
            
        }
        earthItem.position = ccp((214*scaleFactor)+px,(176*scaleFactor)+py);
        
        //LEVEL 8
        SoundMenuItem *venusItem;
        if([mySingleton isItemUnlocked:@"level8"] && [MKStoreManager isFeaturePurchased:kFeatureAId]){
            venusItem = [SoundMenuItem itemFromNormalSpriteFrameName:@"venus.png" selectedSpriteFrameName:@"venus.png"  target:self selector:@selector(playVenus)]; 
        }else{
            if(![MKStoreManager isFeaturePurchased:kFeatureAId]){
                venusItem = [SoundMenuItem itemFromNormalSpriteFrameName:@"venus.png" selectedSpriteFrameName:@"venus.png"  target:self selector:@selector(goToBuyLayer)]; 
            }else{
                //alert
                venusItem = [SoundMenuItem itemFromNormalSpriteFrameName:@"venus.png" selectedSpriteFrameName:@"venus.png"  target:self selector:@selector(alertVenus)]; 
            }
            CCSprite* venusLocker = [CCSprite spriteWithSpriteFrameName:@"locker.png"];
            [venusItem addChild:venusLocker];
            venusLocker.position = ccp(venusItem.contentSize.width/2,venusItem.contentSize.height/2);
            
        }
        venusItem.position = ccp((218*scaleFactor)+px,(96*scaleFactor)+py);
        
        //LEVEL 9
        SoundMenuItem *mercuryItem;
        if([mySingleton isItemUnlocked:@"level9"] && [MKStoreManager isFeaturePurchased:kFeatureAId]){
            mercuryItem = [SoundMenuItem itemFromNormalSpriteFrameName:@"mercury.png" selectedSpriteFrameName:@"mercury.png"  target:self selector:@selector(playMercury)]; 
        }else{
            if(![MKStoreManager isFeaturePurchased:kFeatureAId]){
                mercuryItem = [SoundMenuItem itemFromNormalSpriteFrameName:@"mercury.png" selectedSpriteFrameName:@"mercury.png"  target:self selector:@selector(goToBuyLayer)]; 
            }else{
                //alert
                mercuryItem = [SoundMenuItem itemFromNormalSpriteFrameName:@"mercury.png" selectedSpriteFrameName:@"mercury.png"  target:self selector:@selector(alertMercury)]; 
            }
             CCSprite* mercuryLocker = [CCSprite spriteWithSpriteFrameName:@"locker.png"];
            [mercuryItem addChild:mercuryLocker];
            mercuryLocker.position = ccp(mercuryItem.contentSize.width/2,mercuryItem.contentSize.height/2);
            
        }
        mercuryItem.position = ccp((174*scaleFactor)+px,(32*scaleFactor)+py);
        
        //LEVEL 10
        SoundMenuItem *sunItem;
        if([mySingleton isItemUnlocked:@"level10"] && [MKStoreManager isFeaturePurchased:kFeatureAId]){
            sunItem = [SoundMenuItem itemFromNormalSpriteFrameName:@"sun.png" selectedSpriteFrameName:@"sun.png"  target:self selector:@selector(playSun)];
        }else{
            if(![MKStoreManager isFeaturePurchased:kFeatureAId]){
                sunItem = [SoundMenuItem itemFromNormalSpriteFrameName:@"sun.png" selectedSpriteFrameName:@"sun.png"  target:self selector:@selector(goToBuyLayer)];
            }else{
                //alert
                sunItem = [SoundMenuItem itemFromNormalSpriteFrameName:@"sun.png" selectedSpriteFrameName:@"sun.png"  target:self selector:@selector(alertSun)];
            }
            CCSprite* sunLocker = [CCSprite spriteWithSpriteFrameName:@"locker.png"];
            [sunItem addChild:sunLocker];
            sunLocker.position = ccp(sunItem.contentSize.width/2,sunItem.contentSize.height/2);
            
        }
        sunItem.position = ccp((50*scaleFactor)+px,(110*scaleFactor)+py);
        
        CCMenu *menu = [CCMenu menuWithItems: plutoItem, neptuneItem, uranusItem, saturnItem, jupiterItem, marsItem, earthItem, venusItem, mercuryItem, sunItem, nil];
        //[menu alignItemsVerticallyWithPadding:12.0];
        [menu setPosition:ccp(0,0)];
        [self addChild:menu];
    }
    return self;
}
/*
-(void)selectPluto{ 
    CCSprite* pop = [CCSprite spriteWithSpriteFrameName:@"conf_talk.png"];
    [self addChild:pop z:1];
    
    CCLabelBMFont* title = [CCLabelBMFont labelWithString:@"PLUTO" fntFile:@"XitRam2.fnt"];
    NSString* message = @"Pluto, formal designation 134340 Pluto, is the second-most-massive known dwarf planet in the Solar System (after Eris) and the tenth-most-massive body observed directly orbiting the Sun. Originally classified as the ninth planet from the Sun, Pluto was recategorized as a dwarf planet and plutoid due to the discovery that it is one of several large bodies within the Kuiper belt.";
    CCLabelBMFont* text = [CCLabelBMFont labelWithString:message fntFile:@"A.fnt"];
    [pop addChild:title z:10];
    [pop addChild:text z:10];
    
    SoundMenuItem *close = [SoundMenuItem itemFromNormalSpriteFrameName:@"close.png" selectedSpriteFrameName:@"close_selected.png"  target:self selector:@selector(closePop)]; 
    close.position = ccp((50*scaleFactor)+px,(110*scaleFactor)+py);
    
    CCMenu *menu = [CCMenu menuWithItems: close, nil];
    [pop addChild:menu z:10];
}
-(void)closePop{ 
}
*/


-(void)playPluto{ 
    GlobalSingleton*mySingleton = [GlobalSingleton sharedInstance];
    [mySingleton setGameLevel:1];
    [mySingleton setLevelSelected:@"pluto"];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ActionLayer scene]]];
}
-(void)alertNeptune{
    //alert locked
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ops!" 
                                                    message:@"You must defeat Pluto boss first to unlock Neptune."
                                                   delegate:self 
                                          cancelButtonTitle:@"OK" 
                                          otherButtonTitles:nil];
    [alert show];
    [alert release];

}

-(void)playNeptune{
    GlobalSingleton*mySingleton = [GlobalSingleton sharedInstance];
    [mySingleton setGameLevel:2];
    [mySingleton setLevelSelected:@"neptune"];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ActionLayer scene]]];
}

-(void)alertUranos{
    //alert locked
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ops!" 
                                                    message:@"You must defeat Neptune boss first to unlock Uranos."
                                                   delegate:self 
                                          cancelButtonTitle:@"OK" 
                                          otherButtonTitles:nil];
    [alert show];
    [alert release];
    
}
-(void)playUranus{
    GlobalSingleton*mySingleton = [GlobalSingleton sharedInstance];
    [mySingleton setGameLevel:3];
    [mySingleton setLevelSelected:@"uranus"];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ActionLayer scene]]];
}

-(void)alertSaturn{
    //alert locked
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ops!" 
                                                    message:@"You must defeat Uranos boss first to unlock Saturn."
                                                   delegate:self 
                                          cancelButtonTitle:@"OK" 
                                          otherButtonTitles:nil];
    [alert show];
    [alert release];
    
}
-(void)playSaturn{
    GlobalSingleton*mySingleton = [GlobalSingleton sharedInstance];
    [mySingleton setGameLevel:4];
    [mySingleton setLevelSelected:@"saturn"];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ActionLayer scene]]];
}


-(void)alertJupiter{
    //alert locked
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ops!" 
                                                    message:@"You must defeat Saturn boss first to unlock Jupiter."
                                                   delegate:self 
                                          cancelButtonTitle:@"OK" 
                                          otherButtonTitles:nil];
    [alert show];
    [alert release];
    
}
-(void)playJupiter{
    GlobalSingleton*mySingleton = [GlobalSingleton sharedInstance];
    [mySingleton setGameLevel:5];
    [mySingleton setLevelSelected:@"jupiter"];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ActionLayer scene]]];
}

-(void)alertMars{
    //alert locked
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ops!" 
                                                    message:@"You must defeat Jupiter boss first to unlock Mars."
                                                   delegate:self 
                                          cancelButtonTitle:@"OK" 
                                          otherButtonTitles:nil];
    [alert show];
    [alert release];
    
}
-(void)playMars{
    GlobalSingleton*mySingleton = [GlobalSingleton sharedInstance];
    [mySingleton setGameLevel:6];
    [mySingleton setLevelSelected:@"mars"];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ActionLayer scene]]];
}

-(void)alertEarth{
    //alert locked
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ops!" 
                                                    message:@"You must defeat Mars boss first to unlock Earth."
                                                   delegate:self 
                                          cancelButtonTitle:@"OK" 
                                          otherButtonTitles:nil];
    [alert show];
    [alert release];
    
}
-(void)playEarth{
    GlobalSingleton*mySingleton = [GlobalSingleton sharedInstance];
    [mySingleton setGameLevel:7];
    [mySingleton setLevelSelected:@"earth"];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ActionLayer scene]]];
}

-(void)alertVenus{
    //alert locked
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ops!" 
                                                    message:@"You must defeat Earth boss first to unlock Venus."
                                                   delegate:self 
                                          cancelButtonTitle:@"OK" 
                                          otherButtonTitles:nil];
    [alert show];
    [alert release];
    
}
-(void)playVenus{
    GlobalSingleton*mySingleton = [GlobalSingleton sharedInstance];
    [mySingleton setGameLevel:8];
    [mySingleton setLevelSelected:@"venus"];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ActionLayer scene]]];
}
-(void)alertMercury{
    //alert locked
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ops!" 
                                                    message:@"You must defeat Venus boss first to unlock Mercury."
                                                   delegate:self 
                                          cancelButtonTitle:@"OK" 
                                          otherButtonTitles:nil];
    [alert show];
    [alert release];
    
}
-(void)playMercury{
    GlobalSingleton*mySingleton = [GlobalSingleton sharedInstance];
    [mySingleton setGameLevel:9];
    [mySingleton setLevelSelected:@"mercury"];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ActionLayer scene]]];
}

-(void)alertSun{
    //alert locked
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"Ops!" 
                                                    message:@"You must defeat Mercury boss first to unlock Sun."
                                                   delegate:self 
                                          cancelButtonTitle:@"OK" 
                                          otherButtonTitles:nil];
    [alert show];
    [alert release];
    
}
-(void)playSun{
    GlobalSingleton*mySingleton = [GlobalSingleton sharedInstance];
    [mySingleton setGameLevel:10];
    [mySingleton setLevelSelected:@"sun"];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ActionLayer scene]]];
}

-(void)goBack{
    
    [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ShopLayer scene]]];
    
}



-(void)goToBuyLayer{
    
    [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[GameBuyNowScene scene]]];
    
}
@end;