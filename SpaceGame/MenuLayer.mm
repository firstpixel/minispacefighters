//
//  MenuLayer.cpp
//  MiniSpaceFighters
//
//  Created by Gil Beyruth on 8/29/11.
//  Copyright 2011 Ray Wenderlich. All rights reserved.
//

#import "MenuLayer.h"

#import "ShopLayer.h"

#import "SimpleAudioEngine.h"
#import "GlobalSingleton.h"
#import "SoundMenuItem.h"
#import "MKStoreManager.h"
#import "AppDelegate.h"
#import "IntroLayer.h"
#import "HelpLayer.h"
#import "AboutScene.h"
#import "GameBuyNowScene.h"

@implementation MenuLayer

+(id)scene {
    
    CCScene *scene = [CCScene node];
    MenuLayer *layer = [MenuLayer node];
    [scene addChild:layer];
    return scene;
    
}
-(id)init
{
    if( (self=[super init]) )
	{
        CGSize winSize = [[CCDirector sharedDirector] winSize];
        int scaleFactor;
        float px = 0.0;
        float py = 0.0;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            scaleFactor = 2;
            px = 32.0f;
            py = 64.0f;
        } else {
            scaleFactor = 1;//[[CCDirector sharedDirector] contentScaleFactor];
        }
        if([[GlobalSingleton sharedInstance] audioOn]){
            if ([SimpleAudioEngine sharedEngine] != nil) {
                if([[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying]==NO || ![[GlobalSingleton sharedInstance] isPlayingMenuMusic]){
                    [[GlobalSingleton sharedInstance] setIsPlayingMenuMusic:YES];
                    [[SimpleAudioEngine sharedEngine] preloadBackgroundMusic:@"menu.m4a"];
                    [[SimpleAudioEngine sharedEngine] playBackgroundMusic:@"menu.m4a" loop:YES];
                }
            }
        }else{
            if ([[SimpleAudioEngine sharedEngine] isBackgroundMusicPlaying]==YES){
                [[SimpleAudioEngine sharedEngine] stopBackgroundMusic];
            }
            [[GlobalSingleton sharedInstance] setIsPlayingMenuMusic:NO];    
        }
        SoundMenuItem *audioToogler;
        if([[GlobalSingleton sharedInstance] audioOn]){
            audioToogler = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt_audiooff.png" selectedSpriteFrameName:@"bt_audiooff_selected.png" target:self selector:@selector(audioOff)];
        }else{
             audioToogler = [SoundMenuItem itemFromNormalSpriteFrameName:@"bt_audioon.png" selectedSpriteFrameName:@"bt_audioon_selected.png" target:self selector:@selector(audioOn)];
        }
        CCMenu *menuAudio = [CCMenu menuWithItems: audioToogler, nil];
        [menuAudio setPosition:ccp(( 460*scaleFactor )+px,( 270*scaleFactor)+py )];
        [self addChild:menuAudio z:2];

        
        if([[GlobalSingleton sharedInstance] deviceType]==@"iPod Touch 1G"){
            CCSprite *background2;
            background2 = [CCSprite spriteWithFile:@"menu.png"];
            background2.position = ccp(winSize.width/2, winSize.height/2);
            [self addChild:background2 z:0];
        }else{
            CCSprite *logo = [CCSprite spriteWithFile:@"logo.png"];
            logo.position = ccp(winSize.width/2, winSize.height/4*3);
            [self addChild:logo z:1];
        }
        
        if(![(AppDelegate*)[[UIApplication sharedApplication] delegate] movieIsPlaying:@"select-ship"])[(AppDelegate*)[[UIApplication sharedApplication] delegate] startMovie:@"select-ship" ofType:@"mov"];
        
        CCLabelBMFont *item0;
        CCLabelBMFont *item1;
        CCLabelBMFont *item2;
        CCLabelBMFont *item3;
        CCLabelBMFont *item4;
        CCMenuItemLabel *mitem0;
        CCMenuItemLabel *mitem1;
        CCMenuItemLabel *mitem2;
        CCMenuItemLabel *mitem3;
        CCMenuItemLabel *mitem4;
        
         CCMenu *menu=nil;
         if([MKStoreManager isFeaturePurchased:kFeatureAId]){
            
                 item0 = [CCLabelBMFont labelWithString:@"PLAY" fntFile:@"XitRam2.fnt"]; 
                 mitem0 = [CCMenuItemLabel itemWithLabel:item0 target:self selector:@selector(play)]; 
                 
                 item1 = [CCLabelBMFont labelWithString:@"INTRO" fntFile:@"XitRam2.fnt"]; 
                 mitem1 = [CCMenuItemLabel itemWithLabel:item1 target:self selector:@selector(intro)]; 
                 
             item2 = [CCLabelBMFont labelWithString:@"HELP" fntFile:@"XitRam2.fnt"]; 
             mitem2 = [CCMenuItemLabel itemWithLabel:item2 target:self selector:@selector(help)]; 
             
             item3 = [CCLabelBMFont labelWithString:@"FREE GAMES" fntFile:@"XitRam2.fnt"]; 
             mitem3 = [CCMenuItemLabel itemWithLabel:item3 target:self selector:@selector(freeGames)]; 
             
                item4 = [CCLabelBMFont labelWithString:@"ABOUT" fntFile:@"XitRam2.fnt"]; 
                mitem4 = [CCMenuItemLabel itemWithLabel:item4 target:self selector:@selector(about)]; 
                
                
               menu = [CCMenu menuWithItems: mitem0, mitem1, mitem2, mitem3,mitem4, nil];
        }else{
            
          
                item0 = [CCLabelBMFont labelWithString:@"PLAY" fntFile:@"XitRam2.fnt"]; 
                mitem0 = [CCMenuItemLabel itemWithLabel:item0 target:self selector:@selector(play)]; 
            
                item1 = [CCLabelBMFont labelWithString:@"INTRO" fntFile:@"XitRam2.fnt"]; 
                mitem1 = [CCMenuItemLabel itemWithLabel:item1 target:self selector:@selector(intro)]; 
            
                item2 = [CCLabelBMFont labelWithString:@"HELP" fntFile:@"XitRam2.fnt"]; 
                mitem2 = [CCMenuItemLabel itemWithLabel:item2 target:self selector:@selector(help)]; 
                
                item3 = [CCLabelBMFont labelWithString:@"BUY IGC CERTIFICATE" fntFile:@"XitRam2.fnt"]; 
                mitem3 = [CCMenuItemLabel itemWithLabel:item3 target:self selector:@selector(buy)]; 
            
                item4 = [CCLabelBMFont labelWithString:@"ABOUT" fntFile:@"XitRam2.fnt"]; 
                mitem4 = [CCMenuItemLabel itemWithLabel:item4 target:self selector:@selector(about)]; 
            
               
            menu = [CCMenu menuWithItems: mitem0, mitem1, mitem2, mitem3,mitem4, nil];
            
        }

        
        [menu alignItemsVerticallyWithPadding:14.0 * scaleFactor];
        [menu setPosition:ccp(winSize.width/2, winSize.height/3)];
        [self addChild:menu z:2];
        
        
    }
    return self;
}
-(void)audioOn{
    GlobalSingleton* mySingleton = [GlobalSingleton sharedInstance];
    [mySingleton setAudioOn:YES];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[MenuLayer scene]]];

}
-(void)audioOff{
    GlobalSingleton* mySingleton = [GlobalSingleton sharedInstance];
    [mySingleton setAudioOn:NO];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[MenuLayer scene]]];
}

-(void)play{
    GlobalSingleton* mySingleton = [GlobalSingleton sharedInstance];
    if([mySingleton isItemUnlocked:@"GameIntro"]){
        [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[ShopLayer scene]]];
    }else{
        [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[IntroLayer scene]]];
    }
}

-(void)intro{
     [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[IntroLayer scene]]];
}

-(void)about{
    [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[AboutScene scene]]];
}

-(void)buy{
    [[CCDirector sharedDirector] replaceScene:[CCTransitionProgressRadialCW transitionWithDuration:0.5f scene:[GameBuyNowScene scene]]];
}
-(void)freeGames{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"itms://itunes.com/apps/gilbeyruth"]];
}

/*-(void)settings{
    
}*/
-(void)help{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://minispacefighters.com/help/"]];
}

@end;