//
//  ActionLayer.h
//  SpaceGame
//
//  Created by Ray Wenderlich on 6/24/11.
//  Copyright 2011 Ray Wenderlich. All rights reserved.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.
//
// DO NOT DISTRIBUTE WITHOUT PRIOR AUTHORIZATION (SEE LICENSE.TXT)
//

#import "cocos2d.h"
#import "SpriteArray.h"
#import "Box2D.h"
#import "GLES-Render.h"
#import "GameObject.h"
#import "ParticleSystemArray.h"
#import "LevelManager.h"
#import "BossShip.h"

#import <MediaPlayer/MediaPlayer.h>

#import "HUDLayer.h"

enum GameStage {
    GameStageTitle = 0,
    GameStageAsteroids,
    GameStageDone
};

@interface ActionLayer : CCLayer {
    
    HUDLayer * _hud;
    
    
    MPMoviePlayerController * moviePlayer;
    
    CCLabelBMFont * _titleLabel1;
    CCLabelBMFont * _titleLabel2;
    CCMenuItemLabel *_playItem;
    
    CCSpriteBatchNode *_batchNode;
    CCSpriteBatchNode *_batchNodeFighter;
    CCSpriteBatchNode *_batchNodeBot;
    
    GameObject *_bot1;
    GameObject *_bot2;
    GameObject *_bot3;
    GameObject *_bot4;
    BOOL botCanShoot;
    
    int shotTime;
    CCNode* closestNode;
    float targetDistance;
    
    float px;
    float py;
    int scaleFactor;
    
    GameObject *_ship;
    float _shipPointsPerSecY;
    float newY;
    float _shipPointsPerSecX;
    float newX;
    double _nextAsteroidSpawn;
    SpriteArray * _asteroidsArray;
    SpriteArray * _laserArray;
    SpriteArray * _rocketArray;
    CCParallaxNode *_backgroundNode;
    CCSprite *_spacedust1;
    CCSprite *_spacedust2;
    CCSprite *_planetsunrise;
    CCSprite *_galaxy;
    CCSprite *_spacialanomaly;
    CCSprite *_spacialanomaly2;
    b2World * world_;
    GLESDebugDraw * _debugDraw;
    b2ContactListener * _contactListener;
    ParticleSystemArray * _explosions;
    GameStage _gameStage;
    BOOL _gameOver;
    //double _gameWonTime;
    LevelManager * _levelManager;
    CCLabelBMFont *_levelIntroLabel1;
    CCLabelBMFont *_levelIntroLabel2;
    
    CCLabelBMFont *_levelChatLabel1;
    CCLabelBMFont *_levelChatLabel2;
    CCLabelBMFont *_levelChatLabel3;
    CCLabelBMFont *_levelChatLabel4;
    CCLabelBMFont *_levelChatLabel5;
    CCLabelBMFont *_levelChatLabel6;
    
    SpriteArray * _alienArray;
    double _nextAlienSpawn;
    double _numAlienSpawns;
    CGPoint _alienSpawnStart;
    CGPoint lastTouchPosition;
    ccBezierConfig _bezierConfig;
    double _nextShootChance;
    SpriteArray * _enemyLasers;
    SpriteArray * _powerups;
    ParticleSystemArray* _powerupEffects;
    double _nextPowerupSpawn;
    BOOL _invincible;
    ParticleSystemArray * _boostEffects;
    ParticleSystemArray * _trailEffects;
    
    CCParticleSystemQuad *_fire1Effect;
    CCParticleSystemQuad *_fire12Effect;
    
    CCParticleSystemQuad *_smoke1Effect;
    CCParticleSystemQuad *_smoke11Effect;
    CCParticleSystemQuad *_smoke2Effect;
    CCParticleSystemQuad *_smoke21Effect;
    
    CCParticleSystemQuad *_shield1Effect;
    CCParticleSystemQuad *_shield2Effect;
    CCParticleSystemQuad *_shield3Effect;
    
    BossShip * _boss;
    BOOL _wantNextStage;
    SpriteArray * _cannonBalls;
    
    
    CCAnimation *animationUp;
    CCAnimation *animationDown;
    CCAnimation *animationUpStop;
    CCAnimation *animationDownStop;
    
    BOOL shipIsGoingUp;
    BOOL shipIsStoppingUp;
    BOOL shipIsGoingDown;
    BOOL shipIsStoppingDown;
    
    NSDictionary* powerDictionaryShip;
    NSDictionary* shieldDictionaryShip;
    
    
}
+ (id)scene;
- (id)initWithHUD:(HUDLayer *)hud;

- (void)newStageStarted;
- (void)shootEnemyLaserFromPosition:(CGPoint)position;
- (void)shootCannonBallAtShipFromPosition:(CGPoint)position;

- (void)updateNukes:(int)nukes;
- (void)updateShipShield:(int)shield;
- (void)updateShipPower:(int)power;
- (void)updatePoints:(int)points;

- (void)beginContact:(b2Contact *)contact;
- (void)endContact:(b2Contact *)contact;
@end